﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace ChessGame.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		UIWindow window;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif

			window = new UIWindow (UIScreen.MainScreen.Bounds);
			var rec = UIScreen.MainScreen.Bounds;

			LoadApplication (new App((int)rec.Height,(int)rec.Width));

			return base.FinishedLaunching (app, options);
		}

	}
}
	