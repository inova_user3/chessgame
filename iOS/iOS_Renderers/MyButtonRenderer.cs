using System;
using SharedViews;
using SharedModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using ChessGame.iOS;

[assembly: ExportRenderer (typeof (MyButton), typeof (MyButtonRenderer))]

namespace ChessGame.iOS
{
	/// <summary>
	/// iOS button renderer:
	/// here we set native iOS button properties that can't be accessed in xamarin button.
	/// </summary>
	public class MyButtonRenderer : ButtonRenderer
	{

		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);
			MyButton button = (MyButton)this.Element;
			var nativeButtonView = (UIButton)Control;
			//set button font
			UIFont font =  UIFont.FromName(Global._FONT_NAME,button.FontSize);
			nativeButtonView.Layer.BorderColor = button.BorderColor.ToCGColor();
			Console.WriteLine(nativeButtonView.Layer.BorderColor.Components[0]);
			nativeButtonView.Layer.CornerRadius = button.CornerRadius;
			nativeButtonView.Layer.BorderWidth = button.BorderSize;
			nativeButtonView.Font = font;
			nativeButtonView.LineBreakMode=UILineBreakMode.WordWrap;
			nativeButtonView.TitleLabel.TextAlignment = UITextAlignment.Center;
			var rec = UIScreen.MainScreen.Bounds;

			//set the button background and button image
			if (button.ButtonBackgroundImage != null) 
				nativeButtonView.SetBackgroundImage (UIImage.FromFile (button.ButtonBackgroundImage + ".png"), UIControlState.Normal);
			 if (button.ButtonImage != null) {
				nativeButtonView.SetImage (UIImage.FromFile (button.ButtonImage+".png"), UIControlState.Normal);
				if (button.ButtonImage == Global._RIGHT_ARROW) {
					if (button.Text == "next") {
						nativeButtonView.ImageEdgeInsets = new UIEdgeInsets (0, (rec.Width / 9), 0, 0);
						nativeButtonView.TitleEdgeInsets = new UIEdgeInsets (0, -(rec.Width / 9) / 2, 0, (rec.Width / 9) / 4);
					} else {
						nativeButtonView.ImageEdgeInsets = new UIEdgeInsets (0, (rec.Width / 4), 0, 0);
						nativeButtonView.TitleEdgeInsets = new UIEdgeInsets (0, -(rec.Width / 4) / 4, 0, (rec.Width / 4) / 4);
					}
				}
			}
		}
	}
}

