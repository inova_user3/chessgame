using System;
using SharedViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using ChessGame.iOS;
using SharedModel;


[assembly: ExportRenderer (typeof (MyLabel), typeof (MyLabelRenderer))]
namespace ChessGame.iOS
{
	/// <summary>
	/// iOS label renderer:
	/// here we set native iOS label properties that can't be accessed in xamarin label.
	/// </summary>
	public class MyLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);
			MyLabel label = (MyLabel)this.Element;
			var nativeLabelView = (UILabel)Control;
			UIFont font =  UIFont.FromName(Global._FONT_NAME,label.FontSize);
			nativeLabelView.Font = font;
		}
	}
}

