using System;
using SharedViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using ChessGame.iOS;

[assembly: ExportRenderer (typeof (MyList), typeof (MyListRenderer))]

namespace ChessGame.iOS
{
	/// <summary>
	/// iOS list renderer:
	/// here we set native iOS list properties that can't be accessed in xamarin list.
	/// </summary>
	public class MyListRenderer : ListViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
			var nativeTableView = (UITableView)Control;
			MyList listView = (MyList)this.Element;
			nativeTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			nativeTableView.ScrollEnabled = listView.ShouldScroll;
			nativeTableView.AllowsSelection = false;
		}
	}
}