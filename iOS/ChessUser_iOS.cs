﻿using System;
using SharedModel;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency (typeof (ChessUser_iOS))]

public class ChessUser_iOS : UserInterface
{
	public ChessUser_iOS () {}

	/// <summary>
	/// Saves the user properties to the preferences file.
	/// 1) nickname , city
	/// 2) avatar
	/// 3) chess style
	/// 4) skill level
	/// </summary>
	/// <param name="user">User.</param>
	public void saveUser(ChessUser user){
		var prefs = NSUserDefaults.StandardUserDefaults;
		if (user.NickName == null)
			user.NickName = "user1";
		if(user.City==null)
			user.City="City1";
		//save user's properties for a specific keys
		prefs.SetValueForKey (new NSString (user.NickName), new NSString ("user name"));
		prefs.SetValueForKey (new NSString (user.City), new NSString ("user city"));
		prefs.SetValueForKey(new NSString("chess style"), user.ChessStyle==Global.CHESS_STYLE._ADULT? new NSString("adult"):new NSString("kids"));
		prefs.SetValueForKey(new NSString("avatar"), new NSString(Global.getAvatarImage(user.Avatar)));
		if(user.SkillLevel==Global.SKILL_LEVEL._BEGINNER)
			prefs.SetValueForKey(new NSString("skill level"), new NSString("beginner"));
		else if(user.SkillLevel==Global.SKILL_LEVEL._INTERMEDIATE)
			prefs.SetValueForKey(new NSString("skill level"), new NSString("intermediate"));
		else if(user.SkillLevel==Global.SKILL_LEVEL._EXPERT)
			prefs.SetValueForKey(new NSString("skill level"), new NSString("expert"));
	}

	/// <summary>
	/// Loads the user properties from preferences file.
	/// </summary>
	/// <returns>The user.</returns>
	public ChessUser loadUser(){
		var prefs = NSUserDefaults.StandardUserDefaults;
		if (!prefs.
			ToDictionary().
			//			AsDictionary ().
			ContainsKey (new NSString ("user name")))
			return null;
		ChessUser.getInstance().NickName= prefs.StringForKey ("user name");
		ChessUser.getInstance().City= prefs.StringForKey ("user city");
		ChessUser.getInstance().ChessStyle= prefs.StringForKey ("chess style")=="adult"?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS;
		if(prefs.StringForKey ("skill level")=="beginner")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._BEGINNER;
		else if(prefs.StringForKey ("skill level")=="intermediate")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._INTERMEDIATE;
		else if(prefs.StringForKey ("skill level")=="expert")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._EXPERT;

		return ChessUser.getInstance ();
	}
}