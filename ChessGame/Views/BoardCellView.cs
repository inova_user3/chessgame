using System;
using Xamarin.Forms;
using System.ComponentModel;
using SharedModel;
using System.Collections.Generic;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Layout contains chess piece image and possible move image
	/// for each chess board cell
	/// </summary>
	public class BoardCellView : AbsoluteLayout
	{
		//piece index to know which piece is selected
		int rowNumber;
		int columnNumber;
		Image pieceImage;
		Image possibleMoveImage;

		/// <summary>
		/// Create board cell layout that contains piece image and possible move image if the cell is possible move
		///  and bind the image to the boardcell object so that any change in boardcell object will be done to the view.
		/// </summary>
		/// <param name="rNum">Row number of board cell.</param>
		/// <param name="cNum">Column number of board cell.</param>
		/// <param name="cell">Cell object to bind the image to it.</param>
		public BoardCellView (int rNum,int cNum,BoardCell cell)
		{
			rowNumber = rNum;
			columnNumber = cNum;
			//craete the piece image
			#region create piece image and set paramters
			pieceImage = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest=Global._GRID_CELL_SIZE,
				HeightRequest=Global._GRID_CELL_SIZE,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Source=null
			};
			//add gesture to the image to select the cell
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.TappedCallback += (s, e) => {
				ChessEngine.checkCell(rowNumber,columnNumber);
			};
			pieceImage.GestureRecognizers.Add(tapGestureRecognizer);
			//Binding Board cell with the image
			pieceImage.BindingContext = ChessEngine.gState.Board.getBoardCells()[rowNumber,columnNumber];
			pieceImage.SetBinding(Image.SourceProperty, new Binding("PieceImage"));
			#endregion
			//craete the possible move image with the same board cell size
			possibleMoveImage = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest=Global._GRID_CELL_SIZE,
				HeightRequest=Global._GRID_CELL_SIZE,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Source=null
			};
			//add gesture to the image to select the cell
			possibleMoveImage.GestureRecognizers.Add(tapGestureRecognizer);
			//Binding possible move cell with the image
			possibleMoveImage.BindingContext = ChessEngine.gState.Board.getBoardCells()[rowNumber,columnNumber];
			possibleMoveImage.SetBinding(Image.BackgroundColorProperty, new Binding("BackgroundColor"));
			possibleMoveImage.SetBinding(Image.SourceProperty, new Binding("PossibleMoveImage"));

			this.Children.Add(possibleMoveImage, new Point(0,0));
			this.Children.Add(pieceImage, new Point(0,0));
		}
	}
}