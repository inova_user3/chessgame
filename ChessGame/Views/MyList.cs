using System;
using Xamarin.Forms;

namespace SharedViews
{

	/// <summary>
	/// List propertis that will be rendered in native iOS and native Android
	/// </summary>
	public class MyList :ListView
	{
		/// <summary>
		/// The selected item color property.
		/// </summary>
		public static readonly BindableProperty ShouldScrollProperty = BindableProperty.Create<MyList,bool>(p => p.ShouldScroll,true);

		public bool ShouldScroll{
			get{ return (bool)base.GetValue(ShouldScrollProperty);}
			set {base.SetValue(ShouldScrollProperty,value);}
		}

	}
}

