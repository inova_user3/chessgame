using System;
using SharedModel;
using SharedViews;
using Xamarin.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SharedViews
{
	/// <summary>
	/// Custom timer picker with 2 list views one for seconds and the other for minutes
	/// </summary>
	public class ChessPicker:RelativeLayout,INotifyPropertyChanged
	{
		 
		public event PropertyChangedEventHandler PropertyChanged;
	
		string secsStr="00";
		string minsStr="00";
		string timerValue;

		/// <summary>
		/// Gets or sets the timer value.
		/// </summary>
		/// <value>The timer value.</value>
		public string TimerValue {
			get { return timerValue; }
			set
			{
				if (value.Equals(timerValue,StringComparison.Ordinal))
				{
					return;
				}
				this.timerValue = value;
				OnPropertyChanged("TimerValue");
			}
		}



		/// <summary>
		/// Custom timer picker with 2 list views one for seconds and the other for minutes
		/// </summary>
		public ChessPicker ()
		{
			//set the time value with the 2 digits formate 
			TimerValue=formateTimeValue(minsStr,secsStr);
			List<CustomListViewRow> mins = new List<CustomListViewRow> ();
			List<CustomListViewRow> secs = new List<CustomListViewRow> ();
			for (int k = 0; k < 60; k++) {
				mins.Add (new CustomListViewRow(((k+1)+""),Color.Transparent,k));
				secs.Add (new CustomListViewRow(((k+1)+""),Color.Transparent,k));
			}
			int i = 0;
			Color rowColor=Color.Transparent;
			//create the minutes list view 
			MyList minutesList = new MyList
			{
				BackgroundColor= Color.FromHex("#C2AC57"),
				ShouldScroll=true,
				ItemsSource = mins,
				ItemTemplate = new DataTemplate(() => {
					
						CustomListViewRowView rowView =new CustomListViewRowView(mins[i]);
						//add listner to the image
						var tapGestureRecognizer = new TapGestureRecognizer();
						tapGestureRecognizer.TappedCallback += (s, e) => {
							for(int l1=0;l1<mins.Count;l1++)
									mins[l1].RowColor=Color.Transparent;

							mins[((CustomListViewRowView)s).Row.Index].RowColor=Color.FromHex("#FDFCAC");
							string value= mins[((CustomListViewRowView)s).Row.Index].RowLabel;
							minsStr = value;
							TimerValue=formateTimeValue(minsStr,secsStr);
						};
						rowView.GestureRecognizers.Add(tapGestureRecognizer);


						ViewCell row= new ViewCell
						{
							View =rowView
						};

						i++;
//						row.Tapped += Mins_Cell_Tapped;
						return row;
					})
			};

			int j = 0;
			//create the seconds list view 
			MyList secondsList = new MyList
			{
				BackgroundColor= Color.FromHex("#DECE77"),
				ShouldScroll=true,
				ItemsSource = secs,
				ItemTemplate = new DataTemplate(() =>
					{
						CustomListViewRowView rowView =new CustomListViewRowView(secs[j]);
						//add listner to the image
						var tapGestureRecognizer = new TapGestureRecognizer();
						tapGestureRecognizer.TappedCallback += (s, e) => {
							for(int l2=0;l2<secs.Count;l2++)
								secs[l2].RowColor=Color.Transparent;
							secs[((CustomListViewRowView)s).Row.Index].RowColor=Color.FromHex("#FCFBE9");
							string value= secs[((CustomListViewRowView)s).Row.Index].RowLabel;
							secsStr = value;
							TimerValue=formateTimeValue(minsStr,secsStr);
						};
						rowView.GestureRecognizers.Add(tapGestureRecognizer);

						ViewCell row=new ViewCell
						{
							View = rowView
						};
						j++;

//						row.Tapped += Secs_Cell_Tapped;
						return row; 
					})
			};



			Image background = new Image {
				Aspect = Aspect.Fill,
				Source=Global._TIME_SCROLLER_BACKGROUND
			};

			//create the on_off switcher 
			MySwitcher switcher = new MySwitcher(this){
				Option1="On",
				Option2="off",
				Option1Selected=true,
				Option2Selected=false,
				Option1Image=Global._OPTION_1SELECTED,
				Option2Image=Global._OPTION_2UNSELECTED,
				SwitcherType="ON_OFF",
			};

			// add components to the relative layout 
			//first the background 
			this.Children.Add (background, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);
			// then the 2 lists: minutes list and seconds list 
			this.Children.Add (minutesList, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3/20); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/100); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*7/20); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*72/100); })
			);
			this.Children.Add (secondsList, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/100); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*7/20); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*72/100); })
			);
			//finally the switch button to confirm the selected time
			this.Children.Add (switcher, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*9/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/10); })
			);
		}

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		/// <summary>
		/// Update the time value for the time picker 
		/// </summary>
		/// <param name="flag">Is picker visible.</param>
		public void updatePickerState(bool flag){
			this.IsVisible = false;
			if (!flag) {
				minsStr = "0";
				secsStr = "0";
				TimerValue = formateTimeValue (minsStr, secsStr);
			}
		}

		/// <summary>
		/// Return the time in 2 digits formate 
		/// </summary>
		/// <returns>The time value.</returns>
		/// <param name="_mins">Minutes value.</param>
		/// <param name="_secs">Seconds value.</param>
		public string formateTimeValue(string _mins,string _secs){
			int mins=Convert.ToInt16(_mins);
			int secs=Convert.ToInt16(_secs);
			string value = "";
			if(mins<10)
				value+="0"+mins+":";
			else
				value+=mins+":";
			if(secs<10)
				value+="0"+secs;
			else
				value+=secs;
			return value;
		}
	}
}

