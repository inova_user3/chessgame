using System;
using Xamarin.Forms;
using System.ComponentModel;
using SharedModel;
using System.Collections.Generic;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Layout conatins Image that represents each avatar.
	/// </summary>
	public class AvatarView : AbsoluteLayout
	{
		//Avatar Image
		Image avatarImage;
		//the avatar's indes to know which one was selected
		int index;

		/// <summary>
		/// create avatar view that contains image for each avatar and bind the image to the avatar object so that any change in avatar object will be done to the view.
		/// </summary>
		/// <param name="avatar">Avatar name of the image.</param>
		/// <param name="_index">Index of the avatar image.</param>
		/// <param name="avatarHeight">Avatar image height .</param>
		public AvatarView (Global.AVATAR avatar,int _index,int avatarHeight)
		{
			this.index = _index;
			//create avatar's image with absolute width and height
			avatarImage = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source = Global.getAvatarImage (Global.getAvatarFromIndex(_index))
			};
			//add gesture to the image to select the avatar
			var tapGestureRecognizer1 = new TapGestureRecognizer();
			tapGestureRecognizer1.TappedCallback += (s, e) => {
				MessagingCenter.Send<AvatarView, int> (this, "update avatar", index);
			};
			avatarImage.GestureRecognizers.Add(tapGestureRecognizer1);

			this.Children.Add(avatarImage, new Point(0,0));
		}
	}
}

