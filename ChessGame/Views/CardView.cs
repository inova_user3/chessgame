using System;
using Xamarin.Forms;
using System.ComponentModel;
using SharedModel;
using System.Collections.Generic;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Layout that contains card image.
	/// </summary>
	public class CardView : AbsoluteLayout
	{
		Image cardImage;

		/// <summary>
		/// Create card layout that contains card image 
		/// and bind the image to the card object so that any change in card object will be done to the view.
		/// </summary>
		/// <param name="card">Card object to bind the image to it.</param>
		public CardView (Card card)
		{
			// create the card image with specific size 
			cardImage = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest=Global._GRID_CELL_SIZE*2,
				HeightRequest=Global._GRID_CELL_SIZE*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=null,
			};

			//add listner to the image
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.TappedCallback += (s, e) => {
				ChessEngine.isCardHavingMoves(card);
			};
			cardImage.GestureRecognizers.Add(tapGestureRecognizer);

			//Binding Board cell with the image
			cardImage.BindingContext = card;
			cardImage.SetBinding(Image.SourceProperty, new Binding("CardImage"));

			this.Children.Add(cardImage, new Point(0,0));

		}
	}
}

