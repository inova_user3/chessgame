using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Player wizard 3 screen:
	/// Contains layout to select the avatar you want.
	/// </summary>
	public class PlayerWizard_3Screen : ContentPage
	{
		int avatarHeight;
		Global.AVATAR avatar;

		/// <summary>
		/// Create player wizard 3 screen to choose your avatar 
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		/// <param name="isInit">Is it the first time to open the game.</param>
		public PlayerWizard_3Screen (int widthDP,int heightDP,bool isInit)
		{
			bool isInitialization=isInit;
			avatarHeight = heightDP / 12;
			//=================Define components=====================
			MyButton backButton = new MyButton {
				Text="back",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._LEFT_ARROW
			};

			backButton.Clicked += (s, e) => {
				MessagingCenter.Unsubscribe<AvatarView, int> (this, "update avatar");
				Navigation.PopModalAsync();
			};

			MyButton nextButton = new MyButton {
				Text="next",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._RIGHT_ARROW
			};
			nextButton.Clicked += (s, e) => {
				//Go to the next configuration page
				MessagingCenter.Unsubscribe<AvatarView, int> (this, "update avatar");
				ChessUser.getInstance().Avatar=avatar;
				Navigation.PushModalAsync(new PlayerWizard_4Screen(widthDP,heightDP,true));
			};

			MyButton finishButton = new MyButton {
				Text="finish",
				TextColor=Color.FromHex("#C6BF95"),
				BackgroundColor=Color.Black,
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25
			};
			finishButton.Clicked += (s, e) => {
				MessagingCenter.Unsubscribe<AvatarView, int> (this, "update avatar");
				ChessUser.getInstance().Avatar=avatar;
				DependencyService.Get<UserInterface>().saveUser(ChessUser.getInstance());
				//if the first time to open the game then continue in configuration pages
				//else go to main menu
				if(!isInitialization)
					Navigation.PopModalAsync();
				else
					Navigation.PushModalAsync(new NavigationPage(new TitleScreenPage(widthDP,heightDP)));
			};

			Image front_2_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT2_BACKGROUND,
			};

			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};

			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};

			MyLabel selectAvatarLabel = new MyLabel{
				Text="Select Your Chess Avatar",
				TextColor=Color.Black,
				FontSize=30,
				XAlign=TextAlignment.Center
			};

			StackLayout avatarsLayout = new StackLayout {
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Spacing=20
			};

			int columnsNum=(int)((widthDP/2)/(avatarHeight*2));
			List<StackLayout> layouts=new List<StackLayout>();
			List<AvatarView> avatarsList = new List<AvatarView> ();
			fillAvatarList (avatarsList);
			for (int i = 0; i < 16; i++) {
				if(i%columnsNum==0){
					layouts.Add(new StackLayout{
						BackgroundColor = Color.Transparent,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center
					});
					avatarsLayout.Children.Add (layouts[layouts.Count-1]);
				}
				layouts[layouts.Count-1].Children.Add(avatarsList[i]);
				layouts [layouts.Count - 1].Spacing = 20;
			}

			//Alert to select the avatar
			MessagingCenter.Subscribe<AvatarView, int> (this, "update avatar", (sender, arg) => {
				avatar=Global.getAvatarFromIndex(arg);
				Console.WriteLine(avatar);
			});

			ScrollView scrollView = new ScrollView{
				BackgroundColor=Color.Transparent,
				Orientation=ScrollOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Content = avatarsLayout
			};
			scrollView.IsClippedToBounds=true;

			#region buttons contaier
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_2_Container.Children.Add (scrollView, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);			
			#endregion


			#region front container
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_1_Container.Children.Add (front_2_Background, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4 + parent.Height*1f/32); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2 - parent.Height*2f/32); })
			);

			front_1_Container.Children.Add (selectAvatarLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);

			front_1_Container.Children.Add (backButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);

			front_1_Container.Children.Add (finishButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (nextButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);


			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			if (!isInitialization)
				nextButton.IsVisible = false;
			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}

		/// <summary>
		/// Fills the avatar list with the avatar views.
		/// </summary>
		/// <param name="avatarsList">Avatars list.</param>
		public void fillAvatarList(List<AvatarView> avatarsList){
			for (int i = 0; i < 16; i++) {
				avatarsList.Add (new AvatarView(Global.getAvatarFromIndex(i),i,avatarHeight));
			}
		}

	}
}

