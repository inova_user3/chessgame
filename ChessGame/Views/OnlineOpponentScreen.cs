using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	public class OnlineOpponentScreen : ContentPage
	{
			/// <summary>
			/// Create single player setup screen where you can set :
			/// number of cards, match timer ,move time and computer level for both white and black player.
			/// </summary>
			/// <param name="widthDP">Screen width.</param>
			/// <param name="heightDP">Screen heigh.</param>
			public OnlineOpponentScreen (int widthDP,int heightDP)
			{

				Image veryFontBackground=new Image {
					Aspect = Aspect.Fill,
					Source=Global._FRONT2_BACKGROUND
				};
				Image frontBackground=new Image {
					Aspect = Aspect.Fill,
					Source=Global._SINGLE_PLAYER_SETUP_FRONT
				};

				Image titleImage=new Image {
					Aspect = Aspect.Fill,
					Source=Global._SINGLE_PLAYER_SETUP_TITLE
				};

				Image startMatchButton=new Image {
					Aspect = Aspect.Fill,
					Source=Global._START_MATCH_BUTTON
				};
					

				MyButton returnButton = new MyButton {
					Text="return to main menu",
					TextColor=Color.FromHex("#020202"),
					BackgroundColor=Color.FromHex("#99C6BF95"),
					BorderSize=5,
					CornerRadius =10,
					borderColor=Color.Black,
					FontSize=15,
					ButtonImage=Global._LEFT_ARROW
				};
				returnButton.Clicked += (s, e) => {
					Navigation.PopAsync ();

				};
				MyButton onlineOpponentButton = new MyButton {
					Text="CHOOSE AN OPPONENT WHO IS ONLINE NOW",
					TextColor=Color.FromHex("#020202"),
					BackgroundColor=Color.FromHex("#99C6BF95"),
					BorderSize=5,
					CornerRadius =10,
					borderColor=Color.Black,
					FontSize=15,
					ButtonImage=Global._LEFT_ARROW
				};



				//===============Define stacklayout that contains logo============

			StackLayout cell = new StackLayout{ 
				BackgroundColor=Color.Transparent,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
				Spacing=20,
				Children={
					new Image{
						Source=Global.getAvatarImage(Global.AVATAR._AVATAR_1),Aspect=Aspect.AspectFit,
//						WidthRequest=avatarHeight*2,
//						HeightRequest=avatarHeight*2,
//						HorizontalOptions = LayoutOptions.Center,
					},
					new Label{Text="label",TextColor=Color.Blue},
					new Label{Text="label label label label label",TextColor=Color.Red}
				}
			};

			List<StackLayout> layouts=new List<StackLayout>();
			for (int i = 0; i < 20; i++) {
				layouts.Add ( new StackLayout{ 
					BackgroundColor=Color.Transparent,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Start,
					Spacing=20,
					Children={
						new StackLayout{
							Padding=new Thickness (0,0,20,0),
							Orientation=StackOrientation.Vertical,
							Children={
								new Image{Source=Global.getAvatarImage(Global.AVATAR._AVATAR_1),Aspect=Aspect.AspectFit,HeightRequest=80,WidthRequest=80},
								new Label{Text="Ahmed", TextColor=Color.FromHex("#F2ECBE"), HorizontalOptions=LayoutOptions.Center}
							}
						}
						,
						new MyButton{
							Text="THEY CHOOSE WHITE/YOU ARE BLACK\nLOS ANGELESS, CA",
							TextColor=Color.FromHex("#F2ECBE"),
							BackgroundColor=Color.FromHex("#997B7257"),
							BorderSize=5,
							CornerRadius =10,
							borderColor=Color.Black,
							FontSize=15,

							HorizontalOptions=LayoutOptions.Center
								

						},
						new MyButton{
							Text="CARDS FOR EACH SIDE\nWHITE:3CARDS/BLACK:3CARDS",
							TextColor=Color.FromHex("#020202"),
							BackgroundColor=Color.FromHex("#99B6B090"),
							BorderSize=5,
							CornerRadius =10,
							borderColor=Color.Black,
							FontSize=15,
							HorizontalOptions=LayoutOptions.Center
						}
					}
				});
			}

				StackLayout onlineOpponents = new StackLayout {
					BackgroundColor = Color.Transparent,
					Orientation = StackOrientation.Vertical,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Spacing=20,
					
				};
			for(int i=0;i<layouts.Count;i++)
				onlineOpponents.Children.Add (layouts[i]);
				

				ScrollView scrollView = new ScrollView{
				BackgroundColor=Color.Transparent,
					Orientation=ScrollOrientation.Vertical,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					Content = onlineOpponents
				};
				scrollView.IsClippedToBounds=true;
				
				RelativeLayout veryFrontContainer = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				};


				veryFrontContainer.Children.Add (scrollView, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0/16); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*0f/8); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/3); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*24f/24); })
				);	

	

				//=====================================================================================
				RelativeLayout frontContainer = new RelativeLayout{
					BackgroundColor=Color.Transparent,
				};


				frontContainer.Children.Add (returnButton, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*1.5f/8); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0.9f/3); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*1.5f/24); })
				);
				frontContainer.Children.Add (onlineOpponentButton, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*2.2f/8); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1.4f/3); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*1.5f/24); })
				);
				frontContainer.Children.Add (veryFontBackground, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0.043); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*0.367); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0.913); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*0.5588); })
				);

				frontContainer.Children.Add (veryFrontContainer, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0.043); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*0.4); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*0.913); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*0.5); })
				);

				
				//=====================================================================================
				RelativeLayout container = new RelativeLayout{
					BackgroundColor=Color.Transparent,
					HorizontalOptions=LayoutOptions.Start,
					VerticalOptions=LayoutOptions.Start,
					WidthRequest=widthDP,
					HeightRequest=heightDP
				};

				container.Children.Add (frontBackground,
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*11f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*10f/13); })
				);

				container.Children.Add (frontContainer, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*11f/13); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*10f/13); })
				);

				container.Children.Add (titleImage, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/5); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/(13*6)); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/5); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/13); })
				);


				container.Children.Add (startMatchButton, 
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*45f/52); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
					Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/26); })
				);

				this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
				Content = container;
		}
	}
}


