using System;
using Xamarin.Forms;

namespace SharedViews
{

	/// <summary>
	/// Label propertis that will be rendered in native iOS and native Android Renderers.
	/// </summary>
	public class MyLabel : Label
	{
		//xamarin Label propertis that will be rendered in iOS and Android

		/// <summary>
		/// The font size of the text.
		/// </summary>
		public static readonly BindableProperty fontSizeProperty = BindableProperty.Create<MyLabel,int>(p => p.FontSize,0);

		public int FontSize{
			get{ return (int)base.GetValue(fontSizeProperty);}
			set {base.SetValue(fontSizeProperty,value);}
		}
			
		/// <summary>
		/// The background image for the label.
		/// </summary>
		public static readonly BindableProperty backgroundImageProperty = BindableProperty.Create<MyLabel,string>(p => p.BackgroundImage,null);

		public string BackgroundImage{
			get{ return (string)base.GetValue(backgroundImageProperty);}
			set {base.SetValue(backgroundImageProperty,value);}
		}

	}
}

