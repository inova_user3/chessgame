using System;
using Xamarin.Forms;
using SharedModel;
using SharedViews;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SharedViews
{
	/// <summary>
	/// Custom switcher that contains 2 options.
	/// </summary>
	public class MySwitcher :RelativeLayout, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		ChessPicker picker;

		string option1="123";
		string option2="123";
		string option1Image;
		string option2Image;
		bool option1Selected;
		bool option2Selected;
		string switcherType;
		MyLabel option1Label;
		MyLabel option2Label;


		/// <summary>
		/// Gets or sets the option1 label.
		/// </summary>
		/// <value>The option1 label.</value>
		public MyLabel Option1Label {
			get { return option1Label; }
			set{ this.option1Label = value; }
		}

		/// <summary>
		/// Gets or sets the option2 label.
		/// </summary>
		/// <value>The option2 label.</value>
		public MyLabel Option2Label {
			get { return option2Label; }
			set{ this.option2Label = value; }
		}

		/// <summary>
		/// Gets or sets the type of the switcher.
		/// </summary>
		/// <value>The type of the switcher.</value>
		public string SwitcherType {
			get { return switcherType; }
			set{ this.switcherType = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="SharedViews.MySwitcher"/> option1 selected.
		/// </summary>
		/// <value><c>true</c> if option1 selected; otherwise, <c>false</c>.</value>
		public bool Option1Selected {
			get { return option1Selected; }
			set{ this.option1Selected = value;}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="SharedViews.MySwitcher"/> option2 selected.
		/// </summary>
		/// <value><c>true</c> if option2 selected; otherwise, <c>false</c>.</value>
		public bool Option2Selected {
			get { return option2Selected; }
			set {this.option2Selected = value;	}
		}

		/// <summary>
		/// Gets or sets the option1 image.
		/// </summary>
		/// <value>The option1 image.</value>
		public string Option1Image {
			get { return option1Image; }
			set
			{
				if (value.Equals(this.option1Image,StringComparison.Ordinal))
				{
					return;
				}
				this.option1Image = value;
				OnPropertyChanged("Option1Image");
			}
		}

		/// <summary>
		/// Gets or sets the option2 image.
		/// </summary>
		/// <value>The option2 image.</value>
		public string Option2Image {
			get { return option2Image; }
			set
			{
				if (value.Equals(this.option2Image,StringComparison.Ordinal))
				{
					return;
				}
				this.option2Image = value;
				OnPropertyChanged("Option2Image");
			}
		}

		/// <summary>
		/// Gets or sets the option1.
		/// </summary>
		/// <value>The option1.</value>
		public string Option1 {
			get { return option1; }
			set
			{
				if (value.Equals(this.option1,StringComparison.Ordinal))
				{
					return;
				}
				this.option1 = value;
				OnPropertyChanged("Option1");
			}
		}

		/// <summary>
		/// Gets or sets the option2.
		/// </summary>
		/// <value>The option2.</value>
		public string Option2 {
			get { return option2; }
			set
			{
				if (value.Equals(this.option2,StringComparison.Ordinal))
				{
					return;
				}
				this.option2 = value;
				OnPropertyChanged("Option2");
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SharedViews.MySwitcher"/> class.
		/// </summary>
		/// <param name="picker">Picker.</param>
		public MySwitcher (ChessPicker picker)
		{
			this.picker = picker;

			Image option1 = new Image {
				Aspect = Aspect.Fill,
				BindingContext = this,
			};
			option1.SetBinding (Image.SourceProperty, new Binding ("Option1Image"));


			Image option2 = new Image {
				Aspect = Aspect.Fill,
				Source=Global._DOWN_SWITCHER_BUTTON,
				BindingContext = this
			};
			option2.SetBinding (Image.SourceProperty, new Binding ("Option2Image"));


		
			Option1Label=new MyLabel{
				Text=Option1,
				FontSize=15,
				TextColor=Color.FromHex("#020202"),
				XAlign=TextAlignment.Center,
				YAlign=TextAlignment.Center
			};


			Option2Label=new MyLabel{
				Text=Option2,
				FontSize=15,
				TextColor=Color.FromHex("#F3ECBE"),
				XAlign=TextAlignment.Center,
				YAlign=TextAlignment.Center,
			};

			//add gesture to option1 image
			var option1Action = new TapGestureRecognizer();
			option1Action.TappedCallback += (s, e) => {
				if(SwitcherType=="ON_OFF")
					picker.updatePickerState(true);
				else if(SwitcherType=="White Player Switch")
					MessagingCenter.Send<MySwitcher, string[]> (this, "Change Switcher", new string[]{"White","ON"});
				else 
					MessagingCenter.Send<MySwitcher, string[]> (this, "Change Switcher", new string[]{"Black","ON"});
					
			};
			option1Label.GestureRecognizers.Add(option1Action);

			//add gesture to option2 image
			var option2Action = new TapGestureRecognizer();
			option2Action.TappedCallback += (s, e) => {
				if(SwitcherType=="ON_OFF")
					picker.updatePickerState(false);
				else if(SwitcherType=="White Player Switch")
					MessagingCenter.Send<MySwitcher, string[]> (this, "Change Switcher",  new string[]{"White","OFF"});
				else 
					MessagingCenter.Send<MySwitcher, string[]> (this, "Change Switcher",  new string[]{"Black","OFF"});
					
			};
			option2Label.GestureRecognizers.Add(option2Action);

			option1Label.BindingContext = this;
			option1Label.SetBinding (MyLabel.TextProperty, new Binding ("Option1"));

			option2Label.BindingContext = this;
			option2Label.SetBinding (MyLabel.TextProperty, new Binding ("Option2"));

			this.BackgroundColor = Color.Transparent;

			//add components to the relative layout 
			//first add option1 background and option2 background
			this.Children.Add (option1, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);
			this.Children.Add (option2, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);

			//then add option1 label and option2 label
			this.Children.Add (option1Label, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);

			this.Children.Add (option2Label, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);
		}

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}

