using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Player wizard 2 screen to choose your chess style 
	/// </summary>
	public class PlayerWizard_2Screen : ContentPage
	{
		int avatarHeight;
		Global.CHESS_STYLE chessStyle;

		/// <summary>
		/// Create player wizard 2 screen to choose your chess style
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		/// <param name="isInit">Is it the first time to open the game.</param>
		public PlayerWizard_2Screen (int widthDP,int heightDP,bool isInit)
		{
			bool isInitialization=isInit;

			avatarHeight = heightDP / 12;
			//=================Define components=====================
			Label theme_1Label = new Label {
				Text = "Theme 1",
				TextColor = Color.Black
			};
			Label theme_2Label = new Label {
				Text = "Theme 2",
				TextColor = Color.Black
			};

			MyButton backButton = new MyButton {
				Text="back",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._LEFT_ARROW
			};

			backButton.Clicked += (s, e) => {
				Navigation.PopModalAsync();
			};

			MyButton nextButton = new MyButton {
				Text="next",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._RIGHT_ARROW
			};
			nextButton.Clicked += (s, e) => {
				//Go to the next configuration page
				ChessUser.getInstance().ChessStyle=chessStyle;
				Navigation.PushModalAsync(new PlayerWizard_3Screen(widthDP,heightDP,true));
			};

			MyButton finishButton = new MyButton {
				Text="finish",
				TextColor=Color.FromHex("#C6BF95"),
				BackgroundColor=Color.Black,
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25
			};
			finishButton.Clicked += (s, e) => {
				ChessUser.getInstance().ChessStyle=chessStyle;
				DependencyService.Get<UserInterface>().saveUser(ChessUser.getInstance());
				//if the first time to open the game then continue in configuration pages
				//else go to main menu
				if(!isInitialization)
					Navigation.PopModalAsync();
				else
					Navigation.PushModalAsync(new NavigationPage(new TitleScreenPage(widthDP,heightDP)));
			};

			Image front_2_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT2_BACKGROUND,
			};

			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};

			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};

			MyLabel selectAvatarLabel = new MyLabel{
				Text="Select Your Chess Style",
				TextColor=Color.Black,
				FontSize=30,
				XAlign=TextAlignment.Center
			};

			StackLayout chessStylesLayouts = new StackLayout {
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout chessStyle_1 = new StackLayout {
				BackgroundColor = Color.FromHex("#CBB77A"),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout chessStyle_2 = new StackLayout {
				BackgroundColor = Color.FromHex("#CBB77A"),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			List<Image> chessStyle_1_Pieces = new List<Image> ();
			List<Image> chessStyle_2_Pieces = new List<Image> ();
			fillImages (chessStyle_1_Pieces,0);
			fillImages (chessStyle_2_Pieces,1);

			for (int i = 0; i < 6; i++) {
				chessStyle_1.Children.Add (new StackLayout{
					Padding=new Thickness(0,10,0,10),
					Orientation=StackOrientation.Vertical,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Children={chessStyle_1_Pieces[i],new Label{Text=getCardNameFromIndex(i),TextColor=Color.Black,HorizontalOptions=LayoutOptions.Center}}
				});
				chessStyle_2.Children.Add (new StackLayout{
					Padding=new Thickness(0,10,0,10),
					Orientation=StackOrientation.Vertical,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Children={chessStyle_2_Pieces[i],new Label{Text=getCardNameFromIndex(i),TextColor=Color.Black,HorizontalOptions=LayoutOptions.Center}}
				});
				//add listner to the image
				var tapGestureRecognizer1 = new TapGestureRecognizer();
				tapGestureRecognizer1.TappedCallback += (s, e) => {
					if(chessStyle==Global.CHESS_STYLE._KIDS)
						chessStyle_2_Pieces[0].Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS)];
					chessStyle=Global.CHESS_STYLE._ADULT;
					chessStyle_1_Pieces[0].Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT)];
				};
				chessStyle_1_Pieces[i].GestureRecognizers.Add(tapGestureRecognizer1);
				//add listner to the image
				var tapGestureRecognizer2 = new TapGestureRecognizer();
				tapGestureRecognizer2.TappedCallback += (s, e) => {
					if(chessStyle==Global.CHESS_STYLE._ADULT)
						chessStyle_1_Pieces[0].Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT)];
					chessStyle=Global.CHESS_STYLE._KIDS;
					chessStyle_2_Pieces[0].Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS)];
				};
				chessStyle_2_Pieces[i].GestureRecognizers.Add(tapGestureRecognizer2);
			}
			chessStylesLayouts.Children.Add (theme_1Label);
			chessStylesLayouts.Children.Add (chessStyle_1);
			chessStylesLayouts.Children.Add (theme_2Label);
			chessStylesLayouts.Children.Add (chessStyle_2);

			ScrollView scrollView = new ScrollView{
				BackgroundColor=Color.Transparent,
				Orientation=ScrollOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Content = new ScrollView{
					BackgroundColor=Color.Transparent,
					Orientation=ScrollOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Content = chessStylesLayouts
				}
			};
			scrollView.IsClippedToBounds=true;

			#region buttons contaier
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_2_Container.Children.Add (scrollView, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);			
			#endregion


			#region front container
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_1_Container.Children.Add (front_2_Background, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/32); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4 + parent.Height*1f/32); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*22f/32); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2 - parent.Height*2f/32); })
			);

			front_1_Container.Children.Add (selectAvatarLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);

			front_1_Container.Children.Add (backButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);

			front_1_Container.Children.Add (finishButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (nextButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);


			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			if (!isInitialization)
				nextButton.IsVisible = false;
			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}

		/// <summary>
		/// Fills the list of chess pieces.
		/// </summary>
		/// <param name="list">List.</param>
		/// <param name="i">The index.</param>
		public void fillImages(List<Image> list,int i){
			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});

			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});

			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});
			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});
			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});
			list.Add (new Image{
				Aspect = Aspect.AspectFit,
				WidthRequest=avatarHeight*2,
				HeightRequest=avatarHeight*2,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global.cardToImageMap [Global.getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER1,false,i==0?Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS)]
			});
		}

		/// <summary>
		/// Gets the card name from index.
		/// </summary>
		/// <returns>The card name from index.</returns>
		/// <param name="i">The index.</param>
		public string getCardNameFromIndex(int i){
			if (i == 0)
				return "King";
			else if (i == 1)
				return "Rook";
			else if (i == 2)
				return "Queen";
			else if (i == 3)
				return "Knight";
			else if (i == 4)
				return "Pawn";
			return "Bishop";

		}
	}

}

