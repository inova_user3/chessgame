using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Player wizard 1 screen:
	/// Contains buttons to select the skill level.
	/// </summary>
	public class PlayerWizard_1Screen : ContentPage
	{
		Global.SKILL_LEVEL skillLevel;

		/// <summary>
		/// Create player wizard 1 screen to choose your skill level 
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		/// <param name="isInit">Is the first time to open this screen.</param>
		public PlayerWizard_1Screen (int widthDP,int heightDP,bool isInit)
		{

			bool isInitialization = isInit;
			//=================Define components=====================
			MyButton beginnerButton = new MyButton {
				Text="Beginner",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			MyButton beginnerDescriptionButton = new MyButton {
				Text="I am new to chess",
				TextColor=Color.FromHex("#F2ECBE"),
				BackgroundColor=Color.FromHex("#9973694B"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			beginnerButton.Clicked += (s, e) => {
				skillLevel=Global.SKILL_LEVEL._BEGINNER;
			};
			MyButton intermediateButton = new MyButton {
				Text="Intermediate",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20
			};
			MyButton intermediateDescriptionButton = new MyButton {
				Text="I know the rules of the chess",
				TextColor=Color.FromHex("#F2ECBE"),
				BackgroundColor=Color.FromHex("#9973694B"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			intermediateButton.Clicked += (s, e) => {
				skillLevel=Global.SKILL_LEVEL._INTERMEDIATE;
			};
			MyButton expertButton = new MyButton {
				Text="Expert",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20
			};
			MyButton expertDescriptionButton = new MyButton {
				Text="I have played many games of chess",
				TextColor=Color.FromHex("#F2ECBE"),
				BackgroundColor=Color.FromHex("#9973694B"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			expertButton.Clicked += (s, e) => {
				skillLevel=Global.SKILL_LEVEL._EXPERT;
			};
			MyButton nextButton = new MyButton {
				Text="next",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._RIGHT_ARROW
			};
			nextButton.Clicked += (s, e) => {
				//Go to the next configuration page
				ChessUser.getInstance().SkillLevel=skillLevel;
				Navigation.PushModalAsync(new PlayerWizard_2Screen(widthDP,heightDP,true));
			};

			MyButton finishButton = new MyButton {
				Text="finish",
				TextColor=Color.FromHex("#C6BF95"),
				BackgroundColor=Color.Black,
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25
			};
			finishButton.Clicked += (s, e) => {
				ChessUser.getInstance().SkillLevel=skillLevel;
				DependencyService.Get<UserInterface>().saveUser(ChessUser.getInstance());
				//if the first time to open the game then continue in configuration pages
				//else go to main menu
				if(!isInitialization)
					Navigation.PopModalAsync();
				else
					Navigation.PushModalAsync(new NavigationPage(new TitleScreenPage(widthDP,heightDP)));
			};

			MyButton backButton = new MyButton {
				Text="back",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				IsVisible=false,
				ButtonImage=Global._LEFT_ARROW
			};

			backButton.Clicked += (s, e) => {
				Navigation.PopModalAsync();
			};

			Image front_2_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT2_BACKGROUND,
			};

			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};

			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};

			MyLabel skillLevelLabel = new MyLabel{
				Text="Select Your Chess Skill Level",
				TextColor=Color.Black,
				FontSize=30,
				XAlign=TextAlignment.Center
			};
					
			#region buttons contaier
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			//left
			front_2_Container.Children.Add (beginnerButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*8f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			//right
			front_2_Container.Children.Add (beginnerDescriptionButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*10f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*16f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			//left
			front_2_Container.Children.Add (intermediateButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*8f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			//right
			front_2_Container.Children.Add (intermediateDescriptionButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*10f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*16f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			//left
			front_2_Container.Children.Add (expertButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*8f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			//right
			front_2_Container.Children.Add (expertDescriptionButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*10f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/10); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*16f/27); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/5); })
			);
			#endregion


			#region front container
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_1_Container.Children.Add (front_2_Background, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			front_1_Container.Children.Add (skillLevelLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (finishButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (nextButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (backButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);

			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);

			if (!isInitialization) {
				nextButton.IsVisible = false;
				backButton.IsVisible = true;
			}
			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}
	}
}

