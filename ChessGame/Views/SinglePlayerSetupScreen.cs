using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Single player setup screen.
	/// </summary>
	public class SinglePlayerSetupScreen : ContentPage
	{
		/// <summary>
		/// Create single player setup screen where you can set :
		/// number of cards, match timer ,move time and computer level for both white and black player.
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen heigh.</param>
		public SinglePlayerSetupScreen (int widthDP,int heightDP)
		{
//			NavigationPage.SetHasNavigationBar(this, false);

			MessagingCenter.Unsubscribe<GameState,ChessEngine.MATCH_RESULT> (this, "Match ended");
			MessagingCenter.Unsubscribe<ChessPiece> (this, "Promote");

			var user = ChessUser.getInstance();
			//=================Define components=====================
			ChessPicker whiteMatchTimerPicker = new ChessPicker {IsVisible=false};
			ChessPicker whiteMoveTimerPicker = new ChessPicker {IsVisible=false};
			ChessPicker blackMatchTimerPicker = new ChessPicker {IsVisible=false};
			ChessPicker blackMoveTimerPicker = new ChessPicker {IsVisible=false};


			SkillStepper whiteSkillLevel = new SkillStepper{
				Value=0,
				MinValue=0,
				MaxValue=4,
				IsVisible=false
			};

			SkillStepper blackSkillLevel = new SkillStepper{
				Value=0,
				MinValue=0,
				MaxValue=4,
				IsVisible=false
			};


			MyButton whiteMatchTimerButton = new MyButton{
				Text="0",
				TextColor=Color.FromHex("#F3ECBE"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=15,
				ButtonBackgroundImage=Global._WHITE_PLAYER_TIMER_BACKGROUND
			};
			whiteMatchTimerButton.BindingContext = whiteMatchTimerPicker;
			whiteMatchTimerButton.SetBinding (MyButton.TextProperty, new Binding ("TimerValue"));

			whiteMatchTimerButton.Clicked += (s, e) => {
				whiteMatchTimerPicker.IsVisible=true;
			};

			MyButton whiteMoveTimerButton = new MyButton{ 
				Text="1",
				TextColor=Color.FromHex("#F3ECBE"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=15,
				ButtonBackgroundImage=Global._WHITE_PLAYER_TIMER_BACKGROUND
			};
			whiteMoveTimerButton.BindingContext = whiteMoveTimerPicker;
			whiteMoveTimerButton.SetBinding (MyButton.TextProperty, new Binding ("TimerValue"));
		
			whiteMoveTimerButton.Clicked += (s, e) => {
				whiteMoveTimerPicker.IsVisible=true;
			};
			MyButton blackMatchTimerButton = new MyButton{ 
				Text="2",
				TextColor=Color.FromHex("#020202"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=15,
				ButtonBackgroundImage=Global._BLACK_PLAYER_TIMER_BACKGROUND
			};
			blackMatchTimerButton.BindingContext = blackMatchTimerPicker;
			blackMatchTimerButton.SetBinding (MyButton.TextProperty, new Binding ("TimerValue"));

			blackMatchTimerButton.Clicked += (s, e) => {
				blackMatchTimerPicker.IsVisible=true;
			};
			MyButton blackMoveTimerButton = new MyButton{
				Text="3",
				TextColor=Color.FromHex("#020202"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=15,
				ButtonBackgroundImage=Global._BLACK_PLAYER_TIMER_BACKGROUND

			};
			blackMoveTimerButton.BindingContext = blackMoveTimerPicker;
			blackMoveTimerButton.SetBinding (MyButton.TextProperty, new Binding ("TimerValue"));

			blackMoveTimerButton.Clicked += (s, e) => {
				blackMoveTimerPicker.IsVisible=true;
			};

			Image separator=new Image {
				Aspect = Aspect.Fill,
				Source="separator"
			};


			MyButton blackLabel = new MyButton {
				Text="Black Player",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#F3ECBE"),
				BorderSize=5,
				CornerRadius =10,
				FontSize=20
			};

			MyButton whiteLabel = new MyButton {
				Text="White Player",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#020202"),
				BorderSize=5,
				CornerRadius =10,
				FontSize=20,
			};

			Image frontBackground=new Image {
				Aspect = Aspect.Fill,
				Source=Global._SINGLE_PLAYER_SETUP_FRONT
			};

			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._SINGLE_PLAYER_SETUP_TITLE
			};

			Image startMatchButton=new Image {
				Aspect = Aspect.Fill,
				Source=Global._START_MATCH_BUTTON
			};

			MyLabel blackCardsLabel = new MyLabel{
				BackgroundColor=Color.FromHex("#99DDD37E"),
				Text="How Many Cards Can Black Player Draw?",
				TextColor=Color.Black,
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};

			MyLabel whiteCardsLabel = new MyLabel{
				BackgroundColor=Color.FromHex("#99645430"),
				Text="How Many Cards Can White Player Draw?",
				TextColor=Color.FromHex("#F3ECBE"),
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};

			MyLabel blackMatchTimerLabel = new MyLabel{
				Text="Time Limit For The Game",
				TextColor=Color.Black,
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};
			MyLabel blackMoveTimerLabel = new MyLabel{
				Text="Time Limit Per Move",
				TextColor=Color.Black,
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};

			MyLabel whiteMatchTimerLabel = new MyLabel{
				Text="Time Limit For The Game",
				TextColor=Color.FromHex("#F3ECBE"),
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center

			};

			MyLabel whiteMoveTimerLabel = new MyLabel{
				Text="Time Limit Per Move",
				TextColor=Color.FromHex("#F3ECBE"),
				FontSize=20,
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};

			MyButton returnButton = new MyButton {
				Text="return to main menu",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
				ButtonImage=Global._LEFT_ARROW
			};
			returnButton.Clicked += (s, e) => {
				Navigation.PopAsync ();
			};



			//===============Define stacklayout that contains logo============
			StackLayout blackPlayer= new StackLayout	{
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Vertical,
				VerticalOptions=LayoutOptions.Center,
				Children = {blackLabel}
			};	
			StackLayout whitePlayer= new StackLayout	{
				BackgroundColor = Color.Transparent,
				VerticalOptions=LayoutOptions.Center,
				Orientation = StackOrientation.Vertical,
				Children = {whiteLabel}
			};

			StackLayout blackMatchTimer= new StackLayout	{
				BackgroundColor = Color.FromHex("#99DDD37E"),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions=LayoutOptions.Center,
			};

			StackLayout whiteMatchTimer= new StackLayout	{
				BackgroundColor = Color.FromHex("#99645430"),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions=LayoutOptions.Center,
			};

			StackLayout blackMoveTimer= new StackLayout	{
				BackgroundColor = Color.FromHex("#99DDD37E"),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions=LayoutOptions.Center,
			};	

			StackLayout whiteMoveTimer= new StackLayout	{
				BackgroundColor = Color.FromHex("#99645430"),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions=LayoutOptions.Center,
			};	

			MyStepper whiteStepper = new MyStepper(){
				Value=3,
				MinValue=1,
				MaxValue=9,
			};

			MyStepper blackStepper = new MyStepper(){
				Value=3,
				MinValue=1,
				MaxValue=9,
			};

			MySwitcher whiteSwitcher = new MySwitcher(null){
				Option1="You",
				Option2="Computer",
				Option1Selected=true,
				Option2Selected=false,
				Option1Image=Global._OPTION_1SELECTED,
				Option2Image=Global._OPTION_2UNSELECTED,
				SwitcherType="White Player Switch",

			};

			MySwitcher blackSwitcher = new MySwitcher(null){
				Option1="You",
				Option2="Computer",
				Option1Selected=true,
				Option2Selected=false,
				Option1Image=Global._OPTION_1SELECTED,
				Option2Image=Global._OPTION_2UNSELECTED,
				SwitcherType="Black Player Switch",

			};

			//Alert for changing the switcher state
			MessagingCenter.Subscribe<MySwitcher, string[]> (this, "Change Switcher", (sender, switcherType) => {
				MySwitcher switcher;
				SkillStepper skillLevel;
				if(switcherType[0]=="White"){
					applySwitchMove(switcherType[1],whiteSwitcher,blackSwitcher,whiteSkillLevel);
				}
				else{
					applySwitchMove(switcherType[1],blackSwitcher,whiteSwitcher,blackSkillLevel);
				}

			});

			//add listner to the start match image
			var actionListner = new TapGestureRecognizer();
			actionListner.TappedCallback += async(s, e) => {
//				MessagingCenter.Unsubscribe<MySwitcher,string[]> (this, "Change Switcher");
				int whitePlayerCardsNum=whiteStepper.Value;
				int blackPlayerCardsNum=blackStepper.Value;
				int whitePlayerMoveTimerSecs=timeToSeconds(whiteMoveTimerButton.Text);
				int blackPlayerMoveTimerSecs=timeToSeconds(blackMoveTimerButton.Text);
				int whitePlayerMatchTimerSecs=timeToSeconds(whiteMatchTimerButton.Text);
				int blackPlayerMatchTimerSecs=timeToSeconds(blackMatchTimerButton.Text);
				int computerSkillLevel=-1;
				if(whiteSkillLevel.IsVisible)
					computerSkillLevel=whiteSkillLevel.Value;
				else if (blackSkillLevel.IsVisible)
					computerSkillLevel=blackSkillLevel.Value;

				if(whiteSwitcher.Option1Selected && blackSwitcher.Option1Selected)
					ChessEngine.initGameState(Global.GAME_TYPE._PLAYER_VS_PLAYER,whitePlayerCardsNum,blackPlayerCardsNum,
						whitePlayerMoveTimerSecs,blackPlayerMoveTimerSecs,whitePlayerMatchTimerSecs,blackPlayerMatchTimerSecs,computerSkillLevel);
				else
					ChessEngine.initGameState(Global.GAME_TYPE._PLAYER_VS_COMPUTER,whitePlayerCardsNum,blackPlayerCardsNum,
						whitePlayerMoveTimerSecs,blackPlayerMoveTimerSecs,whitePlayerMatchTimerSecs,blackPlayerMatchTimerSecs,computerSkillLevel);
				ChessEngine.initializeChessBoard ();
				GamePage gamePage=new GamePage(widthDP,heightDP);
				gamePage.BackgroundImage=Global._BOARD_PAGE_BACKGROUND;
				await Navigation.PushAsync(gamePage);
//				await Navigation.PushModalAsync(gamePage);


			};


			startMatchButton.GestureRecognizers.Add(actionListner);
			//===============Define relativelayout that contains buttons and background============

			RelativeLayout buttons = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};

			/////////White///////////


			buttons.Children.Add (whiteStepper, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (whiteCardsLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*4f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (whiteMatchTimer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*12f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (whiteMoveTimer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*12f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);

			buttons.Children.Add (whiteMatchTimerButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*10f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8.5/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2/17); })
			);
			buttons.Children.Add (whiteMoveTimerButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*10f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12.5/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2/17); })
			);

			buttons.Children.Add (whiteMatchTimerLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (whiteMoveTimerLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);

			////////////Black/////////////



			buttons.Children.Add (blackStepper, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*25/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (blackCardsLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*15/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (blackMatchTimer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*15/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*12/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (blackMoveTimer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*15/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*12/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);

			buttons.Children.Add (blackMatchTimerButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*24f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8.5/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2/17); })
			);
			buttons.Children.Add (blackMoveTimerButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*24f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12.5/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2/17); })
			);

			buttons.Children.Add (blackMatchTimerLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*15f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*8/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);
			buttons.Children.Add (blackMoveTimerLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*15f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*12/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/28); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/17); })
			);

			//=====================================================================================
			RelativeLayout frontContainer = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			frontContainer.Children.Add (separator, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*14f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/13); })
			);


			frontContainer.Children.Add (buttons, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*14f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/13); })
			);

			frontContainer.Children.Add (whitePlayer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*11f/48); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/12); })
			);

			frontContainer.Children.Add (blackPlayer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*11f/48); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/12); })
			);

			frontContainer.Children.Add (whiteSwitcher, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/48); })
			);

			frontContainer.Children.Add (blackSwitcher, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/48); })
			);

			frontContainer.Children.Add (returnButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/24); })
			);

			frontContainer.Children.Add (whiteSkillLevel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4.5f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); })
			);

			frontContainer.Children.Add (blackSkillLevel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*9f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*4.5f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); })
			);

			//=====================================================================================
			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};

			container.Children.Add (frontBackground,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*11f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*10f/13); })
			);

			container.Children.Add (frontContainer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*11f/13); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*10f/13); })
			);

			container.Children.Add (titleImage, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/5); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/(13*6)); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/5); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/13); })
			);


			container.Children.Add (startMatchButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*45f/52); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/26); })
			);


			container.Children.Add (whiteMatchTimerPicker, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);
			container.Children.Add (whiteMoveTimerPicker, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);
			container.Children.Add (blackMatchTimerPicker, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);
			container.Children.Add (blackMoveTimerPicker, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}

		/// <summary>
		/// Return time in seconds
		/// </summary>
		/// <returns>The to seconds.</returns>
		/// <param name="time">Time.</param>
		public int timeToSeconds(string time){
			string []mins_secs=time.Split (':');
			int res = 0;
			res += 60*Convert.ToInt32 (mins_secs[0]);
			res += Convert.ToInt32 (mins_secs [1]);
			return res;
		}

		/// <summary>
		/// Applies the switch move.
		/// </summary>
		/// <param name="state">Switch state on or off.</param>
		/// <param name="switcher1">Switcher1.</param>
		/// <param name="switcher2">Switcher2.</param>
		/// <param name="skillLevel">Skill level.</param>
		public void applySwitchMove(string state,MySwitcher switcher1,MySwitcher switcher2,SkillStepper skillLevel){
			if(state=="ON"){

				if(!switcher1.Option1Selected){
					skillLevel.IsVisible=false;
					switcher1.Option1Selected=true;
					switcher1.Option2Selected=false;
					switcher1.Option1Image=Global._OPTION_1SELECTED;
					switcher1.Option2Image=Global._OPTION_2UNSELECTED;
					switcher1.Option2Label.TextColor=Color.FromHex("#F3ECBE");
					switcher1.Option1Label.TextColor=Color.Black;
				}
			}
			else{

				if(switcher2.Option1Selected&&!switcher1.Option2Selected){
					skillLevel.IsVisible=true;
					switcher1.Option2Selected=true;
					switcher1.Option1Selected=false;
					switcher1.Option1Image=Global._OPTION_1UNSELECTED;
					switcher1.Option2Image=Global._OPTION_2SELECTED;
					switcher1.Option1Label.TextColor=Color.FromHex("#F3ECBE");
					switcher1.Option2Label.TextColor=Color.Black;
				}
			}
		}
	}
}
	