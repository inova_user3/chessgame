using System;
using Xamarin.Forms;

namespace SharedViews
{
	/// <summary>
	/// Button propertis that will be rendered in native iOS and native Android Renderers.
	/// </summary>
	public class MyButton : Button
	{

		int androidButtonType;
		public int AndroidButtonType{
			get{ return androidButtonType;}
			set {androidButtonType=value;}
		}
			
		/// <summary>
		///Font size of the text of the button.
		/// </summary>
		public static readonly BindableProperty fontSizeProperty = BindableProperty.Create<MyButton,int>(p => p.FontSize,0);

		public int FontSize{
			get{ return (int)base.GetValue(fontSizeProperty);}
			set {base.SetValue(fontSizeProperty,value);}
		}

		/// <summary>
		///The radius of each corner of the button.
		/// </summary>
		public static readonly BindableProperty cornerRadiusProperty = BindableProperty.Create<MyButton,float>(p => p.CornerRadius,0f);

		public float CornerRadius{
			get{ return (float)base.GetValue(cornerRadiusProperty);}
			set {base.SetValue(cornerRadiusProperty,value);}
		}

		/// <summary>
		///Boarder Color for the button.
		/// </summary>
		public static readonly BindableProperty borderColorProperty = BindableProperty.Create<MyButton,Color>(p => p.borderColor,Color.Transparent);

		public Color borderColor{
			get{ return (Color)base.GetValue(borderColorProperty);}
			set {base.SetValue(borderColorProperty,value);}
		}

		/// <summary>
		/// Boarder size of the button.
		/// </summary>
		public static readonly BindableProperty borderSizeProperty = BindableProperty.Create<MyButton,float>(p => p.BorderSize,0f);

		public float BorderSize{
			get{ return (float)base.GetValue(borderSizeProperty);}
			set {base.SetValue(borderSizeProperty,value);}
		}

		/// <summary>
		///Image inside the button not background Image.
		/// </summary>
		public static readonly BindableProperty buttonImageProperty = BindableProperty.Create<MyButton,string>(p => p.ButtonImage,null);

		public string ButtonImage{
			get{ return (string)base.GetValue(buttonImageProperty);}
			set {base.SetValue(buttonImageProperty,value);}
		}

		/// <summary>
		/// The button background image property.
		/// </summary>
		public static readonly BindableProperty buttonBackgroundImageProperty = BindableProperty.Create<MyButton,string>(p => p.ButtonBackgroundImage,null);

		public string ButtonBackgroundImage{
			get{ return (string)base.GetValue(buttonBackgroundImageProperty);}
			set {base.SetValue(buttonBackgroundImageProperty,value);}
		}
	}
}

