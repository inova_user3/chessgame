using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// The chess game page that contains the cards layout and the chess board layout.
	/// </summary>
	public class GamePage : ContentPage
	{
		int widthDP;
		int heightDP;

		/// <summary>
		/// Craete the chess game page that contains the cards layout and the chess board layout.
		/// </summary>
		/// <param name="widthDP">Screen width D.</param>
		/// <param name="heightDP">Screen height D.</param>
		public GamePage (int widthDP,int heightDP)	{
//			NavigationPage.SetHasNavigationBar(this, false);
//			NavigationPage.BarBackgroundColor=Color.Transparent;
				Global._GRID_CELL_SIZE = heightDP / 12;

			this.widthDP = widthDP;
			this.heightDP = heightDP;
			StackLayout components = new StackLayout
			{
				VerticalOptions = LayoutOptions.Start,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center,
				Spacing=0,
				Padding = new Thickness(
					Global._GRID_CELL_SIZE/2,
					0,
					Global._GRID_CELL_SIZE/2,
					Global._GRID_CELL_SIZE/2),
				Children = {
					boardLayout() ,
					cardsLayout()
				}
			};	

			MyButton backButton = new MyButton {
				Text="back",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._LEFT_ARROW,
				WidthRequest=200	
			};

			backButton.Clicked += (s, e) => {
				Navigation.PopModalAsync();
			};	

			StackLayout backWithComponents = new StackLayout
			{
				VerticalOptions = LayoutOptions.Center,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				Spacing=10,
				Padding = new Thickness(
					0,Global._GRID_CELL_SIZE/2,0,0),
				Children = {
//					backButton,
					components
				}
			};	
			RelativeLayout layout = new RelativeLayout ();
		
			layout.Children.Add (backButton, 
				Constraint.Constant (0), 
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => { return parent.Width; }),
				Constraint.RelativeToParent ((parent) => { return parent.Height; }));

			Content = backWithComponents;

			//start match and move timer for the first player
			ChessEngine.startMoveTimerThread ();
			ChessEngine.startMatchTimerThread ();
		}



		List<List<BoardCell>> boardObjs;

		/// <summary>
		/// Inits the boardcell array.
		/// </summary>
		public void initBoard(){
			boardObjs=new List<List<BoardCell>>();
			BoardCell[,]board=ChessEngine.gState.Board.getBoardCells ();
			for (int i = 0; i < Global._ROWSNUM; i++) {
				List<BoardCell> row=new List<BoardCell>();
				for (int j = 0; j < Global._COLUMNSNUM; j++) {
					row.Add (board[i,j]);
				}
				boardObjs.Add (row);
			}
		}

		/// <summary>
		/// Create the chess grid layout that contains 8*8 board cell view that indicates the chess board cells.
		/// </summary>
		/// <returns>The grid.</returns>
		public ListView chessGrid(){
			initBoard ();
			int row = 0;
			MyList listView = new MyList
			{
				ShouldScroll=false,
				ItemsSource = boardObjs,
				ItemTemplate = new DataTemplate(() =>
				{
					BoardCellView [] boardRow =new BoardCellView[8];
					for(int i=0;i<boardObjs[row].Count;i++){
						boardRow[i]=new BoardCellView(row,i,boardObjs[row][i]){
								WidthRequest = Global._GRID_CELL_SIZE, 
								HeightRequest= Global._GRID_CELL_SIZE
							};
					}
					row++;
					StackLayout rowView=new StackLayout
					{
						IsEnabled=false,
						BackgroundColor=Color.White,
						VerticalOptions=LayoutOptions.Fill,
						Spacing=0,
						Padding = new Thickness(0,0,0,0),
						Orientation = StackOrientation.Horizontal,
						Children = 
						{
							boardRow[0],boardRow[1],boardRow[2],boardRow[3],boardRow[4],boardRow[5],boardRow[6],boardRow[7]
						}
					};
					return new ViewCell
					{
						View = rowView
					};
				})
			};
			listView.HorizontalOptions = LayoutOptions.Fill;
			listView.VerticalOptions = LayoutOptions.Fill;
			listView.BackgroundColor = Color.Transparent;
			
			listView.RowHeight = Global._GRID_CELL_SIZE;
			listView.ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
				if (e.SelectedItem == null)
					return; 
				((ListView)sender).SelectedItem = null;
			};
			// set the minimum width
			listView.WidthRequest = ((Global._GRID_CELL_SIZE )*Global._COLUMNSNUM);
			listView.MinimumWidthRequest = ((Global._GRID_CELL_SIZE )*Global._COLUMNSNUM);

			// Alert message for end match
			MessagingCenter.Subscribe<GameState,ChessEngine.MATCH_RESULT> (this, "Match ended", async(sender,arg) => {

				if(arg==ChessEngine.MATCH_RESULT.PLAYER1_WIN)
					await DisplayAlert ("Game Over", "Player 1 won the match", "OK");
				else 
					await DisplayAlert ("Game Over", "Player 2 won the match", "OK");

				MessagingCenter.Unsubscribe<GameState,ChessEngine.MATCH_RESULT> (this, "Match ended");
				MessagingCenter.Unsubscribe<ChessPiece> (this, "Promote");
				Navigation.PushModalAsync(new NavigationPage(new TitleScreenPage(widthDP,heightDP)));

			});
			// Alert message for promoting a piece
			MessagingCenter.Subscribe<ChessPiece> (this, "Promote", async(sender) => {
				var action = await DisplayActionSheet ("Promotion Move", "Cancel", null, "QUEEN", "ROOK", "BISHOP", "KNIGHT");
				ChessEngine.do_promotion (sender, action);
			});

			return listView;
		}



		/// <summary>
		///	Create and return cards layout : user hand cards 
		/// and deck of cards contains the discarded cards and the drawn cards
		/// </summary>
		/// <returns>Card layout.</returns>
		public StackLayout cardsLayout(){
				
			#region cardsDeck layout 
			Image cardsDeckBackground=new Image {
				Aspect = Aspect.Fill,
				WidthRequest=widthDP- Global._GRID_CELL_SIZE*(10),
				HeightRequest=Global._GRID_CELL_SIZE*4,
				VerticalOptions=LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.Center,
				Source=Global._CARD_DECK_IMAGE,
			};

			Image card1=new Image {
				Aspect = Aspect.Fill,
				WidthRequest=Global._GRID_CELL_SIZE*2,
				HeightRequest=Global._GRID_CELL_SIZE*2.5,
				Source=Global._CARD_BACK_IMAGE,
			};
			Image card2=new Image {
				Aspect = Aspect.Fill,
				WidthRequest=Global._GRID_CELL_SIZE*2,
				HeightRequest=Global._GRID_CELL_SIZE*2.5,
				Source=Global._CARD_BACK_IMAGE,
			};
		
			RelativeLayout cardsDeckLayout = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Center,
				VerticalOptions=LayoutOptions.Start
			};

			CardView drawnCard=new CardView(ChessEngine.gState.nextDrawnCard);

			cardsDeckLayout.Children.Add (cardsDeckBackground, 
				Constraint.Constant (0), 
				Constraint.Constant (0),
				Constraint.Constant (cardsDeckBackground.WidthRequest), 
				Constraint.Constant (cardsDeckBackground.HeightRequest));

			cardsDeckLayout.Children.Add (card1, 
				Constraint.Constant (Global._GRID_CELL_SIZE/2), 
				Constraint.Constant (Global._GRID_CELL_SIZE/2),
				Constraint.Constant (card1.WidthRequest), 
				Constraint.Constant (card1.HeightRequest));

			cardsDeckLayout.Children.Add (card2, 
				Constraint.Constant (2*(Global._GRID_CELL_SIZE/2)), 
				Constraint.Constant (2*(Global._GRID_CELL_SIZE/2)),
				Constraint.Constant (card1.WidthRequest), 
				Constraint.Constant (card1.HeightRequest));
		
			cardsDeckLayout.Children.Add (drawnCard, 
				Constraint.Constant (3*(Global._GRID_CELL_SIZE/2)), 
				Constraint.Constant (3*(Global._GRID_CELL_SIZE/2)),
				Constraint.Constant (card1.WidthRequest), 
				Constraint.Constant (card1.HeightRequest));

			#endregion

			#region cards layout 
			//player 1 cards scroll view
			StackLayout player1CardsLayout = new StackLayout {
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			List<StackLayout> layouts1=new List<StackLayout>();
			List<CardView> player1CardsImages = new List<CardView> ();
			int columnsNum=(int)((widthDP- Global._GRID_CELL_SIZE*(10))/(Global._GRID_CELL_SIZE*2));
			for(int i=0;i<ChessEngine.gState.Player1.PlayerCards.Count;i++){
				if(i%columnsNum==0){
					layouts1.Add(new StackLayout{
						BackgroundColor = Color.Transparent,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center
					});
					player1CardsLayout.Children.Add (layouts1[layouts1.Count-1]);
				}
				player1CardsImages.Add(new CardView (ChessEngine.gState.Player1.PlayerCards[i]));
				layouts1[layouts1.Count-1].Children.Add(player1CardsImages[i]);
			}

			//player 2 cards scroll view
			StackLayout player2CardsLayout = new StackLayout {
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			List<StackLayout> layouts2=new List<StackLayout>();
			List<CardView> player2CardsImages = new List<CardView> ();
			for(int i=0;i<ChessEngine.gState.Player2.PlayerCards.Count;i++){
				if(i%columnsNum==0){
					layouts2.Add(new StackLayout{
						BackgroundColor = Color.Transparent,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center
					});
					player2CardsLayout.Children.Add (layouts2[layouts2.Count-1]);
				}
				player2CardsImages.Add(new CardView (ChessEngine.gState.Player2.PlayerCards[i]));
				layouts2[layouts2.Count-1].Children.Add(player2CardsImages[i]);
			}

			var scrollView = new ScrollView {
				BackgroundColor=Color.Transparent,
				Orientation=ScrollOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				HeightRequest = Global._GRID_CELL_SIZE*6,
				MinimumHeightRequest = Global._GRID_CELL_SIZE*6,
				Content = player1CardsLayout
			};

			scrollView.IsClippedToBounds=true;

			// Alert message for switch between players cards
			MessagingCenter.Subscribe<GameState> (this, "Show Cards", (sender) => {
				if(ChessEngine.gState.CurrentPlayer.PlayerType==Global.PLAYER_TYPE._PLAYER1)
					scrollView.Content=player1CardsLayout;
				else
					scrollView.Content=player2CardsLayout;
			});
			#endregion

			#region move timer
			var moveTimerLabel=new Label {
				Text = "0",
				TextColor=Color.White,
				Font = Font.OfSize (Global._FONT_NAME,20),
				HorizontalOptions = LayoutOptions.Center,
			};
			moveTimerLabel.BindingContext = ChessEngine.gState;
			moveTimerLabel.SetBinding (Label.TextProperty, new Binding ("MoveTimer"));

			#endregion
		
			return new StackLayout	{
				BackgroundColor = Color.Transparent,
				Spacing = 10,
				Padding = new Thickness(Global._GRID_CELL_SIZE/2,0,0,0),
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.End,
				Orientation = StackOrientation.Vertical,
				Children = {
					moveTimerLabel,
					cardsDeckLayout,scrollView}
			};	
		}

		/// <summary>
		/// Create and return the chess board view with timers
		/// </summary>
		/// <returns>The layout.</returns>
		public StackLayout boardLayout(){
				var bottomPlayerLabel = new Label {
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					TextColor=Color.White,
					Font =Device.OnPlatform (
						iOS: Font.OfSize ("MarkerFelt-Thin", NamedSize.Medium),
						Android: Font.OfSize ("Droid Sans Mono", NamedSize.Medium),
						WinPhone: Font.OfSize ("Comic Sans MS", NamedSize.Medium)
					)
				};
			bottomPlayerLabel.BindingContext = ChessEngine.gState;
			bottomPlayerLabel.SetBinding (Label.TextProperty, new Binding ("Player1Label"));

				var topPlayerLabel = new Label {
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					TextColor=Color.White,
					Font = Device.OnPlatform (
						iOS: Font.OfSize ("MarkerFelt-Thin", NamedSize.Medium),
						Android: Font.OfSize ("Droid Sans Mono", NamedSize.Medium),
						WinPhone: Font.OfSize ("Comic Sans MS", NamedSize.Medium)
					)
				};

			topPlayerLabel.BindingContext = ChessEngine.gState;
			topPlayerLabel.SetBinding (Label.TextProperty, new Binding ("Player2Label"));

			#region match timer
			var player1MatchTimerLabel=new Label {
				Text = "0",
				TextColor=Color.White,
				Font = Font.OfSize (Global._FONT_NAME,20),
				HorizontalOptions = LayoutOptions.Center,
			};
			player1MatchTimerLabel.BindingContext = ChessEngine.gState;
			player1MatchTimerLabel.SetBinding (Label.TextProperty, new Binding ("Player1MatchTimer"));


			var player2MatchTimerLabel=new Label {
				Text = "0",
				TextColor=Color.White,
				Font = Font.OfSize (Global._FONT_NAME,20),
				HorizontalOptions = LayoutOptions.Center,
			};
			player2MatchTimerLabel.BindingContext = ChessEngine.gState;
			player2MatchTimerLabel.SetBinding (Label.TextProperty, new Binding ("Player2MatchTimer"));


			#endregion

			return new StackLayout {
				Spacing = 10,
				Padding=new Thickness(0,Global._GRID_CELL_SIZE/2,0,0),
				VerticalOptions = LayoutOptions.Start,
				Orientation = StackOrientation.Vertical,
				Children = {
				new StackLayout {
					Orientation=StackOrientation.Horizontal,
					HorizontalOptions=LayoutOptions.Center,
					Children={topPlayerLabel,new Label{Text="   "},player2MatchTimerLabel}}, 
				chessGrid () ,
				new StackLayout {
					Orientation=StackOrientation.Horizontal,
					HorizontalOptions=LayoutOptions.Center,
					Children={bottomPlayerLabel,new Label{Text="   "},player1MatchTimerLabel}}, 
				}
			};

		}

	}
		
}
	