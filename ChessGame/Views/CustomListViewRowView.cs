using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using SharedModel;

namespace SharedViews
{
	public class CustomListViewRowView :StackLayout
	{
		Label label;
		CustomListViewRow row;

		public CustomListViewRow Row{
			get { return row;}
			set { row = value;}
		}
		public CustomListViewRowView (CustomListViewRow _row)
		{
			this.row = _row;
			label = new Label{
				TextColor=Color.Black,
				HorizontalOptions=LayoutOptions.Center
			};

			//Binding Board cell with the image
			label.BindingContext = row;
			label.SetBinding(Label.TextProperty, new Binding("RowLabel"));

			this.Orientation = StackOrientation.Horizontal;
			this.HorizontalOptions = LayoutOptions.Center;	
			this.VerticalOptions = LayoutOptions.Center;
			this.Children.Add(label);
			this.BindingContext = row;
			this.SetBinding(StackLayout.BackgroundColorProperty, new Binding("RowColor"));
		}

	}
}

