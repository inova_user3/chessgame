using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;


namespace SharedViews
{
	/// <summary>
	/// Change settings screen.
	/// </summary>
	public class ChangeSettingsScreen : ContentPage
	{
		/// <summary>
		/// Create change settings page so that you can select the option you want to change and go to the specific page.
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		public ChangeSettingsScreen (int widthDP,int heightDP)
		{
			//=================Define components=====================

			//button to open avatar selector page
			MyButton chooseAvatarButton = new MyButton {
				Text="Choose Your Avatar",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			chooseAvatarButton.Clicked += (s, e) => {
				//go to avatar selector page
				Navigation.PushModalAsync(new PlayerWizard_3Screen(widthDP,heightDP,false));
			};

			//button to choose chess style from the predefined chess styles
			MyButton chooseThemeButton = new MyButton {
				Text="Choose Your Game Theme",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			chooseThemeButton.Clicked += (s, e) => {
				//go to style selector page
				Navigation.PushModalAsync(new PlayerWizard_2Screen(widthDP,heightDP,false));
			};

			//button to edit your info
			MyButton chooseNameLocButton = new MyButton {
				Text="Choose Your nickname&current location",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			chooseNameLocButton.Clicked += (s, e) => {
				//go to update your info page
				Navigation.PushModalAsync(new PlayerWizard_4Screen(widthDP,heightDP,false));
			};

			//button to change your skill level
			MyButton chooseSkillLevelButton = new MyButton {
				Text="Choose Your Skill Level",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
			};
			chooseSkillLevelButton.Clicked += (s, e) => {
				//go to skill level page
				Navigation.PushModalAsync(new PlayerWizard_1Screen(widthDP,heightDP,false));
			};

			// return to the main menu page
			MyButton returnButton = new MyButton {
				Text="return to main menu",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
				ButtonImage=Global._LEFT_ARROW
			};
			returnButton.Clicked += (s, e) => {
				//return to previous page
				Navigation.PopModalAsync();
			};

			//create background images 
			Image front_2_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT2_BACKGROUND,
			};
			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};
			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};

			#region buttons contaier
			//add buttons to the front container 
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_2_Container.Children.Add (chooseAvatarButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			front_2_Container.Children.Add (chooseThemeButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			front_2_Container.Children.Add (chooseNameLocButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*9f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			front_2_Container.Children.Add (chooseSkillLevelButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*13f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			#endregion


			#region front container
			//add containers to the page 
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent
			};
			front_1_Container.Children.Add (front_2_Background, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*17f/24); })
			);

			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*17f/24); })
			);

			front_1_Container.Children.Add (returnButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/24); })
			);


			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);

			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}
	}
}

