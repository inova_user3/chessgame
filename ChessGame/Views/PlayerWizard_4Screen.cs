using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Player wizard 4 screen:
	/// Contains editors to edit your nickname and city.
	/// </summary>
	public class PlayerWizard_4Screen : ContentPage
	{
		/// <summary>
		/// Create player wizard 4 screen to edit user nickname and city
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		/// <param name="isInit">Is it the first time to open the game .</param>
		public PlayerWizard_4Screen (int widthDP,int heightDP,bool isInit)
		{
			bool isInitialization=isInit;

			//=================Define components=====================

			MyLabel nickNameLabel = new MyLabel{
				Text="Choose your chess playing nickname",
				TextColor=Color.FromHex("#C6BF95"),
				FontSize=20,
				XAlign=TextAlignment.Center
			};
			MyLabel cityLabel = new MyLabel{
				Text="Choose city where you play chess",
				TextColor=Color.FromHex("#C6BF95"),
				FontSize=20,
				XAlign=TextAlignment.Center
			};
			Entry nickNameEditor = new Entry{
				Placeholder="Type your chess player nickname here",
				HorizontalOptions=LayoutOptions.Center,
				VerticalOptions=LayoutOptions.Center,
				BackgroundColor=Color.FromHex("#C6BF95"),
				TextColor=Color.Black


			};
			Entry cityEditor = new Entry{ 
				Placeholder="Type your city here",
				HorizontalOptions=LayoutOptions.Center,
				VerticalOptions=LayoutOptions.Center,
				BackgroundColor=Color.FromHex("#C6BF95"),
				TextColor=Color.Black

			};
			MyButton backButton = new MyButton {
				Text="back",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C6BF95"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25,
				ButtonImage=Global._LEFT_ARROW
			};
			backButton.Clicked += (s, e) => {
				ChessUser user=DependencyService.Get<UserInterface>().loadUser();
				Navigation.PopModalAsync();
			};

			MyButton finishButton = new MyButton {
				Text="finish",
				TextColor=Color.FromHex("#C6BF95"),
				BackgroundColor=Color.Black,
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=25
			};
			finishButton.Clicked += (s, e) => {
				ChessUser.getInstance().NickName=nickNameEditor.Text;
				ChessUser.getInstance().City=cityEditor.Text;
				DependencyService.Get<UserInterface>().saveUser(ChessUser.getInstance());
				//if the first time to open the game then continue in configuration pages
				//else go to main menu
				if(!isInitialization)
					Navigation.PopModalAsync();
				else
					Navigation.PushModalAsync(new NavigationPage(new TitleScreenPage(widthDP,heightDP)));
			};

			Image front_2_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT2_BACKGROUND,
			};

			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};

			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};

			#region buttons contaier
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_2_Container.Children.Add (nickNameLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); })
			);
			front_2_Container.Children.Add (nickNameEditor, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/9); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); })
			);
			front_2_Container.Children.Add (cityLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/9); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); })
			);
			front_2_Container.Children.Add (cityEditor, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/9); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); })
			);
			#endregion


			#region front container
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_1_Container.Children.Add (front_2_Background, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/8); })
			);
			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/8); })
			);

			front_1_Container.Children.Add (finishButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*5f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			front_1_Container.Children.Add (backButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*19f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);


			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);

			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}
	}
}

