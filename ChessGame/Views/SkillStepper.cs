using System;
using Xamarin.Forms;
using SharedModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SharedViews
{

	/// <summary>
	/// Custom stepper that contains up,down,background and label that indicates the current value
	/// </summary>
	public class SkillStepper :RelativeLayout, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		int value;
		string valueStr="Easiest";
		int maxValue;
		int minValue;

		public int Value {
			get { return value; }
			set
			{
				if (value==this.value)
				{
					return;
				}
				this.value = value;

				if(Value==0)
					ValueStr="Easiest";
				else if(Value==1)
					ValueStr= "A little skill";
				else if(Value==2)
					ValueStr= "Pretty good";
				else if(Value==3)
					ValueStr= "Very good";
				else 
					ValueStr= "Amazing skill";

				OnPropertyChanged("Value");
			}
		}

		public string ValueStr {
			get { return valueStr; }
			set
			{
				if (value==this.valueStr)
				{
					return;
				}
				this.valueStr = value;
				OnPropertyChanged("ValueStr");
			}
		}

		public int MinValue {
			get { return minValue; }
			set{ this.minValue = value; }
		}

		public int MaxValue {
			get { return maxValue; }
			set{ this.maxValue = value; }
		}

		/// <summary>
		/// create a stepper object that contains 2 buttons up and down and a label that indicates the current skill level value.
		/// </summary>
		public SkillStepper ()
		{
			Image upButton = new Image {
				Aspect = Aspect.Fill,
				Source=Global._UP_SWITCHER_BUTTON,
			};
			// add listner to up image
			var upAction = new TapGestureRecognizer();
			upAction.TappedCallback += (s, e) => {
				if(Value<MaxValue)
					Value++;
			};
			upButton.GestureRecognizers.Add(upAction);


			Image downButton = new Image {
				Aspect = Aspect.Fill,
				Source=Global._DOWN_SWITCHER_BUTTON,
			};
			// add listner to down image
			var downAction = new TapGestureRecognizer();
			downAction.TappedCallback += (s, e) => {
				if(Value>MinValue)
					Value--;
			};
			downButton.GestureRecognizers.Add(downAction);


			Image switchBackground = new Image {
				Aspect = Aspect.Fill,
				Source=Global._SWITCHER_BACKGROUND,
			};

			//label that indecates the current value
			MyLabel switchValue=new MyLabel{
				Text="Easy",
				FontSize=15,
				TextColor=Color.FromHex("#F3ECBE"),
				YAlign=TextAlignment.Center,
				XAlign=TextAlignment.Center
			};
			switchValue.BindingContext = this;
			switchValue.SetBinding (MyLabel.TextProperty, new Binding ("ValueStr"));

			this.BackgroundColor = Color.Transparent;
			this.WidthRequest=50;
			this.HeightRequest=50;

			//add components to relative layout
			this.Children.Add (upButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); })
			);

			this.Children.Add (downButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); })
			);

			this.Children.Add (switchBackground, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);

			this.Children.Add (switchValue, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);
		}
		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

	
	}
}

