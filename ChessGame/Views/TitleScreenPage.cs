using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;

namespace SharedViews
{
	/// <summary>
	/// Title screen page:
	/// here you can change the game settings or start new game with computer or with human.
	/// </summary>
	public class TitleScreenPage : ContentPage
	{

		/// <summary>
		/// Create title screen page where you can change the game settings or start new game with computer or with human.
		/// </summary>
		/// <param name="widthDP">Screen width.</param>
		/// <param name="heightDP">Screen height.</param>
		public TitleScreenPage (int widthDP,int heightDP)
		{
//			NavigationPage.SetHasNavigationBar(this, false);

			MessagingCenter.Unsubscribe<MySwitcher,string[]> (this, "Change Switcher");
			var user = ChessUser.getInstance();
			//=================Define components=====================
//			var indicator = new ActivityIndicator
//			{
//				HorizontalOptions = LayoutOptions.CenterAndExpand,
//				Color = Color.Black,
//				IsVisible = true 
//			};
					
			MyButton singlePlayerGameButton = new MyButton {
				Text="Play Single Player Game",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#49402F"),
				BorderSize=5,
				CornerRadius =20,
				borderColor=Color.Black,
				FontSize=20,
				AndroidButtonType=1
			};
			singlePlayerGameButton.Clicked += async(s, e) => {
				//go to single player setup page
				await Navigation.PushAsync (new SinglePlayerSetupScreen (widthDP,heightDP));
			};
			MyButton multiPlayerGameButton = new MyButton {
				Text="Player Multi-Player Game",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#49402F"),
				BorderSize=5,
				CornerRadius =20,
				borderColor=Color.Black,
				FontSize=20,
				AndroidButtonType=1
			};

			multiPlayerGameButton.Clicked += async(s, e) => {
				//go to multi-player setup page
				await Navigation.PushAsync (new MultiPlayerSetupScreen (widthDP,heightDP));
			};
			MyButton ChangeSettingsGameButton = new MyButton {
				Text="Change settings",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#49402F"),
				BorderSize=5,
				CornerRadius =20,
				borderColor=Color.Black,
				FontSize=20,
				AndroidButtonType=1
			};

			ChangeSettingsGameButton.Clicked += (s, e) => {
				//go to change settings page
				Navigation.PushModalAsync(new ChangeSettingsScreen(widthDP,heightDP));
			};

			MyButton learnMoreButton = new MyButton {
				Text="Learn More About Easier Chess",
				BackgroundColor=Color.FromHex("#99C6BF95"),
				TextColor=Color.Black,
				BorderSize=5,
				CornerRadius =20,
				borderColor=Color.Black,
				FontSize=12,
				AndroidButtonType=2,
				ButtonImage=Global._RIGHT_ARROW
			};

			learnMoreButton.Clicked += (s, e) => {
				Console.WriteLine("1");
			};

			Image frontBackground=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};

			Image logoImage=new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest=widthDP*2/15,
				HeightRequest=heightDP*2/15,
				Source=Global._LOGO,
			};

			MyLabel titleLabel = new MyLabel{
				Text="Easier Chess",
				TextColor=Color.Black,

			};
			if (Device.OS == TargetPlatform.iOS) {
				titleLabel.FontSize = 51;
			} else {
				titleLabel.FontSize = 40;
			}


//===============Define stacklayout that contains logo============
			StackLayout logo= new StackLayout	{
				BackgroundColor = Color.Transparent,
				Orientation = StackOrientation.Horizontal,
				Children = {logoImage,titleLabel}
			};	
//===============Define relativelayout that contains buttons and background============

			RelativeLayout buttons = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};

			RelativeLayout frontContainer = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};

			frontContainer.Children.Add (logo, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/4) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/9); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/4); })
			);

			frontContainer.Children.Add (buttons, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*14f/36); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
			);

			buttons.Children.Add (singlePlayerGameButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/16); })
			);
			buttons.Children.Add (multiPlayerGameButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/16); })
			);
			buttons.Children.Add (ChangeSettingsGameButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/16); })
			);
			frontContainer.Children.Add (learnMoreButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/2+parent.Width*1/16) ; }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3/4+parent.Height*1/32); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/16); })
			);

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (frontBackground,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/3); })
			);
			container.Children.Add (frontContainer, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/6); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/3); })
			);
//			container.Children.Add (indicator, 
//				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/2); }),
//				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/2); })
//			);

			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;
		}
	}
}

