using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SharedModel;
using ChessGame;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.IO;


namespace SharedViews
{
	public class MultiPlayerSetupScreen : ContentPage
	{
		public MultiPlayerSetupScreen (int widthDP,int heightDP)
		{

			//button to search for friends online
			MyButton searchFriendsButton = new MyButton {
				Text="SEARCH FOR FRIENDS",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#49402F"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=30,
				AndroidButtonType=1
			};
			searchFriendsButton.Clicked += (s, e) => {
			};

			//button to find an opponent online
			MyButton findOpponentButton = new MyButton {
				Text="FIND AN OPPONENT ONLINE",
				TextColor=Color.FromHex("#F3ECBE"),
				BackgroundColor=Color.FromHex("#49402F"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=30,
				AndroidButtonType=1

			};
			findOpponentButton.Clicked += async(s, e) => {
				await Navigation.PushAsync (new OnlineOpponentScreen (widthDP,heightDP));
			};

			// return to the main menu page
			MyButton returnButton = new MyButton {
				Text="return to main menu",
				TextColor=Color.FromHex("#020202"),
				BackgroundColor=Color.FromHex("#99C2B071"),
				BorderSize=5,
				CornerRadius =10,
				borderColor=Color.Black,
				FontSize=20,
				ButtonImage=Global._LEFT_ARROW
			};
			returnButton.Clicked += (s, e) => {
				//return to previous page
				Navigation.PopAsync();
			};

			MyLabel questionLabel = new MyLabel{
				Text="WHO WOULD YOU LIKE TO PLAY CHESS ONLINE WITH?",
				TextColor=Color.FromHex("#000000"),
				FontSize = 25,
				XAlign=TextAlignment.Center,
				VerticalOptions=LayoutOptions.Center
			};

			//create background images 
			Image front_1_Background=new Image {
				Aspect = Aspect.Fill,
				Source=Global._FRONT1_BACKGROUND,
			};
			Image titleImage=new Image {
				Aspect = Aspect.Fill,
				Source=Global._PLAYER_SETUP_TITLE,
			};
		


			#region buttons contaier
			//add buttons to the front container 
			RelativeLayout front_2_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
			};
			front_2_Container.Children.Add (questionLabel, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*-0.5f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*4.5f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			front_2_Container.Children.Add (searchFriendsButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*5f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*4f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			front_2_Container.Children.Add (findOpponentButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*9f/17); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*4f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/17); })
			);
			#endregion


			#region front container
			//add containers to the page 
			RelativeLayout front_1_Container = new RelativeLayout{
				BackgroundColor=Color.Transparent
			};


			front_1_Container.Children.Add (front_2_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*7f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*17f/24); })
			);

			front_1_Container.Children.Add (returnButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/12); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3.5f/24); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*2f/24); })
			);


			#endregion

			RelativeLayout container = new RelativeLayout{
				BackgroundColor=Color.Transparent,
				HorizontalOptions=LayoutOptions.Start,
				VerticalOptions=LayoutOptions.Start,
				WidthRequest=widthDP,
				HeightRequest=heightDP
			};
			container.Children.Add (front_1_Background,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);
			container.Children.Add (titleImage,
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1.5f/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); })
			);
			container.Children.Add (front_1_Container, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1f/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3f/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*3f/4); })
			);

			this.BackgroundImage = Global._BOARD_PAGE_BACKGROUND;
			Content = container;

		}
	}
}


