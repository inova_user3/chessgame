using System;
using Xamarin.Forms;
using SharedModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SharedViews
{
	/// <summary>
	/// Custom stepper that contains up,down,background and label that indicates the current value
	/// </summary>
	public class MyStepper :RelativeLayout, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		int value;
		int maxValue;
		int minValue;

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public int Value {
			get { return value; }
			set
			{
				if (value==this.value){return;}
				this.value = value;
				OnPropertyChanged("Value");
			}
		}

		/// <summary>
		/// Gets or sets the minimum value.
		/// </summary>
		/// <value>The minimum value.</value>
		public int MinValue {
			get { return minValue; }
			set{ this.minValue = value; }
		}

		/// <summary>
		/// Gets or sets the max value.
		/// </summary>
		/// <value>The max value.</value>
		public int MaxValue {
			get { return maxValue; }
			set{ this.maxValue = value; }
		}

		/// <summary>
		/// create a stepper object that contains 2 buttons up and down and a label that indicates the current value.
		/// </summary>
		public MyStepper ()
		{
			Image upButton = new Image {
				Aspect = Aspect.Fill,
				Source=Global._UP_SWITCHER_BUTTON,
			};
			var upAction = new TapGestureRecognizer();
			//add gesture to the up image
			upAction.TappedCallback += (s, e) => {
				if(Value<MaxValue)
					Value++;
			};
			upButton.GestureRecognizers.Add(upAction);


			Image downButton = new Image {
				Aspect = Aspect.Fill,
				Source=Global._DOWN_SWITCHER_BUTTON,
			};
			//add gesture to the down image
			var downAction = new TapGestureRecognizer();
			downAction.TappedCallback += (s, e) => {
				if(Value>MinValue)
					Value--;
			};
			downButton.GestureRecognizers.Add(downAction);


			Image switchBackground = new Image {
				Aspect = Aspect.Fill,
				Source=Global._SWITCHER_BACKGROUND,
			};

			//label that indicates the current value
			MyLabel switchValue=new MyLabel{
				Text=Value+"",
				VerticalOptions=LayoutOptions.Center,
				TextColor=Color.FromHex("#F3ECBE")
			};
			if (Device.OS == TargetPlatform.iOS) {
				switchValue.FontSize = 30;
			} else {
				switchValue.FontSize = 20;
			}

			switchValue.BindingContext = this;
			switchValue.SetBinding (MyLabel.TextProperty, new Binding ("Value"));

			this.BackgroundColor = Color.Transparent;
			this.WidthRequest=50;
			this.HeightRequest=50;
			Console.WriteLine (this.Width+" "+this.Height);


			//add components to the relative layout
			//first add up and down buttons
			this.Children.Add (upButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); })
			);

			this.Children.Add (downButton, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*1/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); })
			);

			//add the background 
			this.Children.Add (switchBackground, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*0); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*2/3); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height); })
			);

			this.Children.Add (switchValue, 
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3/16); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/4); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Width*3/8); }),
				Constraint.RelativeToParent ((parent) => { return (parent.Height*1/2); })
			);
		}

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName){
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}

