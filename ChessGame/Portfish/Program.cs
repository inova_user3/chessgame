using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Portfish
{
    internal sealed class ConsolePlug : IPlug
    {
        public void Write(string message)
        {
            Console.Write(message);
        }
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }

    class Program
    {

		public static void initPortfish(bool flag){
			CheckInfoBroker.init ();
			EvalInfoBroker.init ();
			SwapListBroker.init ();
			MovesSearchedBroker.init ();
			PositionBroker.init ();
			StateInfoArrayBroker.init ();

			MListBroker.init ();
			LoopStackBroker.init ();
			MovePickerBroker.init ();
			StateInfoBroker.init ();

			Utils.init ();
			Book.init ();
			Position.init ();
			KPKPosition.init ();
			Endgame.init ();
			Search.init (flag);
			Evaluate.init ();
			Threads.init ();
		}

		public static string goEvasion(string currentBoard,int depth,string searchMoves,string skillLevel){
//			Uci.set_option (Utils.CreateStack ("name Hash value 3 "));
			Search.flag=true;
			// .Net warmup sequence
			Plug.IsWarmup = true;
			Position pos = new Position (currentBoard+" b - - 0 0 ", false, Threads.main_thread ());
			Stack<string> stack = Utils.CreateStack ("go depth "+depth+" searchmoves "+searchMoves);
			Stack<string> skillLevelOption = Utils.CreateStack ("setoption name Skill Level value "+skillLevel);
			Uci.set_option (skillLevelOption);
			Uci.go (pos, stack);
			Threads.wait_for_search_finished ();
			Plug.IsWarmup = false;
			if(Search.RootMoves [0].pv [0]==0){
				Search.flag=false;
				// .Net warmup sequence
				Plug.IsWarmup = true;
				pos = new Position (currentBoard+" b - - 0 0 ", false, Threads.main_thread ());
				stack = Utils.CreateStack ("go depth "+depth);
				Uci.go (pos, stack);
				Threads.wait_for_search_finished ();
				Plug.IsWarmup = false;
			}
			string m = Utils.move_to_uci (Search.RootMoves [0].pv [0], Search.RootPosition.chess960);
			Console.WriteLine(m);
			return m;
		}

		public static string goNonEvasion(string currentBoard,int depth,string searchMoves,string skillLevel){
			Search.flag=false;
			// .Net warmup sequence
			Plug.IsWarmup = true;
			Position pos = new Position (currentBoard+" b - - 0 0 ", false, Threads.main_thread ());
			Stack<string> stack = Utils.CreateStack ("go depth "+depth+" searchmoves "+searchMoves);
			Stack<string> skillLevelOption = Utils.CreateStack ("setoption name Skill Level value "+skillLevel);
			Uci.set_option (skillLevelOption);
			Uci.go (pos, stack);
			Threads.wait_for_search_finished ();
			Plug.IsWarmup = false;
			string m = Utils.move_to_uci (Search.RootMoves [0].pv [0], Search.RootPosition.chess960);
			Console.WriteLine(m);
			return m;
		}

    }
}

