using System;

namespace SharedModel
{
	/// <summary>
	/// Chess board that contains array of board cells ( [8,8] array) .
	/// </summary>
	public class ChessBoard
	{
		BoardCell [,]boardCells;

		public ChessBoard ()
		{

		}

		/// <summary>
		/// Gets the board cells.
		/// </summary>
		/// <returns>The board cells.</returns>
		public BoardCell [,] getBoardCells(){
			return boardCells;
		}

		/// <summary>
		/// Sets the board cells.
		/// </summary>
		/// <param name="boardCells">Board cells.</param>
		public void setBoardCells(BoardCell [,] boardCells){
			this.boardCells=boardCells;
		}


	}
}

