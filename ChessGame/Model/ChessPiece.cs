using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SharedViews;

namespace SharedModel
{
	/// <summary>
	/// Chess piece object that define piece type, holding player and piece position.
	/// </summary>
	public class ChessPiece
	{
		Position position; // piece position on board
		Global.PIECE_TYPE pieceType; // Piece type (king, Rook, etc..).
		Global.PLAYER_TYPE playerType;// Player that hold this piece
		int numOfMoves; // total number of moves for this piece

		/// <summary>
		/// Create chess piece with properties:
		/// piece position ,piece type and player type
		/// </summary>
		/// <param name="pos">Position.</param>
		/// <param name="type">Piece type.</param>
		/// <param name="pType">Player type.</param>
		public ChessPiece (Position pos,Global.PIECE_TYPE type,Global.PLAYER_TYPE pType)
		{
			position = pos;
			pieceType = type;
			playerType = pType;
			numOfMoves = 0;
		}

		/// <summary>
		/// Gets or sets the number of moves.
		/// </summary>
		/// <value>The number of moves.</value>
		public int NumOfMoves{
			get{ 
				return numOfMoves;
			}
			set{ 
				this.numOfMoves = value;
			}
		}

		/// <summary>
		/// Gets or sets the type of the piece.
		/// </summary>
		/// <value>The type of the piece.</value>
		public Global.PIECE_TYPE PieceType{
			get{return pieceType;}
			set{ pieceType = value; }
		}

		/// <summary>
		/// Gets or sets the type of the player.
		/// </summary>
		/// <value>The type of the player.</value>
		public Global.PLAYER_TYPE PlayerType{
			get{ return playerType; }
			set{playerType = value;}
		}

		/// <summary>
		/// Gets or sets the position.
		/// </summary>
		/// <value>The position.</value>
		public Position Position{
			get{return position;}
			set{position=value;}
		}

		/// <summary>
		/// Request to promote this piece.
		/// </summary>
		public void promote(){
			MessagingCenter.Send<ChessPiece> (this, "Promote");
		}
	}
}

