using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace SharedModel
{
	/// <summary>
	/// Card model that contains the card image, card type and card state.
	/// </summary>
	public class Card: INotifyPropertyChanged
	{

		int index; // Card index in user hand
		string cardImage; // File name for card image
		bool isSelected; // Used to define card state
		Global.CARD_TYPE cardType; // Card type ( King, Move any, etc.. )
		Global.PLAYER_TYPE playerType; // Player that hold this card
		bool isCovered; // Used to define card state

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Create the card object.
		/// </summary>
		/// <param name="_cardType">Card type.</param>
		/// <param name="_playerType">Player type.</param>
		/// <param name="index">Index of the card in players hand.</param>
		public Card(Global.CARD_TYPE _cardType,Global.PLAYER_TYPE _playerType,int index){
			this.index = index;
			this.isCovered = false;
			this.cardType = _cardType;
			this.playerType = _playerType;
			CardImage = Global.cardToImageMap [Global.getCardName(_cardType,_playerType,false,ChessUser.getInstance().ChessStyle)];	
		}

		/// <summary>
		/// Gets or sets the card image file name.
		/// </summary>
		/// <value>The card image file name.</value>
		public string CardImage
		{
			get { return cardImage; }
			set
			{

				if (value.Equals(cardImage,StringComparison.Ordinal))
				{
					// Nothing to do - the value hasn't changed;
					return;
				}
				cardImage = value;
				OnPropertyChanged("CardImage");
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this card is selected or not.
		/// </summary>
		/// <value><c>true</c> if this card is selected; otherwise, <c>false</c>.</value>
		public bool IsSelected
		{
			get { return isSelected; }
			set {
				if (!isCovered && 
					isSelected != value) {
					CardImage = Global.cardToImageMap [Global.getCardName(CardType,playerType,value,ChessUser.getInstance().ChessStyle)];	
					isSelected = value; 
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this card is covered.
		/// </summary>
		/// <value><c>true</c> if this card is covered; otherwise, <c>false</c>.</value>
		public bool IsCovered
		{
			get { return isCovered; }
			set {
				if (value) {
					CardImage = Global._CARD_BACK_IMAGE;	
				}
				else
					CardImage = Global.cardToImageMap [Global.getCardName(CardType,playerType,false,ChessUser.getInstance().ChessStyle)];	

				isCovered = value; 
			}
		}

		/// <summary>
		/// Gets or sets the type of the card ( King, Move any, etc.. ).
		/// </summary>
		/// <value>Card type.</value>
		public Global.CARD_TYPE CardType
		{
			get { return cardType; }
			set{cardType = value;}
		}

		/// <summary>
		/// Gets or sets the player that hold this card.
		/// </summary>
		/// <value>The player that hold this card.</value>
		public Global.PLAYER_TYPE PlayerType
		{
			get { return playerType; }
			set{playerType = value;}
		}

		/// <summary>
		/// Gets or sets the index.
		/// </summary>
		/// <value>The index.</value>
		public int Index
		{
			get { return index; }
			set { index = value; }
		}

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

	}
}

