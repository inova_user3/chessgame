using System;

namespace SharedModel
{
	/// <summary>
	/// Move step contains previous position and next position.
	/// </summary>
	public class Move
	{
		Position prevPosition; // Old position before doing move
		Position nextPosition; // New position after doing move

		/// <summary>
		/// Create move object with previous and next positions.
		/// </summary>
		/// <param name="prevPosition">Previous position.</param>
		/// <param name="nextPosition">Next position.</param>
		public Move (Position prevPosition,Position nextPosition)
		{
			this.prevPosition = prevPosition;
			this.nextPosition = nextPosition;
		}

		/// <summary>
		/// Gets or sets the previous position.
		/// </summary>
		/// <value>The previous position.</value>
		public Position PrevPosition{
			get{ return prevPosition; }
			set{ prevPosition = value;}
		}
		/// <summary>
		/// Gets or sets the next position.
		/// </summary>
		/// <value>The next position.</value>
		public Position NextPosition{
			get{ return nextPosition; }
			set{ nextPosition = value;}
		}
	}
}

