using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SharedModel
{
	public class Global
	{
		//skill levels of the computer begginer , intermediate , expert
		public enum SKILL_LEVEL{_BEGINNER=1,_INTERMEDIATE=2,_EXPERT=3};
		//2 style of pieces and cards kids , adult
		public enum CHESS_STYLE{_KIDS=1,_ADULT=2};
		//16 avatar to choose from
		public enum AVATAR{_AVATAR_1=1,_AVATAR_2=2,_AVATAR_3=3,_AVATAR_4=4,_AVATAR_5=5,_AVATAR_6=6,_AVATAR_7=7,_AVATAR_8= 8,
				_AVATAR_9=9,_AVATAR_10=10,_AVATAR_11=11,_AVATAR_12=12,_AVATAR_13=13,_AVATAR_14=14,_AVATAR_15=15,_AVATAR_16= 16};
		//6 chess pieces pawn,rook,knight,bishop,queen and king
		public enum PIECE_TYPE{_PAWN=1,_ROOK=2,_KNIGHT=3,_BISHOP=4,_QUEEN=5,_KING=6};
		//9 chess cards, 6 for each piece and 3 special cards loseturn,moveany and movelast
		public enum CARD_TYPE{_PAWN_CARD=1,_ROOK_CARD=2,_KNIGHT_CARD=3,_BISHOP_CARD=4,_QUEEN_CARD=5,_KING_CARD=6,_EMPTY_CARD=7,_MOVE_ANY_CARD=8,_MOVE_LAST_CARD=9,_LOSE_TURN_CARD=10};
		//each chess game has 2 players
		public enum PLAYER_TYPE{_PLAYER1=12,_PLAYER2=22};
		//chess board state ordinary cell, selected cell or possible_move cell
		public enum CELL_STATE{_Selected=7,_POSSIBLE_MOVE=8,_ORDINARY=9};
		//chess piece state dead or alive
		public enum PIECE_STATE{_ALIVE=10,_DEAD=11};
		//game type human vs human or human vs computer
		public enum GAME_TYPE{_PLAYER_VS_PLAYER=100,_PLAYER_VS_COMPUTER=101};
		public enum PIECE_CARD_CHECK{YES_LOSE_TURN=2,_YES=1,_NO=0};

		public static readonly int _ENGINE_DEPTH=7;
		public static readonly int _ROWSNUM=8;
		public static readonly int _COLUMNSNUM=8;
		public static int _GRID_CELL_SIZE;
		public static readonly int _BACKGROUND_DIFF =5;

		//default cell colors for even and odd cells
		public static readonly int[] _ODD_CELL_COLOR = new int[] {2,2,2};

		public static readonly int[]  _EVEN_CELL_COLOR = new int[] {242,236,190};

		//===================Images===================
		public static readonly string _POSSIBLE_MOVE_IMAGE="PossibleMoves_Image";
		public static readonly string _BOARD_PAGE_BACKGROUND = "Background_Image";
		public static readonly string _BOARD_BACKGROUND = "boardlist_background";
		public static readonly string _CARD_BACK_IMAGE = "Card_Back";
		public static readonly string _CARD_DECK_IMAGE = "CardsDeck_Image";
		public static readonly string _FRONT1_BACKGROUND = "Front1_Background";
		public static readonly string _FRONT2_BACKGROUND = "Front2_Background";
		public static readonly string _LOGO = "logo_Image";
		public static readonly string _DOWN_ARROW = "Down_arrow";
		public static readonly string _PLAYER_SETUP_TITLE = "PlayerSetupTitle_Image";
		public static readonly string _FONT_NAME = "Copperplate Gothic T.";
		public static readonly string _RIGHT_ARROW = "ButtonNext_arrow";
		public static readonly string _LEFT_ARROW = "ButtonBack_arrow";
		public static readonly string _SINGLE_PLAYER_SETUP_FRONT="SinglePlayerSetupFrontBackground";
		public static readonly string _SINGLE_PLAYER_SETUP_TITLE="SinglePlayerSetupTitle";
		public static readonly string _START_MATCH_BUTTON = "StartMatchButton";
		//switcher options images
		public static readonly string _OPTION_1SELECTED = "option1Selected";
		public static readonly string _OPTION_1UNSELECTED = "option1Unselected";
		public static readonly string _OPTION_2SELECTED = "option2Selected";
		public static readonly string _OPTION_2UNSELECTED = "option2Unselected";
		//time picker images
		public static readonly string _TIME_SCROLLER_BACKGROUND = "TimeScrollerBackground";
		public static readonly string _WHITE_PLAYER_TIMER_BACKGROUND = "WhitePlayerTimerBackground";
		public static readonly string _BLACK_PLAYER_TIMER_BACKGROUND = "BlackPlayerTimerBackground";
		//stepper  images
		public static readonly string _UP_SWITCHER_BUTTON = "UpStepper";
		public static readonly string _DOWN_SWITCHER_BUTTON = "DownStepper";
		public static readonly string _SWITCHER_BACKGROUND = "StepperBackground";

		//map player chess piece to images
		public static readonly Dictionary<string,string> playerPieceToImageMap=new Dictionary<string, string>()
		{
			{getPieceName(Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"king_white_adult"},
			{getPieceName(Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"pawn_white_adult"},
			{getPieceName(Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"knight_white_adult"},
			{getPieceName(Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"queen_white_adult"},
			{getPieceName(Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"bishop_white_adult"},
			{getPieceName(Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._ADULT),"rook_white_adult"},

			{getPieceName(Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"king_black_adult"},
			{getPieceName(Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"pawn_black_adult"}, 
			{getPieceName(Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"knight_black_adult"},
			{getPieceName(Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"queen_black_adult"},
			{getPieceName(Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"bishop_black_adult"},
			{getPieceName(Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._ADULT),"rook_black_adult"},

			{getPieceName(Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"king_white"},
			{getPieceName(Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"pawn_white"},
			{getPieceName(Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"knight_white"},
			{getPieceName(Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"queen_white"},
			{getPieceName(Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"bishop_white"},
			{getPieceName(Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER1,Global.CHESS_STYLE._KIDS),"rook_white"},

			{getPieceName(Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"king_black"},
			{getPieceName(Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"pawn_black"}, 
			{getPieceName(Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"knight_black"},
			{getPieceName(Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"queen_black"},
			{getPieceName(Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"bishop_black"},
			{getPieceName(Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER2,Global.CHESS_STYLE._KIDS),"rook_black"}
		};

		//map player chess card to images
		public static readonly Dictionary<string,string> cardToImageMap=new Dictionary<string, string>()
		{
			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"king_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"pawn_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"knight_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"queen_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"bishop_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"rook_white_card.jpg"},
			{getCardName(Global.CARD_TYPE._LOSE_TURN_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"lose_turn_card"},
			{getCardName(Global.CARD_TYPE._MOVE_ANY_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"move_any_card"},
			{getCardName(Global.CARD_TYPE._MOVE_LAST_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._KIDS),"move_last_card"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"king_black_card.jpg"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"pawn_black_card.jpg"}, 
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"knight_black_card.jpg"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"queen_black_card.jpg"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"bishop_black_card.jpg"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._KIDS),"rook_black_card.jpg"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"king_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"pawn_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"knight_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"queen_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"bishop_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._KIDS),"rook_white_card_selected.jpg"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"king_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"pawn_white_card_selected.jpg"}, 
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"knight_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"queen_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"bishop_white_card_selected.jpg"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._KIDS),"rook_white_card_selected.jpg"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"king_white_card_adult"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"pawn_white_card_adult"},
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"knight_white_card_adult"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"queen_white_card_adult"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"bishop_white_card_adult"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER1,false,Global.CHESS_STYLE._ADULT),"rook_white_card_adult"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"king_black_card_adult"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"pawn_black_card_adult"}, 
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"knight_black_card_adult"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"queen_black_card_adult"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"bishop_black_card_adult"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER2,false,Global.CHESS_STYLE._ADULT),"rook_black_card_adult"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"king_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"pawn_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"knight_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"queen_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"bishop_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER1,true,Global.CHESS_STYLE._ADULT),"rook_white_card_adult_selected"},

			{getCardName(Global.CARD_TYPE._KING_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"king_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._PAWN_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"pawn_white_card_adult_selected"}, 
			{getCardName(Global.CARD_TYPE._KNIGHT_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"knight_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._QUEEN_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"queen_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._BISHOP_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"bishop_white_card_adult_selected"},
			{getCardName(Global.CARD_TYPE._ROOK_CARD,Global.PLAYER_TYPE._PLAYER2,true,Global.CHESS_STYLE._ADULT),"rook_white_card_adult_selected"},
		};

		//return unique identifier for player chess piece 
		public static string getPieceName(Global.PIECE_TYPE pieceType , Global.PLAYER_TYPE playerType,Global.CHESS_STYLE style){
			int val1,val2;
			if(playerType==Global.PLAYER_TYPE._PLAYER1)
				val1=1;
			else
				val1=2;
			if(style==Global.CHESS_STYLE._ADULT)
				val2=1;
			else
				val2=2;

			if(pieceType==Global.PIECE_TYPE._PAWN)
				return "Pawn"+val1+"."+val2;
			else if(pieceType==Global.PIECE_TYPE._QUEEN)
				return "Queen"+val1+"."+val2;
			else if(pieceType==Global.PIECE_TYPE._KING)
				return "King"+val1+"."+val2;
			else if(pieceType==Global.PIECE_TYPE._BISHOP)
				return "Bishop"+val1+"."+val2;
			else if(pieceType==Global.PIECE_TYPE._ROOK)
				return "Rook"+val1+"."+val2;
			else 
				return "Knight"+val1+"."+val2;
		}
		//return unique identifier for player chess card 
		public static string getCardName(Global.CARD_TYPE cardType , Global.PLAYER_TYPE playerType,bool isSelected,Global.CHESS_STYLE style){
			Console.WriteLine (cardType+" "+ playerType+" " + isSelected);
			if (cardType == Global.CARD_TYPE._EMPTY_CARD)
				return "Empty";
			else if (cardType == Global.CARD_TYPE._LOSE_TURN_CARD)
				return "LoseTurn";
			else if (cardType == Global.CARD_TYPE._MOVE_ANY_CARD)
				return "MoveAny";
			else if (cardType == Global.CARD_TYPE._MOVE_LAST_CARD)
				return "MoveLast";

			int player_type;
			int selection;
			int chess_style;
			if(playerType==Global.PLAYER_TYPE._PLAYER1)
				player_type=1;
			else
				player_type=2;

			if (isSelected)
				selection = 1;
			else
				selection = 2;

			if (style==Global.CHESS_STYLE._ADULT)
				chess_style = 1;
			else
				chess_style = 2;

			if(cardType==Global.CARD_TYPE._PAWN_CARD)
				return "Pawn"+player_type+"."+selection+"."+chess_style;
			else if(cardType==Global.CARD_TYPE._QUEEN_CARD)
				return "Queen"+player_type+"."+selection+"."+chess_style;
			else if(cardType==Global.CARD_TYPE._KING_CARD)
				return "King"+player_type+"."+selection+"."+chess_style;
			else if(cardType==Global.CARD_TYPE._BISHOP_CARD)
				return "Bishop"+player_type+"."+selection+"."+chess_style;
			else if(cardType==Global.CARD_TYPE._ROOK_CARD)
				return "Rook"+player_type+"."+selection+"."+chess_style;
			else 
				return "Knight"+player_type+"."+selection+"."+chess_style;
		}
		//return the avatars Image
		public static string getAvatarImage(Global.AVATAR avatar){
			if (avatar == Global.AVATAR._AVATAR_1) 
				return "avatar_1.jpg";
			else if (avatar == Global.AVATAR._AVATAR_2) 
				return "avatar_2.jpg";
			else if (avatar == Global.AVATAR._AVATAR_3) 
				return "avatar_3.jpg";
			else if (avatar == Global.AVATAR._AVATAR_4) 
				return "avatar_4.jpg";
			else if (avatar == Global.AVATAR._AVATAR_5) 
				return "avatar_5.jpg";
			else if (avatar == Global.AVATAR._AVATAR_6) 
				return "avatar_6.jpg";
			else if (avatar == Global.AVATAR._AVATAR_7) 
				return "avatar_7.jpg";
			else if (avatar == Global.AVATAR._AVATAR_8) 
				return "avatar_8.jpg";
			else if (avatar == Global.AVATAR._AVATAR_9) 
				return "avatar_9.jpg";
			else if (avatar == Global.AVATAR._AVATAR_10) 
				return "avatar_10.jpg";
			else if (avatar == Global.AVATAR._AVATAR_11) 
				return "avatar_11.jpg";
			else if (avatar == Global.AVATAR._AVATAR_12) 
				return "avatar_12.jpg";
			else if (avatar == Global.AVATAR._AVATAR_13) 
				return "avatar_13.jpg";
			else if (avatar == Global.AVATAR._AVATAR_14) 
				return "avatar_14.jpg";
			else if (avatar == Global.AVATAR._AVATAR_15) 
				return "avatar_15.jpg";
			return "avatar_16.jpg";
		}

		//return the avatar given it's index
		public static Global.AVATAR getAvatarFromIndex(int i){
			if (i == 0)
				return Global.AVATAR._AVATAR_1;
			else if (i == 1)
				return Global.AVATAR._AVATAR_2;
			else if (i == 2)
				return Global.AVATAR._AVATAR_3;
			else if (i == 3)
				return Global.AVATAR._AVATAR_4;
			else if (i == 4)
				return Global.AVATAR._AVATAR_5;
			else if (i == 5)
				return Global.AVATAR._AVATAR_6;
			else if (i == 6)
				return Global.AVATAR._AVATAR_7;
			else if (i == 7)
				return Global.AVATAR._AVATAR_8;
			else if (i == 8)
				return Global.AVATAR._AVATAR_9;
			else if (i == 9)
				return Global.AVATAR._AVATAR_10;
			else if (i == 10)
				return Global.AVATAR._AVATAR_11;
			else if (i == 11)
				return Global.AVATAR._AVATAR_12;
			else if (i == 12)
				return Global.AVATAR._AVATAR_13;
			else if (i == 13)
				return Global.AVATAR._AVATAR_14;
			else if (i == 14)
				return Global.AVATAR._AVATAR_15;
			return Global.AVATAR._AVATAR_16;
		}
	}
}

