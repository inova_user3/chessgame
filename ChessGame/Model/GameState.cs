using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;


namespace SharedModel
{

	/// <summary>
	/// Game state that hold all data of the match :
	/// player1, playe2, board cells array, match type, list of avaliable cards to draw from, list of discarded cards and list of selected cards.
	/// </summary>
	public class GameState : INotifyPropertyChanged
	{
		ChessBoard board; // Game board
		Global.GAME_TYPE matchType; // Match type ( Human VS Human, Human VS Computer)
		Player currentPlayer; // Player that has current turn.
		Player player1; // First Player (White Player)
		Player player2; // Second Player (Black Player)
		int moveNumber; // Total number of moves. Used to define current player
		string player1Label; // Text typed down the board
		string player2Label; // Text typed above the board 

		Global.PIECE_TYPE player1LastPieceMoved; // Last piece player one moved. used when player select last move card
		Global.PIECE_TYPE player2LastPieceMoved; // Last piece player two moved. used when player select last move card 
		public event PropertyChangedEventHandler PropertyChanged;
		string moveTimer=""; // current player move timer text
		string player1MatchTimer=""; // player one turn timer text
		string player2MatchTimer=""; // player two turn timer text
		string computerSkillLevel; // computer skill level

		public List<Card> selectedCardsList; // current selected cards. used if player is selecting 3 cards from the same type

		public List<Global.CARD_TYPE> availableCards;// Available cards to draw from

		public List<Global.CARD_TYPE> discardedCards;// all discarded cards

		public Card nextDrawnCard; // next card to be drawn

		bool cardSelected; // True if player selected any card else Flase.
		Card selectedCard; // Current selected Card
		public ChessPiece selectedPiece; // current selected piece

		/// <summary>
		/// Gets or sets the player1 match timer.
		/// </summary>
		/// <value>The player1 match timer.</value>
		public string Player1MatchTimer
		{
			get { return player1MatchTimer; }
			set
			{
				if (value.Equals(player1MatchTimer,StringComparison.Ordinal))
				{
					return;
				}
				if (value.Equals("0"))
					player1MatchTimer = "";
				else
					player1MatchTimer = value;
				OnPropertyChanged("Player1MatchTimer");
			}
		}

		/// <summary>
		/// Gets or sets the player2 match timer.
		/// </summary>
		/// <value>The player2 match timer.</value>
		public string Player2MatchTimer
		{
			get { return player2MatchTimer; }
			set
			{
				if (value.Equals(player2MatchTimer,StringComparison.Ordinal))
				{
					return;
				}
				if (value.Equals("0"))
					player2MatchTimer = "";
				else
					player2MatchTimer = value;
				OnPropertyChanged("Player2MatchTimer");
			}
		}

		/// <summary>
		/// Gets or sets the current move timer text.
		/// </summary>
		/// <value>The move timer.</value>
		public string MoveTimer
		{
			get { return moveTimer; }
			set
			{
				if (value.Equals(moveTimer,StringComparison.Ordinal))
				{
					return;
				}
				if (value.Equals("0"))
					moveTimer = "";
				else
					moveTimer = value;
				OnPropertyChanged("MoveTimer");
			}
		}
		/// <summary>
		/// Gets or sets current turn player.
		/// </summary>
		/// <value>The current turn player.</value>
		public Player CurrentPlayer{
			get{return currentPlayer; }
			set{currentPlayer = value; }
		}

		/// <summary>
		/// Gets or sets the player1 last piece moved.
		/// </summary>
		/// <value>The player1 last piece moved.</value>
		public Global.PIECE_TYPE Player1LastPieceMoved{
			get{return player1LastPieceMoved; }
			set{player1LastPieceMoved = value; }
		}

		/// <summary>
		/// Gets or sets the player2 last piece moved.
		/// </summary>
		/// <value>The player2 last piece moved.</value>
		public Global.PIECE_TYPE Player2LastPieceMoved{
			get{return player2LastPieceMoved; }
			set{player2LastPieceMoved = value; }
		}

		/// <summary>
		/// Gets or sets the computer skill level.
		/// </summary>
		/// <value>The computer skill level.</value>
		public string ComputerSkillLevel{
			get{return computerSkillLevel; }
			set{computerSkillLevel = value; }
		}

		/// <summary>
		/// Gets or sets player1 object.
		/// </summary>
		/// <value>The player1 object.</value>
		public Player Player1{
			get{ return player1;}
			set{player1=value;}
		}

		/// <summary>
		/// Gets or sets the player2 object.
		/// </summary>
		/// <value>The player2 object.</value>
		public Player Player2{
			get{ return player2;}
			set{player2=value;}
		}

		/// <summary>
		/// Gets or sets the player1 label.
		/// </summary>
		/// <value>The player1 label.</value>
		public string Player1Label
		{
			get { return player1Label; }
			set
			{
				if (value.Equals(player1Label,StringComparison.Ordinal))
				{
					return;
				}
				player1Label = value;
				OnPropertyChanged("Player1Label");
			}
		}

		/// <summary>
		/// Gets or sets the player2 label.
		/// </summary>
		/// <value>The player2 label.</value>
		public string Player2Label
		{
			get { return player2Label; }
			set
			{
				if (value.Equals(player2Label,StringComparison.Ordinal))
				{
					return;
				}
				player2Label = value;
				OnPropertyChanged("Player2Label");
			}
		}

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		/// <summary>
		/// Gets or sets the board.
		/// </summary>
		/// <value>The board.</value>
		public ChessBoard Board{
			get{
				return this.board;
			} 
			set{
				this.board = value;
			}
		}

		/// <summary>
		/// Gets or sets the type of the match.
		/// </summary>
		/// <value>The type of the match.</value>
		public Global.GAME_TYPE MatchType{
			get{
				return this.matchType;
			} 
			set{
				this.matchType = value;
			}
		}

		/// <summary>
		/// Gets or sets the move number.
		/// </summary>
		/// <value>The move number.</value>
		public int MoveNumber{
			get{
				return this.moveNumber;
			} 
			set{
				this.moveNumber = value;
				if(value%2==0)
					CurrentPlayer=Player1;
				else				
					CurrentPlayer=Player2;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating there is any card selected or not.
		/// </summary>
		/// <value><c>true</c> if card selected; otherwise, <c>false</c>.</value>
		public bool CardSelected{
			get{return cardSelected; }
			set{cardSelected = value; }
		}

		/// <summary>
		/// Gets or sets the selected card.
		/// </summary>
		/// <value>The selected card.</value>
		public Card SelectedCard{
			get{return selectedCard; }
			set{selectedCard = value; }
		}


		/// <summary>
		/// Gets or sets the selected piece.
		/// </summary>
		/// <value>The selected piece.</value>
		public ChessPiece SelectedPiece{
			get{return selectedPiece; }
			set{selectedPiece = value; }
		}

		/// <summary>
		/// Initialize the game state with a specific game type ( player vs player or player vs computer ).
		/// </summary>
		/// <param name="_gameType">Game type:player vs player or player vs computer.</param>
		public GameState (Global.GAME_TYPE _gameType)
		{
			this.availableCards=new List<Global.CARD_TYPE>();
			this.discardedCards = new List<Global.CARD_TYPE> ();
			this.selectedCardsList = new List<Card> ();
			this.Player1LastPieceMoved = Global.PIECE_TYPE._PAWN;
			this.Player2LastPieceMoved = Global.PIECE_TYPE._PAWN;
			this.matchType = _gameType;

			Player1Label = "Player1 ( Human )";
			Player2Label = "Player2 ("+((_gameType==Global.GAME_TYPE._PLAYER_VS_PLAYER)?" Human )":" Computer )");
		}

		/// <summary>
		/// Updates the match result.
		/// </summary>
		/// <param name="resultVal">Result value.</param>
		public void updateMatchResult(ChessEngine.MATCH_RESULT resultVal){
			MessagingCenter.Send<GameState,ChessEngine.MATCH_RESULT> (this, "Match ended",resultVal);
		}

		/// <summary>
		/// Called when any player end his turn.
		/// </summary>
		public void turnDidChange(){
			if(MoveNumber%2==0){
				Player1Label="Player1 ( Human ) Thinking ...";
				Player2Label="Player2 ("+((this.MatchType==Global.GAME_TYPE._PLAYER_VS_PLAYER)?" Human )":" Computer )");
			}
			else{
				Player1Label="Player1 ( Human )";
				Player2Label="Player2 ("+((this.MatchType==Global.GAME_TYPE._PLAYER_VS_PLAYER)?" Human ) Thinking ...":" Computer ) Thinking ...");
			}
		}
			
		/// <summary>
		/// Updates the cards view. called to update cards view (when player take the turn)
		/// </summary>
		public void updateCardsView(){
			for (int i = 0; i < currentPlayer.PlayerCards.Count; i++) 
				currentPlayer.PlayerCards [i].IsCovered = false;
			MessagingCenter.Send<GameState> (this, "Show Cards");
		}

		/// <summary>
		/// Flip the cards not to be seen. Called when player end his turn and waiting other player to take the turn
		/// </summary>
		public void coverCards(){
			for (int i = 0; i < currentPlayer.PlayerCards.Count; i++) {
				currentPlayer.PlayerCards [i].IsCovered = true;
			}
		}
	}
}

