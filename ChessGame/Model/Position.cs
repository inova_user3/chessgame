using System;

namespace SharedModel
{
	/// <summary>
	/// Used to define postion on board.
	/// </summary>
	public class Position
	{
		int rowNum;
		int columnNum;

		/// <summary>
		/// Create position object with row and column properties
		/// </summary>
		/// <param name="row">Row.</param>
		/// <param name="column">Column.</param>
		public Position (int row,int column)
		{
			rowNum = row;
			columnNum = column;
		}

		/// <summary>
		/// Gets or sets the row number.
		/// </summary>
		/// <value>The row number.</value>
		public int RowNum{
			get {return rowNum;}
			set {rowNum = value;}
		}

		/// <summary>
		/// Gets or sets the column number.
		/// </summary>
		/// <value>The column number.</value>
		public int ColumnNum{
			get {return columnNum;}
			set {columnNum = value;}
		}
	}
}

