using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace SharedModel
{
	/// <summary>
	/// Board cell has piece image, possible move image and position properties
	/// </summary>
	public class BoardCell : INotifyPropertyChanged{

		Global.CELL_STATE currentState;
		ChessPiece cellPiece;

		Color backgroundColor;
		public event PropertyChangedEventHandler PropertyChanged;
		string pieceImage;
		string possbileMoveIamge;
		Position cellPosition;


		/// <summary>
		/// Gets or sets the cell background color.
		/// </summary>
		/// <value>Cell background color.</value>
		public Color BackgroundColor
		{
			get { return backgroundColor; }
			set
			{
				if (value.Equals(backgroundColor))
				{
					// Nothing to do - the value hasn't changed;
					return;
				}
				backgroundColor = value;
				OnPropertyChanged("BackgroundColor");
			}
		}

		/// <summary>
		/// Gets or sets the piece image file name.
		/// </summary>
		/// <value>Piece image file name.</value>
		public string PieceImage
		{
			get { return pieceImage; }
			set
			{
				if ((value==null && pieceImage==null)||
					(value!=null && pieceImage!=null && value.Equals(pieceImage,StringComparison.Ordinal)))
				{
					return;
				}
				pieceImage = value;
				OnPropertyChanged("PieceImage");
			}
		}

		/// <summary>
		/// Gets or sets the possible move image file name.
		/// </summary>
		/// <value>The possible move image name.</value>
		public string PossibleMoveImage
		{
			get { return possbileMoveIamge; }
			set
			{
				if ((value==null && possbileMoveIamge==null)||
					(value!=null && possbileMoveIamge!=null && value.Equals(possbileMoveIamge,StringComparison.Ordinal)))
				{
					return;
				}
				possbileMoveIamge = value;
				OnPropertyChanged("PossibleMoveImage");
			}
		}

		/// <summary>
		/// Raises the property changed event.
		/// Called when any property value is changed.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}



		/// <summary>
		/// Initializes board cell with a specific position (row ,column).
		/// </summary>
		/// <param name="pos">Position.</param>

		public BoardCell (Position pos)
		{
			cellPosition = pos;
			if ((pos.RowNum + pos.ColumnNum ) % 2 != 0) 
				backgroundColor = Color.FromRgb (Global._ODD_CELL_COLOR[0],Global._ODD_CELL_COLOR[1],Global._ODD_CELL_COLOR[2]);
			else 
				backgroundColor = Color.FromRgb(Global._EVEN_CELL_COLOR[0],Global._EVEN_CELL_COLOR[1],Global._EVEN_CELL_COLOR[2]);
			currentState = Global.CELL_STATE._ORDINARY;
			possbileMoveIamge = null;
		}
			
		/// <summary>
		/// Sets the state of the board cell (ordinary , selected or possible move).
		/// </summary>
		/// <param name="state">State of cell (ordinary , selected or possible move).</param>
		public void setState(Global.CELL_STATE state){
			currentState=state;
			if (state == Global.CELL_STATE._Selected) {
				PossibleMoveImage = null;
			} else if (state == Global.CELL_STATE._POSSIBLE_MOVE) {
				//add marker to the possible move cell
				PossibleMoveImage = Global._POSSIBLE_MOVE_IMAGE;
				BackgroundColor= Color.FromRgb (Global._ODD_CELL_COLOR[0],Global._ODD_CELL_COLOR[1],Global._ODD_CELL_COLOR[2]);
			} else {
				if ((cellPosition.RowNum + cellPosition.ColumnNum) % 2 == 0)
					BackgroundColor= Color.FromRgb (Global._EVEN_CELL_COLOR[0],Global._EVEN_CELL_COLOR[1],Global._EVEN_CELL_COLOR[2]);
				 else 
					BackgroundColor = Color.FromRgb(Global._ODD_CELL_COLOR[0],Global._ODD_CELL_COLOR[1],Global._ODD_CELL_COLOR[2]);
				PossibleMoveImage = null;
			}
		}
		/// <summary>
		/// Gets the cell state.
		/// </summary>
		/// <returns>The cell state.</returns>
		public Global.CELL_STATE getState(){
			return currentState;
		}
		/// <summary>
		/// Gets the piece object.
		/// </summary>
		/// <returns>The piece object.</returns>
		public ChessPiece getPiece(){
			return cellPiece;
		}
		/// <summary>
		/// Gets cell position.
		/// </summary>
		/// <returns>Cell position.</returns>
		public Position getPosition(){

			return cellPosition;
		}
			
		/// <summary>
		/// Sets the piece object for the board cell.
		/// </summary>
		/// <param name="piece">Piece object.</param>
		public void setPiece(ChessPiece piece){
			cellPiece=piece;
			if (piece != null) {
				PieceImage = Global.playerPieceToImageMap [Global.getPieceName(piece.PieceType,piece.PlayerType,ChessUser.getInstance().ChessStyle)];
			} else {
				PieceImage = null;
			}
		}
			
	}
}
