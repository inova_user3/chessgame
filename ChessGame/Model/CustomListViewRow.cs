using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace SharedModel
{
	public class CustomListViewRow : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;


		int index;
		public int Index{

			get{ return index;}
			set{ index = value;}
		}

		Color rowColor;
		public Color RowColor {
			get { return rowColor; }
			set
			{
				if (value.Equals(rowColor))
				{
					return;
				}
				this.rowColor = value;
				OnPropertyChanged("RowColor");
			}
		}


		string rowLabel;
		public string RowLabel {
			get { return rowLabel; }
			set
			{
				if (value.Equals(rowLabel,StringComparison.Ordinal))
				{
					return;
				}
				this.rowLabel = value;
				OnPropertyChanged("RowLabel");
			}
		}

		public CustomListViewRow (string text,Color backgroundColor,int _index)
		{
			RowColor = backgroundColor;
			RowLabel = text;
			Index = _index;
		}


		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		void OnPropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}

