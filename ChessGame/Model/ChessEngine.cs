using System;
using System.Timers;
using System.Collections.Generic;
using Xamarin.Forms;
using System.IO;
using System.Text;
using System.Threading;
using SharedViews;
using Portfish;
//using Portfish2;

namespace SharedModel
{
	/// <summary>
	/// Chess engine that perform every action in the game.
	/// </summary>
	public static class ChessEngine
	{
		public enum MATCH_RESULT{PLAYER1_WIN,PLAYER2_WIN,RUNNING};
		public static Random rand; // Used to generate random number. Used for shuffling cards list

		//the player moved a piece and ready to change turn
		public static bool movingPiece;

		//TODO remove gstate to the view 
		public static GameState gState;

		public static bool shouldStop = false; 		//Used to stop move(turn) timer 
		public static bool player1MatchTimerShouldStop = true; // Used to stop  player 1 match timer
		public static bool player2MatchTimerShouldStop = true; // Used to stop  player 2 match timer

		/// <summary>
		/// Inits the state of the game
		/// </summary>
		/// <returns>The game state.</returns>
		/// <param name="_gameType">Game type.</param>
		/// <param name="player1CardsNum">Player1 cards number.</param>
		/// <param name="player2CardsNum">Player2 cards number.</param>
		/// <param name="player1MoveSeconds">Player1 move seconds.</param>
		/// <param name="player2MoveSeconds">Player2 move seconds.</param>
		/// <param name="player1MatchSeconds">Player1 match seconds.</param>
		/// <param name="player2MatchSeconds">Player2 match seconds.</param>
		public static GameState initGameState(Global.GAME_TYPE _gameType,int player1CardsNum,int player2CardsNum,int player1MoveSeconds,int player2MoveSeconds,int player1MatchSeconds,int player2MatchSeconds,int computerSkillLevel){
			gState=new GameState (_gameType);
			if(computerSkillLevel==0) //Easy
				gState.ComputerSkillLevel="1";
			else if(computerSkillLevel==1) // a little skill
				gState.ComputerSkillLevel="5";
			else if(computerSkillLevel==2) // pretty good
				gState.ComputerSkillLevel="10";
			else if(computerSkillLevel==3) // very good
				gState.ComputerSkillLevel="15";
			else if(computerSkillLevel==4) // amazing
				gState.ComputerSkillLevel="20";
				
			shouldStop = true; 
			player1MatchTimerShouldStop = true; 
			player2MatchTimerShouldStop = true;
			initCards ();
			gState.selectedCardsList=new List<Card>();
			rand = new Random ();
			List<Card> player1Cards = new List<Card> ();
			List<Card> player2Cards = new List<Card> ();
	
			for (int i = 0; i < player1CardsNum; i++)
				player1Cards.Add(selectRandomCard (i,Global.PLAYER_TYPE._PLAYER1));
			for (int i = 0; i < player2CardsNum; i++)
				player2Cards.Add(selectRandomCard (i,Global.PLAYER_TYPE._PLAYER2));

			Player player1 = new Player (Global.PLAYER_TYPE._PLAYER1,player1Cards,player1MoveSeconds,player1MatchSeconds);
			Player player2 = new Player (Global.PLAYER_TYPE._PLAYER2,player2Cards,player2MoveSeconds,player2MatchSeconds);
			gState.Player1 = player1;
			gState.Player2 = player2;
			gState.MoveNumber = 0;
			gState.nextDrawnCard = new Card (gState.availableCards [gState.availableCards.Count - 1], gState.CurrentPlayer.PlayerType, 0);
			player1Seconds=player1MatchSeconds;
			player2Seconds=player2MatchSeconds;
			return gState;
		}

		/// <summary>
		/// Initializes the pieces in the chess board cells.
		/// </summary>
		/// <returns>The chess board.</returns>
		public static ChessBoard initializeChessBoard(){
			shouldStop = true;
			player1MatchTimerShouldStop = false;
			player2MatchTimerShouldStop = true;
			ChessBoard chessBoard = new ChessBoard ();
			BoardCell[,] board;
			//set cells in the board
			board=new BoardCell[Global._ROWSNUM,Global._COLUMNSNUM];
			for(int i=0;i<Global._ROWSNUM;i++){
				for(int j=0;j<Global._COLUMNSNUM;j++){
					board[i,j]=new BoardCell(new Position(i,j));
				}
			}
			//set Pieces for player2

			//set Pawn pieces
			for (int i = 0; i < Global._COLUMNSNUM; i++) {
				board [1, i].setPiece (new ChessPiece (new Position(1,i),Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER2));
			}

			// set King Knight Rook Queen Bishop pieces 
			board [0, 0].setPiece (new ChessPiece (new Position(0,0),Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER2));
			board [0, 7].setPiece (new ChessPiece (new Position(0,7),Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER2));
			board [0, 1].setPiece (new ChessPiece (new Position(0,1),Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER2));
			board [0, 6].setPiece (new ChessPiece (new Position(0,6),Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER2));
			board [0, 2].setPiece (new ChessPiece (new Position(0,2),Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER2));
			board [0, 5].setPiece (new ChessPiece (new Position(0,5),Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER2));
			board [0, 3].setPiece (new ChessPiece (new Position(0,3),Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER2));
			board [0, 4].setPiece (new ChessPiece (new Position(0,4),Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER2));

			//set Pieces for player1 
			//set Pawn pieces
			for (int i = 0; i < Global._COLUMNSNUM; i++) {
				board [6, i].setPiece (new ChessPiece (new Position(6,i),Global.PIECE_TYPE._PAWN,Global.PLAYER_TYPE._PLAYER1));
			}
			// set King Knight Rook Queen Bishop pieces 
			board [7, 0].setPiece (new ChessPiece (new Position(7,0),Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER1));
			board [7, 7].setPiece (new ChessPiece (new Position(7,7),Global.PIECE_TYPE._ROOK,Global.PLAYER_TYPE._PLAYER1));
			board [7, 1].setPiece (new ChessPiece (new Position(7,1),Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER1));
			board [7, 6].setPiece (new ChessPiece (new Position(7,6),Global.PIECE_TYPE._KNIGHT,Global.PLAYER_TYPE._PLAYER1));
			board [7, 2].setPiece (new ChessPiece (new Position(7,2),Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER1));
			board [7, 5].setPiece (new ChessPiece (new Position(7,5),Global.PIECE_TYPE._BISHOP,Global.PLAYER_TYPE._PLAYER1));
			board [7, 3].setPiece (new ChessPiece (new Position(7,4),Global.PIECE_TYPE._QUEEN,Global.PLAYER_TYPE._PLAYER1));
			board [7, 4].setPiece (new ChessPiece (new Position(7,3),Global.PIECE_TYPE._KING,Global.PLAYER_TYPE._PLAYER1));


			chessBoard.setBoardCells (board);
			gState.Board = chessBoard;
			return chessBoard;
		}

		/// <summary>
		/// Initialize the avaliable cards list. Create 68 cards
		/// 6 for each card of (lose turn, move any and move last)
		/// 7 for king card
		/// 8 for each card of (knight, queen, bishop and rook) 
		/// 11 for pawn card
		/// </summary>
		public static void initCards ()
		{
			gState.availableCards = new List<Global.CARD_TYPE> ();
			gState.discardedCards=new List<Global.CARD_TYPE> ();
			for (int i = 0; i < 11; i++) {
				if (i < 6) {
					gState.availableCards.Add (Global.CARD_TYPE._LOSE_TURN_CARD);
					gState.availableCards.Add (Global.CARD_TYPE._MOVE_ANY_CARD);
					gState.availableCards.Add (Global.CARD_TYPE._MOVE_LAST_CARD);
				}
				if(i<7)
					gState.availableCards.Add (Global.CARD_TYPE._KING_CARD);
				if (i < 8) {
					gState.availableCards.Add (Global.CARD_TYPE._KNIGHT_CARD);
					gState.availableCards.Add (Global.CARD_TYPE._QUEEN_CARD);
					gState.availableCards.Add (Global.CARD_TYPE._BISHOP_CARD);
					gState.availableCards.Add (Global.CARD_TYPE._ROOK_CARD);
				}
				gState.availableCards.Add (Global.CARD_TYPE._PAWN_CARD);
			}
//			for(int i=0;i<gState.availableCards.Count;i++)
//				Console.WriteLine (i+" : "+gState.availableCards[i]);

			shuffle (gState.availableCards);

//			Console.WriteLine ("--------Shuffled----------");

//			for(int i=0;i<gState.availableCards.Count;i++)
//				Console.WriteLine (i+" : "+gState.availableCards[i]);
		}
			
		/// <summary>
		/// Random shuffle the specified list.
		/// </summary>
		/// <param name="list">List to be shuffled .</param>
		public static void shuffle(List<Global.CARD_TYPE> list)  
		{  
			Random rng = new Random();  
			int n = list.Count;  
			while (n > 1) {  
				n--;  
				int k = rng.Next(n + 1);  
				Global.CARD_TYPE value = list[k];  
				list[k] = list[n];  
				list[n] = value;  
			}
		}

		/// <summary>
		/// Selects the cell in the index row and column.
		/// </summary>
		/// <param name="rowNum">Row number.</param>
		/// <param name="columnNum">Column number.</param>
		public static void selectCell(int rowNum,int columnNum){
			BoardCell cell = gState.Board.getBoardCells () [rowNum, columnNum];
			List<Position>possibleMoves=getPossibleMoves(rowNum,columnNum);
			if (possibleMoves.Count != 0) {
				setPossibleMoves (possibleMoves);
				cell.setState (Global.CELL_STATE._Selected);
				gState.SelectedPiece = cell.getPiece ();
			}
		}

		/// <summary>
		/// Unselect all cells in the board when canceling the selection 
		/// </summary>
		public static void unSelectAllCells(){
			for(int i=0;i<Global._ROWSNUM;i++)
				for(int j=0;j<Global._COLUMNSNUM;j++)
					gState.Board.getBoardCells()[i,j].setState(Global.CELL_STATE._ORDINARY);
		}

		/// <summary>
		/// Update board cells with possible moves
		/// </summary>
		/// <param name="possibleMoves">Possible moves List.</param>
		public static void setPossibleMoves(List<Position> possibleMoves){
			for (int i = 0; i < possibleMoves.Count; i++) {
				BoardCell cell = gState.Board.getBoardCells () [possibleMoves [i].RowNum, possibleMoves [i].ColumnNum];
				cell.setState (Global.CELL_STATE._POSSIBLE_MOVE);
			}
		}

		/// <summary>
		/// Check if the index is valid or not ( between board bounds)
		/// </summary>
		/// <returns><c>true</c>, if index is valid, <c>false</c> otherwise.</returns>
		/// <param name="rowNum">Row number.</param>
		/// <param name="columnNum">Column number.</param>
		public static bool validIndex(int rowNum,int columnNum){
			if (rowNum < 0 || rowNum > 7 || columnNum > 7 || columnNum < 0)
				return false;
			return true;
		}

		/// <summary>
		/// Gemerate the king,pawn and knight possible moves and add them to possible moves list for position.
		/// </summary>
		/// <returns>The possible moves List.</returns>
		/// <param name="rowNumber">Piece row number.</param>
		/// <param name="columnNumber">Piece column number.</param>
		public static List<Position> getPossibleMoves(int rowNumber,int columnNumber){
			BoardCell[,] board = gState.Board.getBoardCells ();
			ChessPiece piece = gState.Board.getBoardCells()[rowNumber,columnNumber].getPiece();
			List<Position> possibleMoves = new List<Position>();

			#region PAWN possible moves
			if (piece.PieceType == Global.PIECE_TYPE._PAWN) {
				 
				if (piece.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
					//can move 1 step
					if(validIndex(rowNumber-1,columnNumber)&&board[rowNumber-1,columnNumber].getPiece()==null)
						possibleMoves.Add (new Position (rowNumber - 1, columnNumber));
					//can move 2 steps
					if(piece.NumOfMoves==0&&validIndex(rowNumber-2,columnNumber)&&board[rowNumber-2,columnNumber].getPiece()==null&&validIndex(rowNumber-1,columnNumber)&&board[rowNumber-1,columnNumber].getPiece()==null)
						possibleMoves.Add (new Position (rowNumber - 2, columnNumber));
					//can capture
					if(validIndex(rowNumber-1,columnNumber-1)&&board[rowNumber-1,columnNumber-1].getPiece()!=null&&board[rowNumber-1,columnNumber-1].getPiece().PlayerType!=piece.PlayerType)
						possibleMoves.Add (new Position (rowNumber - 1, columnNumber-1));
					if(validIndex(rowNumber-1,columnNumber+1)&&board[rowNumber-1,columnNumber+1].getPiece()!=null&&board[rowNumber-1,columnNumber+1].getPiece().PlayerType!=piece.PlayerType)
						possibleMoves.Add (new Position (rowNumber - 1, columnNumber+1));

				} else {
					if(validIndex(rowNumber+1,columnNumber)&&board[rowNumber+1,columnNumber].getPiece()==null)
						possibleMoves.Add (new Position (rowNumber + 1, columnNumber));
					if(piece.NumOfMoves==0&&validIndex(rowNumber+2,columnNumber)&&board[rowNumber+2,columnNumber].getPiece()==null&&validIndex(rowNumber+1,columnNumber)&&board[rowNumber+1,columnNumber].getPiece()==null)
						possibleMoves.Add (new Position (rowNumber + 2, columnNumber));
					if(validIndex(rowNumber+1,columnNumber-1)&&board[rowNumber+1,columnNumber-1].getPiece()!=null&&board[rowNumber+1,columnNumber-1].getPiece().PlayerType!=piece.PlayerType)
						possibleMoves.Add (new Position (rowNumber + 1, columnNumber-1));
					if(validIndex(rowNumber+1,columnNumber+1)&&board[rowNumber+1,columnNumber+1].getPiece()!=null&&board[rowNumber+1,columnNumber+1].getPiece().PlayerType!=piece.PlayerType)
						possibleMoves.Add (new Position (rowNumber + 1, columnNumber+1));
				}
			}
			#endregion

			#region KNIGHT possible moves
			else if (piece.PieceType  == Global.PIECE_TYPE._KNIGHT) {
				int []dx=new int[]{-1,-2,-2,-1,1,2,2,1};
				int []dy=new int[]{-2,-1,1,2,-2,-1,1,2};
				for (int i = 0; i < dx.Length; i++) {
					if (!validIndex (rowNumber + dx [i], columnNumber + dy [i]) || (board [rowNumber + dx [i], columnNumber + dy [i]].getPiece () != null && board [rowNumber + dx [i], columnNumber + dy [i]].getPiece ().PlayerType == piece.PlayerType))
						continue;
					possibleMoves.Add (new Position (rowNumber +dx[i], columnNumber+dy[i]));
				}
			} 
			#endregion

			#region KING possible moves
			else if (piece.PieceType  == Global.PIECE_TYPE._KING) {
				int []dx=new int[]{-1,-1,-1,1,1,1,0,0};
				int []dy=new int[]{-1,0,1,-1,0,1,1,-1};
				for (int i = 0; i < dx.Length; i++) {
					if (!validIndex (rowNumber + dx [i], columnNumber + dy [i]) || (board [rowNumber +dx[i], columnNumber+dy[i]].getPiece () != null && board [rowNumber +dx[i], columnNumber+dy[i]].getPiece ().PlayerType == piece.PlayerType))
						continue;
					possibleMoves.Add (new Position (rowNumber +dx[i], columnNumber+dy[i]));
				}
				// Castling rule player 1
				if(piece.NumOfMoves==0 && piece.PlayerType==Global.PLAYER_TYPE._PLAYER1 && 
					board[7,1].getPiece()== null && board[7,2].getPiece()== null &&board[7,3].getPiece()== null&&
					board[7,0].getPiece().PieceType==Global.PIECE_TYPE._ROOK&&board[7,0].getPiece().NumOfMoves==0){
					possibleMoves.Add (new Position (rowNumber, columnNumber-2));
				}
				if(piece.NumOfMoves==0 && piece.PlayerType==Global.PLAYER_TYPE._PLAYER1 && 
					board[7,5].getPiece()== null && board[7,6].getPiece()== null &&
					board[7,7].getPiece().PieceType==Global.PIECE_TYPE._ROOK && board[7,7].getPiece().NumOfMoves==0){
					possibleMoves.Add (new Position (rowNumber, columnNumber+2));
				}
				// Castling rule player 2
				if(piece.NumOfMoves==0 && piece.PlayerType==Global.PLAYER_TYPE._PLAYER2 && 
					board[0,1].getPiece()== null && board[0,2].getPiece()== null &&board[0,3].getPiece()== null&&
					board[0,0].getPiece().PieceType==Global.PIECE_TYPE._ROOK&&board[0,0].getPiece().NumOfMoves==0){
					possibleMoves.Add (new Position (rowNumber, columnNumber-2));
				}
				if(piece.NumOfMoves==0 && piece.PlayerType==Global.PLAYER_TYPE._PLAYER2 && 
					board[0,5].getPiece()== null && board[0,6].getPiece()== null &&
					board[0,7].getPiece().PieceType==Global.PIECE_TYPE._ROOK&&board[0,7].getPiece().NumOfMoves==0){
					possibleMoves.Add (new Position (rowNumber, columnNumber+2));
				}
			}

			#endregion

			#region Bishop possible moves
			else if (piece.PieceType  == Global.PIECE_TYPE._BISHOP) {
				addBishop(rowNumber,columnNumber,board,piece,possibleMoves);
			}
			#endregion

			#region Queen possible moves
			else if (piece.PieceType  == Global.PIECE_TYPE._QUEEN) {
				//Rook moves
				addRook(rowNumber,columnNumber,board,piece,possibleMoves);

				//Bishop moves
				addBishop(rowNumber,columnNumber,board,piece,possibleMoves);

			}
			#endregion

			#region Rook possible moves
			else if(piece.PieceType ==Global.PIECE_TYPE._ROOK){
				addRook(rowNumber,columnNumber,board,piece,possibleMoves);
			}
			#endregion
			return possibleMoves;
		}

		#region add Bishop possible moves
		/// <summary>
		/// Gemerate the bishop possible moves and add them to possible moves list.
		/// </summary>
		/// <param name="rowNumber">Bishop row number.</param>
		/// <param name="columnNumber">Bishop column number.</param>
		/// <param name="board">Board.</param>
		/// <param name="Piece">Piece.</param>
		/// <param name="possibleMoves">Possible moves.</param>
		public static void addBishop(int rowNumber,int columnNumber,BoardCell[,]board,ChessPiece Piece,List<Position>possibleMoves){
			int newRow = 0;
			int newColumn = 0;
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow = rowNumber + i;
				newColumn = columnNumber + i;
				if (validIndex (newRow, newColumn)) {
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));
				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow = rowNumber - i;
				newColumn = columnNumber - i;
				if (validIndex (newRow, newColumn)) {
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));
				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow = rowNumber + i;
				newColumn = columnNumber - i;
				if (validIndex (newRow, newColumn)) {

					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));
				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow = rowNumber - i;
				newColumn = columnNumber + i;
				if(validIndex(newRow,newColumn)){
					if (board [newRow,newColumn].getPiece () != null && board [newRow,newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));
				}
			}
		}
		#endregion

		#region add rook possible moves
		/// <summary>
		/// Gemerate the Rook possible moves and add them to possible moves list.
		/// </summary>
		/// <param name="rowNumber">Rook row number.</param>
		/// <param name="columnNumber">Rook column number.</param>
		/// <param name="board">Board cells array.</param>
		/// <param name="Piece">Rook piece.</param>
		/// <param name="possibleMoves">Possible moves to be added.</param>
		public static void addRook(int rowNumber,int columnNumber,BoardCell[,]board,ChessPiece Piece,List<Position>possibleMoves){
			int newRow=0;
			int newColumn=0;
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow=rowNumber+i;
				newColumn=columnNumber;
				if (validIndex (newRow, newColumn)) {
					if (board [newRow,newColumn].getPiece () != null && board [newRow,newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));

				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow=rowNumber-i;
				newColumn=columnNumber;
				if (validIndex (newRow, newColumn)) {

					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));
				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow=rowNumber;
				newColumn=columnNumber+i;
				if (validIndex (newRow, newColumn)) {

					if (board [newRow,newColumn].getPiece () != null && board [newRow,newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));

				}
			}
			for (int i = 1; i < Global._ROWSNUM; i++) {
				newRow=rowNumber;
				newColumn=columnNumber-i;
				if (validIndex (newRow, newColumn)) {

					if (board [newRow,newColumn].getPiece () != null && board [newRow,newColumn].getPiece ().PlayerType == Piece.PlayerType)
						break;
					if (board [newRow, newColumn].getPiece () != null && board [newRow, newColumn].getPiece ().PlayerType != Piece.PlayerType){
						possibleMoves.Add (new Position (newRow,newColumn));
						break;
					}
					possibleMoves.Add (new Position (newRow,newColumn));

				}
			}
		}

		#endregion

		/// <summary>
		/// Initialize the portfish engine
		/// </summary>
		public static void initPortfish(){
			Program.initPortfish (true);
		}


		/// <summary>
		/// Check if there is a move that computer can win the match with (PortFish can't do this move so we do it)
		/// </summary>
		/// <returns>The move that computer will win with else empty string.</returns>
		public static string computerCanWin(){
			BoardCell[,] board = gState.Board.getBoardCells ();
			List<Position> possibleMoves;
			for(int row=0;row<Global._ROWSNUM;row++){
				for(int column=0;column<Global._COLUMNSNUM;column++){
					if (board [row, column].getPiece () != null && board [row, column].getPiece ().PlayerType == Global.PLAYER_TYPE._PLAYER2) {
						possibleMoves=getPossibleMoves(row,column);
						for (int k = 0; k < possibleMoves.Count; k++) {
							if (board [possibleMoves [k].RowNum, possibleMoves [k].ColumnNum].getPiece () != null &&
								board [possibleMoves [k].RowNum, possibleMoves [k].ColumnNum].getPiece ().PieceType  == Global.PIECE_TYPE._KING) {
								return convertMoveToString(new Move(new Position(row,column),possibleMoves[k]));
							}
						}
					}
				}
			}
			return "";
		}

		/// <summary>
		/// Player2s the in check.
		/// </summary>
		/// <returns><c>true</c>, if in check was player2ed, <c>false</c> otherwise.</returns>
		public static bool player2InCheck(){
			BoardCell[,] board = gState.Board.getBoardCells ();
			List<Position> possibleMoves;
			for(int i=0;i<Global._ROWSNUM;i++){
				for(int j=0;j<Global._COLUMNSNUM;j++){
					if (board [i, j].getPiece () != null && board [i, j].getPiece ().PlayerType == Global.PLAYER_TYPE._PLAYER1) {
						possibleMoves=getPossibleMoves(i,j);
						for (int k = 0; k < possibleMoves.Count; k++) {
							if (board [possibleMoves [k].RowNum, possibleMoves [k].ColumnNum].getPiece () != null &&
								board [possibleMoves [k].RowNum, possibleMoves [k].ColumnNum].getPiece ().PieceType  == Global.PIECE_TYPE._KING) {
								return true;
							}
						}
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Convert the board to char array that will be used to update the board state in the Portfish engine.
		/// </summary>
		/// <returns>The board in string.</returns>
		public static string boardToString(){
			string boardStr="";
			int spaces;
			BoardCell[,] board = gState.Board.getBoardCells ();
			for (int i = 0; i < Global._ROWSNUM; i++) {
				spaces = 0;
				for (int j = 0; j < Global._COLUMNSNUM; j++) {
					if (board [i, j].getPiece () == null) {
						spaces++;
					} else {
						if(spaces>0)
						boardStr += spaces;
						spaces = 0;
						if (board [i, j].getPiece ().PlayerType == Global.PLAYER_TYPE._PLAYER1) {

							if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._PAWN)
								boardStr += "P";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._BISHOP)
								boardStr += "B";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._KING)
								boardStr += "K";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._KNIGHT)
								boardStr += "N";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._QUEEN)
								boardStr += "Q";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._ROOK)
								boardStr += "R";
						} else {
							if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._PAWN)
								boardStr += "p";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._BISHOP)
								boardStr += "b";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._KING)
								boardStr += "k";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._KNIGHT)
								boardStr += "n";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._QUEEN)
								boardStr += "q";
							else if(board [i, j].getPiece ().PieceType ==Global.PIECE_TYPE._ROOK)
								boardStr += "r";
						}

					}

				}
				if (spaces > 0)
					boardStr +=spaces;
				if(i<7)
					boardStr += "/";
			}
			return boardStr;
		}


		/// <summary>
		/// Check the selected cell type and perform the suitable action
		/// </summary>
		/// <param name="row">Row.</param>
		/// <param name="column">Column.</param>
		public static void checkCell(int row,int column){

			// if player vs computer & player 1 turn : do player1 move then do computer move 
			if (gState.MatchType == Global.GAME_TYPE._PLAYER_VS_COMPUTER) { 
				if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
					if (gState.CardSelected) {
						if (gState.SelectedCard.CardType == Global.CARD_TYPE._LOSE_TURN_CARD|| !isValidCard(gState.SelectedCard)) {
							updatePlayerCards ();
							movingPiece = true;
							shouldStop = true;

							gState.Player2.MatchSeconds = player2Seconds;
							player1MatchTimerShouldStop = true;
							player2MatchTimerShouldStop = true;

							changeTurn ();
						}
						else
							applyHumanAction (row, column, Global.PLAYER_TYPE._PLAYER1);
					}
				}
			} 
			//if 2 human players then do the move corresponding to the current turn
			else {
				if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
					if (movingPiece) {
						changeTurn ();
					}
					if (gState.CardSelected) {
						if (gState.SelectedCard.CardType == Global.CARD_TYPE._LOSE_TURN_CARD || !isValidCard(gState.SelectedCard)) {
							updatePlayerCards ();
							movingPiece = true;
							shouldStop = true;

							gState.Player2.MatchSeconds = player2Seconds;
							player1MatchTimerShouldStop = true;
							player2MatchTimerShouldStop = true;

						}
						else
							applyHumanAction (row, column, Global.PLAYER_TYPE._PLAYER1);
					}

				} else  {
					if (movingPiece) {
						changeTurn ();
					}
					if (gState.CardSelected) {
						if (gState.SelectedCard.CardType == Global.CARD_TYPE._LOSE_TURN_CARD || !isValidCard(gState.SelectedCard)) {
							updatePlayerCards ();
							movingPiece = true;
							shouldStop = true;

							gState.Player1.MatchSeconds = player1Seconds;
							player1MatchTimerShouldStop = true;
							player2MatchTimerShouldStop = true;

						}
						else
							applyHumanAction (row, column, Global.PLAYER_TYPE._PLAYER2);
					}
				}
			}
		}

		/// <summary>
		/// Check if the card has valid moves in the board
		/// </summary>
		/// <returns><c>true</c>, if the card is valid, <c>false</c> otherwise.</returns>
		/// <param name="card">Card.</param>
		public static bool isValidCard(Card card){
			BoardCell [,]board = gState.Board.getBoardCells ();
			for (int i = 0; i < Global._ROWSNUM; i++) {
				for (int j = 0; j < Global._COLUMNSNUM; j++) {
					if (board [i, j].getPiece () != null && board [i, j].getPiece().PlayerType==gState.CurrentPlayer.PlayerType
						&& (isPieceAvailableForCard (card, board [i, j].getPiece()) == Global.PIECE_CARD_CHECK._YES || isPieceAvailableForCard (card, board [i, j].getPiece()) == Global.PIECE_CARD_CHECK.YES_LOSE_TURN) 
						&& getPossibleMoves(i,j).Count>0)
							return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Applies the human action.
		/// </summary>
		/// <param name="row">Selected piece row number.</param>
		/// <param name="column">Selected piece column number.</param>
		/// <param name="playerType">Player 1 or player 2.</param>
		public static void applyHumanAction(int row,int column,Global.PLAYER_TYPE playerType){
			//check which type of selection and do the action
			BoardCell[,] boardCells = gState.Board.getBoardCells ();
			ChessPiece cellPiece = boardCells [row, column].getPiece ();
			Position selectedCellPos = hasSelectedCell ();

			if (selectedCellPos != null) {
				if (boardCells [row, column].getState () == Global.CELL_STATE._POSSIBLE_MOVE) {
					//perform The move
					Move move = new Move (selectedCellPos,new Position (row, column));

					//change players last moves
					updateLastMoves (gState.SelectedPiece);
					//do move
					movePiece (move);
					//unselect cells 
					unSelectAllCells ();
					//update player cards

					updatePlayerCards ();

					movingPiece = true;

					shouldStop = true;

					if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
						gState.Player2.MatchSeconds = player2Seconds;
						player1MatchTimerShouldStop = true;
						player2MatchTimerShouldStop = true;
					} else {
						gState.Player1.MatchSeconds = player1Seconds;
						player1MatchTimerShouldStop = true;
						player2MatchTimerShouldStop = true;
					}

					if(gState.MatchType==Global.GAME_TYPE._PLAYER_VS_COMPUTER)
						changeTurn ();
				} else {
					unSelectAllCells ();
				}
			} else if (cellPiece != null && cellPiece.PlayerType == playerType && isPieceAvailableForCard(gState.SelectedCard,cellPiece)==Global.PIECE_CARD_CHECK._YES ) {
				selectCell (row, column );
			}

		}


		/// <summary>
		/// Applies the computer action.
		/// </summary>
		public static void applyComputerAction(){
			try{
				bool flag=false;
				string computerSkillLevel=gState.ComputerSkillLevel;
				string pos = computerCanWin();
				string moveStr="";
				if(pos==""){
					string currentBoard = boardToString();
					string searchMoves=getCardsPossibleMoves();
					if(searchMoves.Equals("")||searchMoves.Equals("1")){
						flag=true;
						Xamarin.Forms.Device.BeginInvokeOnMainThread(new Action(delegate() {
							gState.SelectedCard=gState.CurrentPlayer.PlayerCards[0];
							updatePlayerCards ();
							changeTurn();
							return;
						}));
					}else
					{
						if(player2InCheck())
							moveStr = Program.goEvasion (currentBoard,Global._ENGINE_DEPTH,searchMoves,computerSkillLevel);
						else
							moveStr = Program.goNonEvasion (currentBoard,Global._ENGINE_DEPTH,searchMoves,computerSkillLevel);
					}
				}
				else
					moveStr=pos;
				if(!flag){
					char[] moveChar = moveStr.ToCharArray ();
					Position prevPosition = new Position (7-(moveChar[1]-'1'),moveChar[0]-'a');
					Position nextPosition = new Position (7-(moveChar[3]-'1'),moveChar[2]-'a');
					gState.SelectedPiece=gState.Board.getBoardCells()[prevPosition.RowNum,prevPosition.ColumnNum].getPiece();
					Move move=new Move(prevPosition,nextPosition);
					gState.SelectedCard = getCardForMove(move);

					gState.CardSelected = true;
					if(gState.SelectedCard!=null){
						Xamarin.Forms.Device.BeginInvokeOnMainThread(new Action(delegate() {
							//change players last moves
							updateLastMoves (gState.SelectedPiece);
							//do move
							movePiece (move);
							//update player cards
							updatePlayerCards ();
							changeTurn ();
						}));
						gState.turnDidChange();
					}
					else{
						Xamarin.Forms.Device.BeginInvokeOnMainThread(new Action(delegate() {

							gState.SelectedCard=gState.CurrentPlayer.PlayerCards[0];
							updatePlayerCards ();
							changeTurn();
							return;					
						}));
					}
				}
			}
			catch(Exception e){
				Console.WriteLine ("Error "+e);
				Xamarin.Forms.Device.BeginInvokeOnMainThread(new Action(delegate() {

				gState.SelectedCard=gState.CurrentPlayer.PlayerCards[0];
				updatePlayerCards ();
				changeTurn();
					return;					
				}));

			}
		}

		/// <summary>
		/// Updates the last piece moved for both player1 and player2
		/// will be used in move last piece special card.
		/// </summary>
		/// <param name="piece">Piece.</param>
		public static void updateLastMoves(ChessPiece piece){
			if(piece!=null)
			if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1)
				gState.Player1LastPieceMoved = piece.PieceType;
			else
				gState.Player2LastPieceMoved = piece.PieceType;
		}

		/// <summary>
		/// Changes the player turn this will start the current player timers 
		/// and update the current player hand cards.
		/// </summary>
		public static void changeTurn(){
			for (int k = 0; k < gState.CurrentPlayer.PlayerCards.Count; k++) 
				gState.CurrentPlayer.PlayerCards[k].IsSelected=false;
			gState.MoveNumber++;
			if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER2 && gState.MatchType == Global.GAME_TYPE._PLAYER_VS_COMPUTER) 
				gState.MoveTimer = "";
			else
				gState.updateCardsView ();
			gState.turnDidChange();
			movingPiece = false;
			gState.SelectedCard = null;
			gState.CardSelected = false;
			gState.selectedCardsList.Clear ();
			shouldStop = true;
			gState.MoveTimer = gState.CurrentPlayer.TurnSeconds+"";
			bool flag = false;

			//if the current player is computer don't show his cards
			if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER2 && gState.MatchType == Global.GAME_TYPE._PLAYER_VS_COMPUTER) {
				ThreadPool.QueueUserWorkItem (o => applyComputerAction ());

			} else {
				gState.nextDrawnCard.CardType = gState.availableCards [gState.availableCards.Count - 1];
				gState.nextDrawnCard.PlayerType = gState.CurrentPlayer.PlayerType;
				gState.nextDrawnCard.CardImage=  Global.cardToImageMap [Global.getCardName(gState.nextDrawnCard.CardType,gState.nextDrawnCard.PlayerType,false,ChessUser.getInstance().ChessStyle)];	
				if(!flag&&gState.CurrentPlayer.TurnSeconds>0)
					ThreadPool.QueueUserWorkItem (o => startMoveTimer ());


			}
			//start the current player match and move timers
			if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
				gState.Player2.MatchSeconds = player2Seconds;
				player1MatchTimerShouldStop = false;
				player2MatchTimerShouldStop = true;
			} else {
				gState.Player1.MatchSeconds = player1Seconds;
				player1MatchTimerShouldStop = true;
				player2MatchTimerShouldStop = false;
			}
			startMatchTimerThread ();
		}

		/// <summary>
		/// Starts the move timer for the current player in a new thread.
		/// </summary>
		public static void startMoveTimerThread(){
			if(ChessEngine.gState.CurrentPlayer.TurnSeconds>0)
				ThreadPool.QueueUserWorkItem (o =>  ChessEngine.startMoveTimer());
		}

		/// <summary>
		/// Starts the match timer for the current player in a new thread.
		/// </summary>
		public static void startMatchTimerThread(){
			if(ChessEngine.gState.Player1.MatchSeconds>0)
				ThreadPool.QueueUserWorkItem (o =>  ChessEngine.startPlayer1MatchTimer());
			if(ChessEngine.gState.Player2.MatchSeconds>0)
				ThreadPool.QueueUserWorkItem (o =>  ChessEngine.startPlayer2MatchTimer());
		}

		static int player1Seconds;
		static int player2Seconds;

		/// <summary>
		/// Starts the player1 match timer this will update the timer for player 1, if ended then the match is over.
		/// </summary>
		public static void startPlayer1MatchTimer(){
			int seconds = gState.Player1.MatchSeconds;
			gState.Player1MatchTimer = "" + seconds;
			long now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			long then = now + (gState.Player1.MatchSeconds*1000);
			long currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);

			while (!player1MatchTimerShouldStop&&currentTime < then) {
				if (currentTime > now + 1000) {
					int elapsedSeconds=(int)(currentTime - now) / 1000;
					now = currentTime;
					seconds-=elapsedSeconds;
					player1Seconds = seconds;
					gState.Player1MatchTimer=""+seconds;
				}
				currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
			}
			//if the match timer ended then player 1 lose the match
			Xamarin.Forms.Device.BeginInvokeOnMainThread (new Action (delegate() {
				if(player1Seconds<=1)
					gState.updateMatchResult (MATCH_RESULT.PLAYER2_WIN);
			}));
		}

		/// <summary>
		/// Starts the player2 match timer this will update the timer for player 2, if ended then the match is over.
		/// </summary>
		public static void startPlayer2MatchTimer(){
			int seconds = gState.Player2.MatchSeconds;
			gState.Player2MatchTimer = "" + seconds;
			long now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			long then = now + (gState.Player2.MatchSeconds*1000);
			long currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);

			while (!player2MatchTimerShouldStop&&currentTime < then) {
				if (currentTime > now + 1000) {
					int elapsedSeconds=(int)(currentTime - now) / 1000;
					now = currentTime;
					seconds-=elapsedSeconds;
					player2Seconds = seconds;
					gState.Player2MatchTimer=""+seconds;
				}
				currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
			}
			//if the match timer ended then player 1 lose the match
			Xamarin.Forms.Device.BeginInvokeOnMainThread (new Action (delegate() {
				if(player2Seconds<=1)
					gState.updateMatchResult (MATCH_RESULT.PLAYER1_WIN);
			}));
		}

		/// <summary>
		/// Starts current move timer.
		/// </summary>
		public static void startMoveTimer(){
			shouldStop = false;
			int seconds = gState.CurrentPlayer.TurnSeconds;
			gState.MoveTimer = "" + seconds;
			long now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
			long then = now + (gState.CurrentPlayer.TurnSeconds*1000);
			long currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
			//Decrement timer till player move piece or 
			while (!shouldStop && currentTime < then) {
				if (currentTime > now + 1000) {
					Console.WriteLine ("now : "+currentTime);
					int elapsedSeconds=(int)(currentTime - now) / 1000;
					now = currentTime;
					Console.WriteLine ("elapsed time: "+elapsedSeconds);
					seconds-=elapsedSeconds;
					gState.MoveTimer=""+seconds;
				}
				currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond);
			}
			Console.WriteLine("end turn");
			if (!shouldStop) {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (new Action (delegate() {
					Console.WriteLine("Timer is fired");
					updatePlayerCards();
					unSelectAllCells();
					movingPiece=true;

					if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
						gState.Player2.MatchSeconds = player2Seconds;
						player1MatchTimerShouldStop = true;
						player2MatchTimerShouldStop = true;
					} else {
						gState.Player1.MatchSeconds = player1Seconds;
						player1MatchTimerShouldStop = true;
						player2MatchTimerShouldStop = true;
					}

					if(gState.MatchType==Global.GAME_TYPE._PLAYER_VS_COMPUTER)
						changeTurn ();
				}));
			}
		}

		/// <summary>
		/// Check selected card from the view has possible moves or not and add it to the selected card list
		/// </summary>
		/// <param name="card">Card.</param>
		public static void isCardHavingMoves(Card card){
			// check the 3 same cards special move 
			if (gState.selectedCardsList.Count == 0 || (gState.selectedCardsList.Count != 0 && gState.selectedCardsList [gState.selectedCardsList.Count - 1].CardType == card.CardType)) {
				bool flag = false;
				for (int i = 0; i < gState.selectedCardsList.Count; i++)
					if (card.Index == gState.selectedCardsList [i].Index) {
						flag = true;
						break;
					}
				if (!flag) {
					for (int i = 0; i < gState.selectedCardsList.Count; i++)
						gState.selectedCardsList[i].IsSelected = true;
					card.IsSelected = true;
					gState.selectedCardsList.Add (card);
					if (gState.selectedCardsList.Count == 3) {
						movingPiece = true;
						change3Cards ();
					}
				}
			} else{
				for (int k = 0; k < gState.CurrentPlayer.PlayerCards.Count; k++) 
					gState.CurrentPlayer.PlayerCards[k].IsSelected=false;
				card.IsSelected = true;
				gState.SelectedCard = null;
				gState.CardSelected = false;
				gState.selectedCardsList.Clear ();
				gState.selectedCardsList.Add (card);
			}

			getCardForMove (card);
		}

		/// <summary>
		/// Set the selected card.
		/// </summary>
		/// <param name="card">Card.</param>
		// TODO: Change name to "selectCard"
		public static void getCardForMove(Card card){
			card.IsSelected = true;
			gState.SelectedCard = card;
			gState.CardSelected = true;
		}

		/// <summary>
		/// Apply the 3 cards change for the selected cards list.
		/// </summary>
		public static void change3Cards(){
			Card discardedCard = null;
			Card drawnCard=null;
			gState.selectedCardsList.Sort(delegate(Card c1, Card c2) { return c1.Index.CompareTo(c2.Index); });
			for(int i=0;i<gState.selectedCardsList.Count;i++){
				discardedCard = gState.selectedCardsList[i];
				drawnCard = selectRandomCard (discardedCard.Index, gState.CurrentPlayer.PlayerType);
				updateCard (discardedCard,drawnCard);
			}
			gState.coverCards ();
			gState.MoveTimer="Tap the board to take turn";
			shouldStop = true;

			if (gState.CurrentPlayer.PlayerType == Global.PLAYER_TYPE._PLAYER1) {
				gState.Player2.MatchSeconds = player2Seconds;
				player1MatchTimerShouldStop = true;
				player2MatchTimerShouldStop = true;
			} else {
				gState.Player1.MatchSeconds = player1Seconds;
				player1MatchTimerShouldStop = true;
				player2MatchTimerShouldStop = true;
			}

			if(gState.MatchType==Global.GAME_TYPE._PLAYER_VS_COMPUTER)
				changeTurn ();
		}
	
		/// <summary>
		/// Updates the player cards : replace the discarded card with the drawn one.
		/// </summary>
		public static void updatePlayerCards(){
			if (gState.SelectedCard != null) {
				Card discardedCard = gState.SelectedCard;
				Card drawnCard = selectRandomCard (discardedCard.Index, gState.CurrentPlayer.PlayerType);
				// update model

				updateCard (discardedCard,drawnCard);

				//add discarded card to discarded list 
				gState.discardedCards.Add (discardedCard.CardType);
			}
			//update view with MessageCenter
			if (gState.MatchType != Global.GAME_TYPE._PLAYER_VS_COMPUTER) {
				gState.coverCards ();
				gState.MoveTimer = "Tap the board to take turn";
			}
			if(gState.MatchType==Global.GAME_TYPE._PLAYER_VS_COMPUTER&& gState.CurrentPlayer.PlayerType==Global.PLAYER_TYPE._PLAYER2)
				gState.MoveTimer="";
		}

		/// <summary>
		/// Update the bindable object to the card view
		/// </summary>
		/// <param name="discardedCard">Discarded card.</param>
		/// <param name="drawnCard">Drawn card.</param>
		public static void updateCard(Card discardedCard,Card drawnCard){
			gState.CurrentPlayer.PlayerCards [discardedCard.Index].CardImage = drawnCard.CardImage;
			gState.CurrentPlayer.PlayerCards [discardedCard.Index].CardType = drawnCard.CardType;
			gState.CurrentPlayer.PlayerCards [discardedCard.Index].PlayerType = drawnCard.PlayerType;
			gState.CurrentPlayer.PlayerCards [discardedCard.Index].IsCovered = false;
			gState.CurrentPlayer.PlayerCards [discardedCard.Index].IsSelected = false;
		}

		/// <summary>
		/// Regenerate available cards from discarded cards then clear discarded cards
		/// </summary>
		private static void regenearateAvailableCards(){

			gState.availableCards.Clear ();

			for(int i=0;i<gState.discardedCards.Count;i++)
				gState.availableCards.Add (gState.discardedCards [i]);
			shuffle (gState.availableCards);

			gState.discardedCards.Clear ();
		}

		/// <summary>
		/// Selects a random card.
		/// </summary>
		/// <returns>The random card.</returns>
		/// <param name="index">Card index.</param>
		/// <param name="playerType">Player type.</param>
		public static Card selectRandomCard(int index,Global.PLAYER_TYPE playerType){
			if (gState.availableCards.Count == 0) {
				regenearateAvailableCards ();
			}
			Card card = new Card ( gState.availableCards [gState.availableCards.Count-1], playerType, index);
			gState.availableCards.RemoveAt(gState.availableCards.Count-1);
			return card;
		}

		/// <summary>
		/// Check if card type matches piece type to check if this piece is available to be selected based on selected card
		/// </summary>
		/// <returns>The piece available for card.</returns>
		/// <param name="card">Card.</param>
		/// <param name="piece">Piece.</param>
		public static Global.PIECE_CARD_CHECK isPieceAvailableForCard(Card card,ChessPiece piece){
			if (card.CardType == Global.CARD_TYPE._LOSE_TURN_CARD)
				return Global.PIECE_CARD_CHECK.YES_LOSE_TURN;
			if (card.CardType == Global.CARD_TYPE._MOVE_ANY_CARD||
				(card.CardType == Global.CARD_TYPE._MOVE_LAST_CARD &&
					(piece.PieceType==gState.Player1LastPieceMoved||piece.PieceType==gState.Player2LastPieceMoved)))
				return Global.PIECE_CARD_CHECK._YES;
			if ((piece.PieceType  == Global.PIECE_TYPE._BISHOP && card.CardType==Global.CARD_TYPE._BISHOP_CARD)
				||(piece.PieceType  == Global.PIECE_TYPE._PAWN && card.CardType==Global.CARD_TYPE._PAWN_CARD)
				||(piece.PieceType  == Global.PIECE_TYPE._KING && card.CardType==Global.CARD_TYPE._KING_CARD)
				||(piece.PieceType  == Global.PIECE_TYPE._QUEEN && card.CardType==Global.CARD_TYPE._QUEEN_CARD)
				||(piece.PieceType  == Global.PIECE_TYPE._ROOK && card.CardType==Global.CARD_TYPE._ROOK_CARD)
				||(piece.PieceType  == Global.PIECE_TYPE._KNIGHT && card.CardType==Global.CARD_TYPE._KNIGHT_CARD)) {
				return Global.PIECE_CARD_CHECK._YES;
			}
			return Global.PIECE_CARD_CHECK._NO;
		}

		/// <summary>
		/// Gets possible moves for selected card.
		/// </summary>
		/// <returns>The cards possible moves.</returns>
		public static string getCardsPossibleMoves(){
			bool flag=false;
			StringBuilder result = new StringBuilder ();
			List<Card> playerCards = gState.CurrentPlayer.PlayerCards;
			BoardCell[,] board = gState.Board.getBoardCells ();
			HashSet<Global.CARD_TYPE> visitedCards=new HashSet<Global.CARD_TYPE>();
			for (int i = 0; i < Global._ROWSNUM; i++) {
				for (int j = 0; j < Global._COLUMNSNUM; j++) {
					if (board [i, j].getPiece () != null && board [i, j].getPiece ().PlayerType == gState.CurrentPlayer.PlayerType) {
						visitedCards.Clear();
						for (int k = 0; k < playerCards.Count; k++) {
							if (playerCards [k].CardType == Global.CARD_TYPE._LOSE_TURN_CARD||playerCards [k].CardType == Global.CARD_TYPE._MOVE_LAST_CARD)
								flag = true;
							if (!visitedCards.Contains(playerCards[k].CardType)&&isPieceAvailableForCard (playerCards[k], board [i, j].getPiece ()) == Global.PIECE_CARD_CHECK._YES) {
								visitedCards.Add(playerCards[k].CardType);
								//get possible moves in strs 
								List<Position> possibleMoves=getPossibleMoves (i, j);
								for (int l = 0; l < possibleMoves.Count; l++)
									result.Append ((convertMoveToString(new Move(new Position(i,j),possibleMoves[l]))+" ").ToCharArray());
							}
						}
					}
				}
			}
			//lose turn
			if (result.ToString ().Equals ("") && flag)
				return "1";

			return result.ToString();
		}
			
		/// <summary>
		/// Get the card that portfish select to do the move
		/// </summary>
		/// <returns>The card for the move.</returns>
		/// <param name="move">Move.</param>
		public static Card getCardForMove(Move move){
			BoardCell[,] board = gState.Board.getBoardCells ();
			ChessPiece piece = board [move.PrevPosition.RowNum, move.PrevPosition.ColumnNum].getPiece();
			for (int i = 0; i < gState.CurrentPlayer.PlayerCards.Count; i++) {
				if (getCardTypeForPiece (piece) == gState.CurrentPlayer.PlayerCards [i].CardType)
					return gState.CurrentPlayer.PlayerCards [i];
			}
			for (int i = 0; i < gState.CurrentPlayer.PlayerCards.Count; i++) {
				if(gState.CurrentPlayer.PlayerCards [i].CardType==Global.CARD_TYPE._MOVE_LAST_CARD||
					gState.CurrentPlayer.PlayerCards [i].CardType==Global.CARD_TYPE._MOVE_ANY_CARD)
					return gState.CurrentPlayer.PlayerCards [i];
			}
			return null;
		}

		/// <summary>
		/// Get suitable card type for specific chess piece
		/// </summary>
		/// <returns>The card type for piece.</returns>
		/// <param name="piece">Piece.</param>
		public static Global.CARD_TYPE getCardTypeForPiece(ChessPiece piece){
			if (piece.PieceType == Global.PIECE_TYPE._BISHOP)
				return Global.CARD_TYPE._BISHOP_CARD;
			if (piece.PieceType == Global.PIECE_TYPE._KING)
				return Global.CARD_TYPE._KING_CARD;
			if (piece.PieceType == Global.PIECE_TYPE._KNIGHT)
				return Global.CARD_TYPE._KNIGHT_CARD;
			if (piece.PieceType == Global.PIECE_TYPE._PAWN)
				return Global.CARD_TYPE._PAWN_CARD;
			if (piece.PieceType == Global.PIECE_TYPE._ROOK)
				return Global.CARD_TYPE._ROOK_CARD;
			if (piece.PieceType == Global.PIECE_TYPE._QUEEN)
				return Global.CARD_TYPE._QUEEN_CARD;
			return Global.CARD_TYPE._MOVE_ANY_CARD;
		}

		/// <summary>
		/// Get the string representation for the move to use in portfish
		/// </summary>
		/// <returns>The move to string.</returns>
		/// <param name="move">Move.</param>
		public static string convertMoveToString(Move move){
			Position oldPos = move.PrevPosition;
			Position newPos = move.NextPosition;
			//return move as string to write in log file
			string s1 = ""+(char)('a' + oldPos.ColumnNum);
			string s2 = ""+(char)((7-oldPos.RowNum)+'1');
			string s3 = ""+(char)('a' + newPos.ColumnNum);
			string s4 = ""+(char)((7-newPos.RowNum)+'1');
			return s1+s2+s3+s4;
		}

		/// <summary>
		/// Check if there is a selected cell in the board
		/// </summary>
		/// <returns>The selected cell.</returns>
		public static Position hasSelectedCell(){
			for (int i = 0; i < Global._ROWSNUM; i++)
				for (int j = 0; j < Global._COLUMNSNUM; j++) 
					if (gState.Board.getBoardCells () [i, j].getState()== Global.CELL_STATE._Selected) 
						return new Position(i,j);
			return null;
		}

		/// <summary>
		/// Check if there is a possible move in the board.
		/// </summary>
		/// <returns><c>true</c>, if there exist a possiable move, <c>false</c> otherwise.</returns>
		public static bool isPossibleMove(){
			for (int i = 0; i < Global._ROWSNUM; i++)
				for (int j = 0; j < Global._COLUMNSNUM; j++) 
					if (gState.Board.getBoardCells () [i, j].getState()== Global.CELL_STATE._POSSIBLE_MOVE) 
						return true;
			return false;
		}

		/// <summary>
		/// Apply a move.
		/// </summary>
		/// <param name="move">Move.</param>
		public static void movePiece(Move move){
			Position prevPosition=move.PrevPosition;
			Position nextPosition=move.NextPosition;
			BoardCell[,] board = gState.Board.getBoardCells ();
			ChessPiece piece = board [prevPosition.RowNum, prevPosition.ColumnNum].getPiece ();
			piece.Position = nextPosition;
			piece.NumOfMoves++;

			if (!do_castling (move)) {

				board [prevPosition.RowNum, prevPosition.ColumnNum].setPiece (null);
				board [nextPosition.RowNum, nextPosition.ColumnNum].setPiece (piece);

				ChessPiece newPiece = board [nextPosition.RowNum, nextPosition.ColumnNum].getPiece ();

				MATCH_RESULT endVal = checkEndGame ();
				if (endVal != MATCH_RESULT.RUNNING) {
					gState.updateMatchResult (endVal);
				}
				if (endVal == MATCH_RESULT.RUNNING) {
					if ((newPiece.PieceType  == Global.PIECE_TYPE._PAWN && newPiece.PlayerType == Global.PLAYER_TYPE._PLAYER1 && nextPosition.RowNum == 0) ||
						(newPiece.PieceType  == Global.PIECE_TYPE._PAWN && newPiece.PlayerType == Global.PLAYER_TYPE._PLAYER2 && nextPosition.RowNum == 7)) {
						newPiece.promote ();

					} 
				}
			}
		}

		/// <summary>
		/// Do the castling move: switch the king with the castle.
		/// </summary>
		/// <returns><c>true</c>, if castling was done, <c>false</c> otherwise.</returns>
		/// <param name="move">Move.</param>
		public static bool do_castling(Move move){
			Position prevPosition=move.PrevPosition;
			Position nextPosition=move.NextPosition;
			BoardCell[,] board = gState.Board.getBoardCells ();
			ChessPiece kingPiece = board [prevPosition.RowNum, prevPosition.ColumnNum].getPiece ();
			ChessPiece rookPiece ;

			if (kingPiece.PieceType  == Global.PIECE_TYPE._KING && nextPosition.ColumnNum - prevPosition.ColumnNum==2) {
				rookPiece = board [prevPosition.RowNum, prevPosition.ColumnNum + 3].getPiece ();
				board [prevPosition.RowNum, prevPosition.ColumnNum].setPiece (null);
				board [nextPosition.RowNum, nextPosition.ColumnNum].setPiece (kingPiece);
				board [prevPosition.RowNum, prevPosition.ColumnNum + 3].setPiece (null);
				board [nextPosition.RowNum, nextPosition.ColumnNum-1].setPiece (rookPiece);

				return true;
			}
			if (kingPiece.PieceType  == Global.PIECE_TYPE._KING && nextPosition.ColumnNum - prevPosition.ColumnNum==-2) {
				rookPiece = board [prevPosition.RowNum, prevPosition.ColumnNum - 4].getPiece ();
				board [prevPosition.RowNum, prevPosition.ColumnNum].setPiece (null);
				board [nextPosition.RowNum, nextPosition.ColumnNum].setPiece (kingPiece);
				board [prevPosition.RowNum, prevPosition.ColumnNum - 4].setPiece (null);
				board [nextPosition.RowNum, nextPosition.ColumnNum+1].setPiece (rookPiece);

				return true;
			}
			return false;
		
		}

	
		/// <summary>
		/// Do the promotion action: change the piece to the promoted one.
		/// </summary>
		/// <param name="piece">Piece.</param>
		/// <param name="promoteToPiece">Promote to piece.</param>
		public static void do_promotion(ChessPiece piece,string promoteToPiece){
			int row = piece.Position.RowNum;
			int column = piece.Position.ColumnNum;
			BoardCell[,] board = gState.Board.getBoardCells ();
			if(promoteToPiece!="Cancel")
				board [row, column].setPiece (null);
			switch (promoteToPiece) {
				case "QUEEN":
				ChessPiece queen = new ChessPiece (new Position (row, column), Global.PIECE_TYPE._QUEEN, piece.PlayerType==Global.PLAYER_TYPE._PLAYER1?Global.PLAYER_TYPE._PLAYER1:Global.PLAYER_TYPE._PLAYER2);
				board [row, column].setPiece (queen);
					break;
				case "ROOK":
				ChessPiece rook = new ChessPiece (new Position (row, column), Global.PIECE_TYPE._ROOK,  piece.PlayerType==Global.PLAYER_TYPE._PLAYER1?Global.PLAYER_TYPE._PLAYER1:Global.PLAYER_TYPE._PLAYER2);
				board [row, column].setPiece (rook);
					break;
				case "BISHOP":
				ChessPiece bishop = new ChessPiece (new Position (row, column), Global.PIECE_TYPE._BISHOP, piece.PlayerType==Global.PLAYER_TYPE._PLAYER1?Global.PLAYER_TYPE._PLAYER1:Global.PLAYER_TYPE._PLAYER2);
				board [row, column].setPiece (bishop);
					break;
				case "KNIGHT":
				ChessPiece knight = new ChessPiece (new Position (row, column), Global.PIECE_TYPE._KNIGHT, piece.PlayerType==Global.PLAYER_TYPE._PLAYER1?Global.PLAYER_TYPE._PLAYER1:Global.PLAYER_TYPE._PLAYER2);
				board [row, column].setPiece (knight);
					break;
			}
		}

		/// <summary>
		/// Checks the end of the game.
		/// </summary>
		/// <returns>The end game.</returns>
		public static ChessEngine.MATCH_RESULT checkEndGame(){
			BoardCell[,] board = gState.Board.getBoardCells ();
			bool king1 = false,king2 =false;
			for (int i = 0; i < Global._ROWSNUM; i++) {
				for (int j = 0; j < Global._COLUMNSNUM; j++) {
					if (board [i, j].getPiece()!=null&&board [i, j].getPiece ().PieceType == Global.PIECE_TYPE._KING ) {
						if (board [i, j].getPiece ().PlayerType == Global.PLAYER_TYPE._PLAYER1)
							king1 = true;
						else if (board [i, j].getPiece ().PlayerType == Global.PLAYER_TYPE._PLAYER2)
							king2 = true;
					}
				}
			}
			if (!king1)
				return ChessEngine.MATCH_RESULT.PLAYER2_WIN;//player 2 wins 
			if (!king2)
				return  ChessEngine.MATCH_RESULT.PLAYER1_WIN;//player 1 wins 
			return  ChessEngine.MATCH_RESULT.RUNNING;// still playing 
		}
	}
}