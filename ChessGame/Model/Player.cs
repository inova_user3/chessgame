using System;
using System.Collections.Generic;


namespace SharedModel
{
	public class Player
	{
		Global.PLAYER_TYPE playerType; // Player type
		List<Card> playerCards; // Cards that player hold in his hand
		int turnSeconds; // Player turn timer seconds
		int matchSeconds; // Player match timer seconds

		/// <summary>
		/// Create player object that contains player type, cards list, turn seconds and match seconds properties.
		/// </summary>
		/// <param name="_playerType">Player type.</param>
		/// <param name="_playerCards">Player cards.</param>
		/// <param name="_turnSeconds">Turn seconds number.</param>
		/// <param name="_matchSeconds">Match seconds number.</param>
		public Player (Global.PLAYER_TYPE _playerType,List<Card> _playerCards,int _turnSeconds,int _matchSeconds)
		{
			this.matchSeconds=_matchSeconds;
			this.playerCards = _playerCards;
			this.playerType = _playerType;
			this.turnSeconds = _turnSeconds;
		}

		/// <summary>
		/// Gets or sets the match seconds.
		/// </summary>
		/// <value>The match seconds.</value>
		public int MatchSeconds{
			get{return matchSeconds;}
			set{matchSeconds=value;}
		}

		/// <summary>
		/// Gets or sets the turn seconds.
		/// </summary>
		/// <value>The turn seconds.</value>
		public int TurnSeconds{
			get{return turnSeconds;}
			set{turnSeconds=value;}
		}

		/// <summary>
		/// Gets or sets the type of the player.
		/// </summary>
		/// <value>The type of the player.</value>
		public Global.PLAYER_TYPE PlayerType{
			get{return playerType;}
			set{playerType=value;}
		}

		/// <summary>
		/// Gets or sets the player cards.
		/// </summary>
		/// <value>The player cards.</value>
		public List<Card> PlayerCards{
			get{ return playerCards;}
			set{ playerCards=value;}
		}
	}
}

