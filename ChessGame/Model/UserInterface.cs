using System;

namespace SharedModel
{
	/// <summary>
	/// User interface: used to save/load user info from/to preferences.
	/// </summary>
	public interface UserInterface
	{
		void saveUser(ChessUser user);
		ChessUser loadUser();

	}
}
