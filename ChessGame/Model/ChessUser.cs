using System;

namespace SharedModel
{
	/// <summary>
	/// Chess user object that define current user settings (nickname, city, skill level, avatar and chess theme).
	/// </summary>
	public class ChessUser
	{
		string nickName;
	
		/// <summary>
		/// Gets or sets user nickname.
		/// </summary>
		/// <value>User nickname.</value>
		public string NickName{
			get{return nickName;}
			set{ nickName = value;}
		}

		string city;
		/// <summary>
		/// Gets or sets the city.
		/// </summary>
		/// <value>The city.</value>
		public string City{
			get{return city;}
			set{ city = value;}
		}

		Global.AVATAR avatar;
		/// <summary>
		/// Gets or sets the avatar.
		/// </summary>
		/// <value>The avatar.</value>
		public Global.AVATAR Avatar{
			get{return avatar;}
			set{ avatar = value;}
		}


		Global.CHESS_STYLE chessStyle;
		/// <summary>
		/// Gets or sets the chess style.
		/// </summary>
		/// <value>The chess style.</value>
		public Global.CHESS_STYLE ChessStyle{
			get{return chessStyle;}
			set{ chessStyle = value;}
		}

		Global.SKILL_LEVEL skillLevel;
		/// <summary>
		/// Gets or sets the skill level.
		/// </summary>
		/// <value>The skill level.</value>
		public Global.SKILL_LEVEL SkillLevel{
			get{return skillLevel;}
			set{ skillLevel = value;}
		}

		public static ChessUser user=null;

		// init ChessUser 
		public ChessUser ()
		{

		}

		/// <summary>
		/// Gets current user.
		/// </summary>
		/// <returns>Current user.</returns>
		public static ChessUser getInstance(){
			if (user == null)
				user = new ChessUser ();
			return user;
		}
	}
}

