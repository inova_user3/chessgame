using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Portfish2
{
    internal sealed class ConsolePlug : IPlug
    {
        public void Write(string message)
        {
            Console.Write(message);
        }
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }

    class Program
    {
		public static void initPortfish(){
			CheckInfoBroker.init ();
			EvalInfoBroker.init ();
			SwapListBroker.init ();
			MovesSearchedBroker.init ();
			PositionBroker.init ();
			StateInfoArrayBroker.init ();

			MListBroker.init ();
			LoopStackBroker.init ();
			MovePickerBroker.init ();
			StateInfoBroker.init ();

			Utils.init ();
			Book.init ();
			Position.init ();
			KPKPosition.init ();
			Endgame.init ();
			Search.init ();
			Evaluate.init ();
			Threads.init ();
		}

//		public static string go(string currentBoard,int depth){
//			// .Net warmup sequence
//			Plug.IsWarmup = true;
//			currentBoard = "8/1K3Q2/8/8/4Pr2/4rk2/8/3B4";
//
//			Portfish2.Position pos = new Portfish2.Position (currentBoard+" b KQkq - 0 1", false, Threads.main_thread ());
//			Stack<string> stack = Utils.CreateStack ("go depth "+depth);
//			Uci.go (pos, stack);
//
//			Threads.wait_for_search_finished ();
//			Plug.IsWarmup = false;
//
//			string m = Utils.move_to_uci (Search.RootMoves [0].pv [0], Search.RootPosition.chess960);
//			return m;
//		}

		public static string go(string currentBoard,int depth,string searchMoves){
			//Uci.set_option (Utils.CreateStack ("name Hash value 3 "));
			// .Net warmup sequence
			Plug.IsWarmup = true;
			Position pos = new Position (currentBoard+" b - - 0 0 ", false, Threads.main_thread ());
			Stack<string> stack = Utils.CreateStack ("go depth "+depth+" searchmoves "+searchMoves);
			Uci.go (pos, stack);
			Threads.wait_for_search_finished ();
			Plug.IsWarmup = false;
			string m = Utils.move_to_uci (Search.RootMoves [0].pv [0], Search.RootPosition.chess960);
			Console.WriteLine(m);
			return m;
		}
    }
}
