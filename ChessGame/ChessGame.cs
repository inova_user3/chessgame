﻿using System;
using Xamarin.Forms;
using SharedViews;
using SharedModel;
using System.Threading;

using Xamarin.Forms;

namespace ChessGame
{
	public class App : Application
	{
		public App (int widthDP,int heightDP)
		{

			// initialize portfish engine that represents the AI of the game
			ThreadPool.QueueUserWorkItem (o => ChessEngine.initPortfish ());
			//load the current user from preferences file
			ChessUser user=DependencyService.Get<UserInterface>().loadUser();
			//			Page mainNav = null;
			//if no user in the preferences file 
			//then start the 4 wizard pages 
			//else go to the main menu of the game
			int height = Math.Min (widthDP,heightDP);
			int width = Math.Max (widthDP,heightDP);

			if (user == null)
				MainPage = new PlayerWizard_1Screen (width, height, true);
			else {
				NavigationPage navigationPager= new NavigationPage(new TitleScreenPage(width,height));
				navigationPager.BarBackgroundColor = Color.Transparent;
				MainPage = navigationPager;
			}

			//			MainPage = new OnlineOpponentScreen (width,height);
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}


