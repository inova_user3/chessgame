﻿using System;
using Xamarin.Forms;
using Android.Graphics;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using SharedModel;


namespace ChessGame.Droid
{
	[Activity (Label = "ChessGame.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{

			base.OnCreate (bundle);

			var metrics = Resources.DisplayMetrics;
			var widthInDp = ConvertPixelsToDp(metrics.WidthPixels);
			var heightInDp = ConvertPixelsToDp(metrics.HeightPixels);


			global::Xamarin.Forms.Forms.Init (this, bundle);

			try {
				LoadApplication(new App(widthInDp, heightInDp)); 
			} 
			catch (Exception ex) {
				Console.WriteLine (ex); 
				throw;
			}

			//			var page = App.GetMainPage (widthInDp, heightInDp);
			//			page.BackgroundColor =Xamarin.Forms.Color.White;
			//			SetPage (page);

		}
		private int ConvertPixelsToDp(float pixelValue)
		{
			var dp = (int) ((pixelValue)/Resources.DisplayMetrics.Density);
			return dp;
		}
	}
}

//TODO:
/*
 * for memory limit exception try:
 * BitmapFactory.Options o = new BitmapFactory.Options();
 * o.inSampleSize = 2;
 * Bitmap b = BitmapFactory.decodeFile(pathToBitmap, o);
 */
