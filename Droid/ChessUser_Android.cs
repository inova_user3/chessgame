﻿using System;
using Xamarin.Forms;
using Android.Graphics;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using SharedModel;

[assembly: Xamarin.Forms.Dependency (typeof (ChessUser_Android))]
public class ChessUser_Android : Java.Lang.Object, UserInterface
{
	/// <summary>
	/// Saves the user properties to the preferences file.
	/// 1) nickname , city
	/// 2) avatar
	/// 3) chess style
	/// 4) skill level
	/// </summary>
	/// <param name="user">User.</param>
	public void saveUser(ChessUser user){
		var prefs = Android.App.Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private);
		var prefEditor = prefs.Edit();
		//save user's properties for a specific key
		prefEditor.PutString("user name", user.NickName);
		prefEditor.PutString("user city", user.City);
		prefEditor.PutString("chess style", user.ChessStyle==Global.CHESS_STYLE._ADULT? "adult":"kids");
		prefEditor.PutString("avatar",Global.getAvatarImage(user.Avatar));
		if(user.SkillLevel==Global.SKILL_LEVEL._BEGINNER)
			prefEditor.PutString("skill level", "beginner");
		else if(user.SkillLevel==Global.SKILL_LEVEL._INTERMEDIATE)
			prefEditor.PutString("skill level","intermediate");
		else if(user.SkillLevel==Global.SKILL_LEVEL._EXPERT)
			prefEditor.PutString("skill level","expert");
		prefEditor.Commit();
	}

	/// <summary>
	/// Loads the user properties from preferences file.
	/// </summary>
	/// <returns>The user.</returns>
	public ChessUser loadUser(){
		
		var prefs = Android.App.Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private); 
		if (!prefs.Contains ("user name"))
			return null;
		ChessUser.getInstance().NickName = prefs.GetString("user name",null);
		ChessUser.getInstance().City = prefs.GetString("user city",null);
		ChessUser.getInstance().ChessStyle = prefs.GetString("chess style",null)=="adult"? Global.CHESS_STYLE._ADULT:Global.CHESS_STYLE._KIDS;

		if(prefs.GetString ("skill level",null)=="beginner")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._BEGINNER;
		else if(prefs.GetString ("skill level",null)=="intermediate")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._INTERMEDIATE;
		else if(prefs.GetString ("skill level",null)=="expert")
			ChessUser.getInstance().SkillLevel= Global.SKILL_LEVEL._EXPERT;

		return ChessUser.getInstance ();
	}

}