﻿using System;
using SharedViews;
using SharedModel;
using Android.Widget;
using Android.Graphics;
//using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ChessGame.Android;

[assembly: Xamarin.Forms.ExportRenderer (typeof (MyButton), typeof (MyButtonRenderer))]
namespace ChessGame.Android
{
	/// <summary>
	/// Android button renderer:
	/// here we set native Android button properties that can't be accessed in xamarin button.
	/// </summary>
	public class MyButtonRenderer : ButtonRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);
			MyButton element = (MyButton)this.Element;
			var nativeButtonView = (Button)Control;
			//set button font with a forign font
			Typeface font = Typeface.CreateFromAsset (Xamarin.Forms.Forms.Context.Assets, "Fonts/Copperplate Gothic Bold T..ttf");
			nativeButtonView.Typeface = font;
			nativeButtonView.TextSize = element.FontSize;


			//set the button image
			if(element.AndroidButtonType==1)
				nativeButtonView.SetBackgroundResource(ChessGame.Droid.Resource.Drawable.MyButtonLayout);
			else if(element.AndroidButtonType==2)
				nativeButtonView.SetBackgroundResource(ChessGame.Droid.Resource.Drawable.MyButton2Layout);
			if(element.ButtonImage=="ButtonNext_arrow")
				nativeButtonView.SetCompoundDrawablesWithIntrinsicBounds (0, 0,ChessGame.Droid.Resource.Drawable.ButtonNext_arrow, 0);
			else if(element.ButtonImage=="ButtonBack_arrow")
				nativeButtonView.SetCompoundDrawablesWithIntrinsicBounds (ChessGame.Droid.Resource.Drawable.ButtonBack_arrow,0, 0, 0);
		}
	}
}

