﻿using System;
using SharedModel;
using SharedViews;
using Android.Widget;
using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ChessGame.Android;

[assembly: ExportRenderer (typeof (MyLabel), typeof (MyLabelRenderer))]
namespace ChessGame.Android
{
	/// <summary>
	/// Android label renderer:
	/// here we set native Android label properties that can't be accessed in xamarin label.
	/// </summary>
	public class MyLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e) {
			base.OnElementChanged (e);
			MyLabel element = (MyLabel)this.Element;
			var nativeLabelView = (TextView)Control;
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets,"Fonts/Copperplate Gothic Bold T..ttf");
			nativeLabelView.Typeface = font;
			nativeLabelView.TextSize = element.FontSize;
		}
	}
}

